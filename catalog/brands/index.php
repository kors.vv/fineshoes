<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
$APPLICATION->SetTitle("Бренды");
$APPLICATION->SetPageProperty("title", "Бренды обуви ручной работы | Купить в Москве классическую обувь известных производителей | Магазин Fineshoes");
$APPLICATION->SetPageProperty("description", "Каталог брендов обуви ручной работы, которые вы можете купить в нашем интернет-магазине Fine shoes в Москве и Санкт-Петербурге.");




		$hlblock = HL\HighloadBlockTable::getById(2)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();
		$rsData = $entity_data_class::getList(array(
			"select" => array("*"),
			"order" => array("ID" => "ASC"),
			"filter" => array()
		));
		
?><h1>Бренды обуви ручной работы</h1>
<div class="brands">
	<div class="brands__list">
		 <?if($rsData) { 
			while($brand = $rsData->Fetch()) {?>
		<div class="brands__item">
			 <? if(!empty($brand['UF_FILE'])) { 
				$file = CFile::GetFileArray($brand['UF_FILE']);
				
			?> <a class="brands__item-image" href="/catalog/brands/<?=$brand['UF_XML_ID']?>/"> <img alt="<?=$brand['UF_NAME']?>" src="<?=$file['SRC']?>
			 "> </a>
			<? } ?> <a class="brands__item-title" href="/catalog/brands/<?=$brand['UF_XML_ID']?>/"><?=$brand['UF_NAME']?></a>
		</div>
		 <? } } ?>
	</div>
</div>
 <br>
<p>
	 Сдержанность и утонченность, элегантность и стильность, высочайшее качество и безупречный вид&nbsp;—&nbsp;именно этими свойствами отличается английская обувь. А еще&nbsp;—&nbsp;очень многие британские бренды имеют богатую историю, ведь основаны фирмы больше столетия тому назад.
</p>
<p>
	 Туфли или&nbsp;ботинки&nbsp;«родом из Англии»&nbsp;—&nbsp;это качественная кожа, отличный дизайн,&nbsp;ручная работа, широчайший размерный ряд и предельное внимание к мелочам. А если выбирать такую обувь в нашем магазине&nbsp;—&nbsp;то это еще и огромное разнообразие моделей (от классических до весьма креативных), оперативность, а также возможность сэкономить (у нас практикуется дисконтная система, предлагаются весьма привлекательные скидки).
</p>
<h2>Какие бренды обуви в английском стиле представлены в нашем магазине?<br>
 </h2>
<p>
	 Если вы хотите получить действительно идеальную пару, обратите внимание на обувь английских и испанских брендов, имеющих богатую историю и завоевавших призвание не только у скрупулезных британцев, но и у модников всего мира. Эти страны славятся своими компаниями, производящими качественные туфли и ботинки, именно поэтому обувь, приобретенная в Fine Shoes, выглядит безупречно.
</p>
<p>
	 У нас представлены лучшие бренды этих стран:
</p>
<ul>
	<li><a href="https://fineshoes.ru/catalog/brands/barker/">Barker</a>&nbsp;— изначально эта компания специализировалась на производстве исключительно мужской обуви, но позже начала разрабатывать и женские модели; это настоящая британская классика.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/loake/">Loake</a>&nbsp;—&nbsp;один из самых известных обувных брендов Британии. Эта марка английской обуви известна, среди всего прочего, тем, что компания является официальным поставщиком Королевского двора.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/crockett-and-jones/">Crockett&nbsp;&amp;&nbsp;Jones</a>&nbsp;—&nbsp;история компании началась в конце девятнадцатого века с небольшой мануфактуры. Сегодня же это огромное разнообразие моделей, которые отличаются универсальностью и доступной стоимостью.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/grenson/">Grenson</a>&nbsp;—&nbsp;еще один бренд с длительной историей. Обувь английской фирмы&nbsp;Grenson&nbsp;до сегодняшнего дня изготавливается по рантовой технологии, которую практиковали еще в начале девятнадцатого века.</li>
	<li>John White&nbsp;—&nbsp;основатель этой фирмы назвал свою обувь «неуязвимой», и это слово как нельзя лучше описывает основное ее свойство&nbsp;—&nbsp;долговечность и возможность сохранять безупречный вид.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/trickers/">Tricker's</a>&nbsp;—&nbsp;эта компания начиналась как семейное дело, но позже производство расширилось. Особое внимание уделяют в компании качеству обуви, для чего подбирают самых лучших мастеров.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/clarks/">Clarks</a>&nbsp;—&nbsp;еще одна поистине легендарная английская фирма обуви, о которой наверняка слышали даже люди, очень далекие от мира моды; идеальное качество и безупречный дизайн&nbsp;—&nbsp;это отличительные черты данной марки.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/churchs/">Church's</a>&nbsp;—&nbsp;бренд, производящий по-настоящему роскошные модели. Английская женская обувь этой марки, как и модели для джентльменов, отличается эксклюзивностью.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/cheaney/">Cheaney</a>&nbsp;—&nbsp;история бренда перевалила за столетие. За это время модели компании были оценены многими потребителями, а также появлялись на обложках модных журналов.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/yanko/">Yanko</a>&nbsp;—&nbsp;бренд сравнительно молодой. Он был создан в 1961 году, хотя на его родине&nbsp;—&nbsp;острове Майорка (Mallorca)&nbsp;—&nbsp;классическую обувь в английском стиле производят с 1890-х годов.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/carmina/">Carmina</a>&nbsp;—&nbsp;обувь ручной работы, производимая под брендом Carmina Shoemaker, является одной из самых востребованных и лучших не только в Европе, но и в мире.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/berwick/">Berwick</a>&nbsp;—&nbsp;испанский бренд Berwick, специализируется на производстве обуви и аксессуаров из кожи.</li>
	<li><a href="https://fineshoes.ru/catalog/brands/michel/">Michel</a>&nbsp;—&nbsp;фирма основанная в 1925 году&nbsp;—&nbsp;это производитель&nbsp;обуви, у которого с момента основания была цель&nbsp;преуспеть&nbsp;в&nbsp;своем&nbsp;деле.</li>
</ul>
<h2>Лучшие английские марки обуви&nbsp;—&nbsp;у нас</h2>
<p>
	 Как видите, в нашем магазине представлены самые известные и популярные бренды, причем многие марки английской обуви являются лидерами обувного рынка уже не одно десятилетие. Хотите обзавестись парой туфель или ботинок от таких именитых производителей? Тогда милости просим в Fine Shoes, где вас уже наверняка ожидает именно ваша идеальная пара, сшитая из кожи или замши самого высокого качества.
</p>
<p>
	 В нашем интернет-магазине вы найдёте бренды <a href="english/">Англии</a> и <a href="spanish">Испании</a>.
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>