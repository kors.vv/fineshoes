<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION;
$aMenuLinksExt = array();

if(CModule::IncludeModule('iblock'))
{
	$arFilter = array(
		"TYPE" => "catalog",
		"SITE_ID" => SITE_ID,
	);

	$dbIBlock = CIBlock::GetList(array('SORT' => 'ASC', 'ID' => 'ASC'), $arFilter);
	$dbIBlock = new CIBlockResult($dbIBlock);

	if ($arIBlock = $dbIBlock->GetNext())
	{
		if(defined("BX_COMP_MANAGED_CACHE"))
			$GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_".$arIBlock["ID"]);

		if($arIBlock["ACTIVE"] == "Y")
		{
			$section = CIBlockSection::GetList(
				array('SORT' => 'ASC', 'NAME' => 'ASC'),
				array('IBLOCK_ID' => 6, 'SECTION_ID' => false, 'ACTIVE'=>'Y') ,
				false,
				array('ID', 'NAME', 'CODE', 'UF_HIDE', 'UF_COUNTRY'),
				false
			);
			
			$aMenuLinksExt = array();
			
			while($ii = $section->GetNext()) {
                                if( $ii['ID'] == 599 || $ii['ID'] == 764)
                                    continue;
                                
				$aMenuLinksExt[] = array(
					$ii['NAME'],
					'/catalog/' . $ii['CODE'] . '/',
					array('/catalog/' . $ii['CODE'] . '/'),
					array(
					'FROM_IBLOCK' => 1,
					'IS_PARENT' => '',
					'DEPTH_LEVEL' => 1,
                                        'UF_HIDE' => $ii['UF_HIDE'],
                                        'UF_COUNTRY' => $ii['UF_COUNTRY']
					)
				);
			}
			
			$section = CIBlockSection::GetList(
				array('SORT' => 'ASC', 'NAME' => 'ASC'),
				array('IBLOCK_ID' => 6, 'SECTION_ID' => 134, 'ACTIVE'=>'Y') ,
				false,
				array('ID', 'NAME', 'CODE', 'UF_HIDE', 'UF_COUNTRY'),
				false
			);
			
			while($ii = $section->GetNext()) {
                                if( $ii['ID'] == 604 || $ii['ID'] == 605)
                                    continue;
                                
				$aMenuLinksExt[] = array(
					$ii['NAME'],
					'/catalog/brands/' . $ii['CODE'] . '/',
					array('/catalog/brands/' . $ii['CODE'] . '/'),
					array(
					'FROM_IBLOCK' => 1,
					'IS_PARENT' => '',
					'DEPTH_LEVEL' => 1,
						'UF_HIDE' => $ii['UF_HIDE'],
						'UF_COUNTRY' => $ii['UF_COUNTRY']
					)
				);
			}
		}
	}

	if(defined("BX_COMP_MANAGED_CACHE"))
		$GLOBALS["CACHE_MANAGER"]->RegisterTag("iblock_id_new");
}
//if($_SERVER['REMOTE_ADDR']=='81.1.252.98'){

foreach ($aMenuLinksExt as $key => $link) {
	if($link[1]=='/catalog/brands/'){
		unset($aMenuLinksExt[$key]);
	}
}
	//echo '<pre>';print_r($aMenuLinksExt);echo '</pre>';
//}
$aMenuLinks = array_merge($aMenuLinks, $aMenuLinksExt);
?>