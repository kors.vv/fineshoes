<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
CModule::IncludeModule("highloadblock");
use Bitrix\Highloadblock as HL;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;


		$hlblock = HL\HighloadBlockTable::getById(2)->fetch();
		$entity = HL\HighloadBlockTable::compileEntity($hlblock);
		$entity_data_class = $entity->getDataClass();
		$rsData = $entity_data_class::getList(array(
			"select" => array("*"),
			"order" => array("ID" => "ASC"),
			"filter" => array()
		));
?>
<table>
	<tr>
		<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>     <!-- first level-->
			<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
		
		<td>
			<div class="childs">
				<a <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>class="active"<?endif?> href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?></a>
			<?if (is_array($arColumns) && count($arColumns) > 0):?>
				<div class="submenu">
					<?foreach($arColumns as $key=>$arRow):?>
						<div class="column"><ul>
						<? if ($arResult["ALL_ITEMS"][$itemID]["TEXT"] == 'Каталог') echo '<li><a href="/catalog/"><strong>Все типы замков</strong></a></li>'?>
						<?foreach($arRow as $itemIdLevel_2=>$arLevel_3):
						if(!empty($arResult["ALL_ITEMS"][$itemIdLevel_2]['PARAMS']['UF_HIDE'])) continue;
						?>  <!-- second level-->
							<li>
							
								<a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a>
							</li>
						<?endforeach;?>
						</ul></div>
					<?endforeach;?>
					<? if(!empty($arResult["ALL_ITEMS"][$itemID]['PARAMS']['BRAND'])) {?>
					<?
		

					if($rsData) { ?> <div class="column last"><ul><li><a href="/catalog/brands/"><strong>Бренды</strong></a></li><?
			
						while($brand = $rsData->Fetch()) {
							?>
							<li><a href="/catalog/?arrFilter_51_<?=abs(crc32($brand['UF_XML_ID']))?>=Y&set_filter=Показать"><?=$brand['UF_NAME']?></a></li><?
				
						} ?> </ul></div> <?
			
					}

		?>
					
					
					<? } ?>
				</div>
			<?endif?>
			</div>
		</td>	
		<?endforeach;?>
	</tr>
</table>
