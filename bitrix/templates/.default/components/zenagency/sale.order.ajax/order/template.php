<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y")
{
	if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
	{
		if(strlen($arResult["REDIRECT_URL"]) > 0)
		{
			$APPLICATION->RestartBuffer();
			?>
			<script type="text/javascript">
				window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			</script>
			<?
			die();
		}

	}
}
?>
<a name="order_form"></a>
<div class="order-checkout">
<NOSCRIPT>
	<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>
<?
if (!function_exists("getColumnName")){
	function getColumnName($arHeader){
		return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
	}
}
if (!function_exists("cmpBySort")){
	function cmpBySort($array1, $array2){
		if (!isset($array1["SORT"]) || !isset($array2["SORT"]))
			return -1;

		if ($array1["SORT"] > $array2["SORT"])
			return 1;

		if ($array1["SORT"] < $array2["SORT"])
			return -1;

		if ($array1["SORT"] == $array2["SORT"])
			return 0;
	}
}
?>
<div>
	<?
	if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")	{
		if(!empty($arResult["ERROR"]))		{
			foreach($arResult["ERROR"] as $v)
				echo ShowError($v);
		}
		elseif(!empty($arResult["OK_MESSAGE"]))		{
			foreach($arResult["OK_MESSAGE"] as $v)
				echo ShowNote($v);
		}
		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
	}else{
		if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")	{
			if(strlen($arResult["REDIRECT_URL"]) == 0){
				?>
				<h1>Оформление заказа</h1>
				<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");?>
				<?
			}
		}else{
			?>
			<script type="text/javascript">
			<?if(CSaleLocation::isLocationProEnabled()):?>
				<?
				// spike: for children of cities we place this prompt
				$city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
				?>
				BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
					'source' => $this->__component->getPath().'/get.php',
					'cityTypeId' => intval($city['ID']),
					'messages' => array(
						'otherLocation' => '--- '.GetMessage('SOA_OTHER_LOCATION'),
						'moreInfoLocation' => '--- '.GetMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
						'notFoundPrompt' => '<div class="-bx-popup-special-prompt">'.GetMessage('SOA_LOCATION_NOT_FOUND').'.<br />'.GetMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
							'#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
							'#ANCHOR_END#' => '</a>'
						)).'</div>'
					)
				))?>);
			<?endif?>
			var BXFormPosting = false;
			function submitForm(val){
				if (BXFormPosting === true)
					return true;

				BXFormPosting = true;
				if(val != 'Y')
					BX('confirmorder').value = 'N';

				var orderForm = BX('ORDER_FORM');
				BX.showWait();

				<?if(CSaleLocation::isLocationProEnabled()):?>
					BX.saleOrderAjax.cleanUp();
				<?endif?>

				BX.ajax.submit(orderForm, ajaxResult);
				return true;
			}
			function ajaxResult(res){
				var orderForm = BX('ORDER_FORM');
				try{
					// if json came, it obviously a successfull order submit

					var json = JSON.parse(res);
					BX.closeWait();

					if (json.error){
						BXFormPosting = false;
						return;
					}else if (json.redirect){
						window.top.location.href = json.redirect;
					}
				}catch (e){
					// json parse failed, so it is a simple chunk of html
					BXFormPosting = false;
					BX('order_form_content').innerHTML = res;
					<?if(CSaleLocation::isLocationProEnabled()):?>
						BX.saleOrderAjax.initDeferredControl();
					<?endif?>
				}
				BX.closeWait();
				BX.onCustomEvent(orderForm, 'onAjaxSuccess');
			}
			function SetContact(profileId){
				BX("profile_change").value = "Y";
				submitForm();
			}
			</script>
			<?if($_POST["is_ajax_post"] != "Y"){
				?>
<form action="/personal/order/make/" onsubmit="yaCounter47085678.reachGoal('ORDER'); return true;" method="post" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
	<div class="ttl_order">Оформление заказа</div>
	<div class="order_wrap">
				<?
				$datadeliv = array(
					'PROPERTY_CITY_VALUE' => '',
					'PROPERTY_STREET_VALUE' => '',
					'PROPERTY_HOME_VALUE' => '',
					'PROPERTY_CORPUS_VALUE' => '',
					'PROPERTY_ROOM_VALUE' => '',
				);
				if($USER->GetID()) {
					$rsUser = CUser::GetByID($USER->GetID());
					$arUser = $rsUser->Fetch();
					CModule::IncludeModule('iblock');
					$elem = CIBlockElement::GetList(
						array(),
						array('PROPERTY_USER' => $USER->GetID(), 'IBLOCK_ID' => 8),
						false,
						1,
						array('ID' ,'IBLOCK_ID', 'PROPERTY_CITY', 'PROPERTY_STREET', 'PROPERTY_HOME', 'PROPERTY_CORPUS', 'PROPERTY_ROOM')
					);
					$ii = $elem->Fetch();
					if(!empty($ii))
						$datadeliv = $ii;
				}
				$phone = '';
				if(!empty($arUser['PERSONAL_PHONE']))
					$phone = $arUser['PERSONAL_PHONE'];
				?>
				<div class="order_left">
					<div class="basket_title_group">Контактные данные</div>
					<div class="display_flex_order">
						<input type="text" name="ORDER_PROP_1" placeholder="Ваше имя:" class="text input_order_name" value="<?=$USER->GetFirstName()?>"/>
						<input type="text" name="ORDER_PROP_3" placeholder="Телефон. Пример: +7 123 456-78-90" class="text input_order_phone"  value="<?=$phone?>"/>
					</div>
					<input type="text" name="ORDER_PROP_2" placeholder="E-mail:" class="text"  value="<?=$USER->GetParam('EMAIL')?>"/>
					<div class="basket_title_group op display_flex_order">
						<label class="order_pay">Варианты оплаты:</label>
						<select class="order_pay" name="PAY_SYSTEM_ID">
							<option value="1">В магазине (наличными или картой)</option>
							<option value="11">Курьеру (Москва и Петербург)</option>
							<option value="15">Онлайн по карте</option>
						</select>
					</div>
					<div class="clear"></div>
				</div>
				<div class="order_right">
					<div class="basket_title_group">Способы доставки</div>
					<div class="display_flex_order">
						<div>
							<input id="msk" class="radio" type="radio" name="address"  title="Москва" value="Москва"<?if($datadeliv['PROPERTY_CITY_VALUE'] == 'Москва' || !$datadeliv['PROPERTY_CITY_VALUE']) echo ' checked="checked"'?>/>
							<label for="msk" class="radiolabel">Москва</label>
						</div>
						<div>
							<input id="spb" class="radio" type="radio" name="address"  title="Петербург" value="Петербург"<?if($datadeliv['PROPERTY_CITY_VALUE'] == 'Петербург') echo ' checked="checked"'?>/>
							<label for="spb" class="radiolabel">Петербург</label>
						</div>
						<div>
							<input class="radio" id="address_drugoy" type="radio" name="address"  title="Другой город" value="" />
							<input id="address_costom" type="text" name="ORDER_PROP_5" placeholder="Другой город:" class="text input_order_ad_custom" <?if($datadeliv['PROPERTY_CITY_VALUE'] != 'Москва' && $datadeliv['PROPERTY_CITY_VALUE'] != 'Петербург') echo ' value="'.$datadeliv['PROPERTY_CITY_VALUE'].'"'?>/>
						</div>
					</div>
					<div class="clearfix"><?/*div_order_right*/?>
						<input id="sam" class="radio" type="radio" name="DELIVERY_ID"  title="Самовывоз" value="3"<?if(!$datadeliv['DELIVERY_ID']) echo ' checked="checked"'?>>
						<label for="sam" class="radiolabel">Самовывоз</label>
						<input id="kur" class="radio" type="radio" name="DELIVERY_ID"  title="Курьером на адрес" value="2" />
						<label for="kur" class="radiolabel">Курьером на адрес</label><br />
						<div style="display: none;">
							<input id="currach" class="radio " type="radio" name="DELIVERY_ID"  title="Курьером на адрес"  value="16" />
						</div>
					<div class="hidden_dostavka">
						<label class="labeltitle">Адрес доставки:</label>

						<input id="dostavka_street" type="text" name="ORDER_PROP_20" placeholder="Улица" class="text" value="<?=$datadeliv['PROPERTY_STREET_VALUE']?>" />

						<input id="dostavka_dom"  type="text" name="ORDER_PROP_21" placeholder="Дом" class="text fl_3" value="<?=$datadeliv['PROPERTY_HOME_VALUE']?>"  />

						<input id="dostavka_korp" type="text" name="ORDER_PROP_22" placeholder="Корпус" class="text fl_3" value="<?=$datadeliv['PROPERTY_CORPUS_VALUE']?>"  />

						<input id="dostavka_kv" type="text" name="ORDER_PROP_23" placeholder="Квартира" class="text fl_3" value="<?=$datadeliv['PROPERTY_ROOM_VALUE']?>"  />
					</div>

				</div>
				<div class="basket_title_group top">Комментарий к заказу:</div>
				<textarea name="ORDER_DESCRIPTION" class="text" rows="2"></textarea>
			</div>
		</div>
		<div class="order_wrap">
			<div class="clear"></div>
			<div class="captcha_wrap clearfix">
				<div class="captcha_pad">
					<div class="text_conf">При отправке данных, вы принимаете условия <a href="/politika-konfidencialnosti/" target="_blank">пользовательского соглашения</a></div>
					<button  onclick="yaCounter47085678.reachGoal('vkorziny-oformit'); return true;" class="submitz" type="submit" name="send">Оформить заказ</button>
					<?/*<div class="captcha_div">
						<img src="/common/htdocs/captcha?1478684488&key=quest_key" onclick="this.src='/common/htdocs/captcha?'+Math.random();'&key=quest_key'"/>
						<input onfocus="if (this.value=='Код проверки:') this.value=''" onblur="if (this.value=='') this.value='Код проверки:'" type="text" name="captcha" value="Код проверки:" />
					</div>*/?>
				</div>
			</div>
				<?=bitrix_sessid_post()?>
				<div id="order_form_content">
				<?
			}else{
				$APPLICATION->RestartBuffer();
			}
			if($_REQUEST['PERMANENT_MODE_STEPS'] == 1){
				?>
				<input type="hidden" name="PERMANENT_MODE_STEPS" value="1" />
				<?
			}
			if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y"){
				foreach($arResult["ERROR"] as $v)
					echo ShowError($v);
				?>
				<script type="text/javascript">
					top.BX.scrollToNode(top.BX('ORDER_FORM'));
				</script>
				<?
			}
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");
			if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d"){
				//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
				//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
			}else{
				//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
				//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
			}
			//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");
			//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
			if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
				echo $arResult["PREPAY_ADIT_FIELDS"];
			?>
			<?if($_POST["is_ajax_post"] != "Y"){
				?>
					</div>
					<input type="hidden" name="confirmorder" id="confirmorder" value="Y">
					<input type="hidden" name="profile_change" id="profile_change" value="N">
					<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
					<input type="hidden" name="json" value="Y">
					<?//<div class="bx_ordercart_order_pay_center"><a href="javascript:void();" onclick="submitForm('Y'); return false;" id="ORDER_CONFIRM_BUTTON" class="checkout"><?=GetMessage("SOA_TEMPL_BUTTON")</a></div>?>
					<div class="clear"></div>
				</div>
			</form>
				<?
				if($arParams["DELIVERY_NO_AJAX"] == "N"){
					?>
					<div style="display:none;"><?$APPLICATION->IncludeComponent("bitrix:sale.ajax.delivery.calculator", "", array(), null, array('HIDE_ICONS' => 'Y')); ?></div>
					<?
				}
			}else{
				?>
				<script type="text/javascript">
					top.BX('confirmorder').value = 'Y';
					top.BX('profile_change').value = 'N';
				</script>
				<?
				die();
			}
		}
	}
	?>
	</div>
</div>
<?
/*cronos*/

?>
	<script>
		$( document ).ready(function() {
			$( ".recount" ).click(function() {
				BX.ajax({
					url: '<?=$this->GetFolder();?>/tCoupon.php',
					data: {
						"promo_kod" : $("#promo_kod")[0].value
					},
					method: 'POST',
					dataType: 'html',
					timeout: 30,
					onsuccess: function(data){
						if(data == 'false')
						{
							$(".errorr").css({"display":"block"});
							$(".errorg").css({"display":"none"});
						}
						if(data != 'false')
						{
							$(".load_ajax_products").html(data);
							$(".errorr").css({"display":"none"});
							$(".errorg").css({"display":"block"});
							//submitForm();
						}
					},
					onfailure: function(){

					}
				});
			});
		});
	</script>
<?/*cronos*/?>
