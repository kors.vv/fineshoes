<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
if(!empty($_POST["promo_kod"]))
{
	CModule::IncludeModule("catalog");
	if(CCatalogDiscountCoupon::SetCoupon($_POST["promo_kod"]))
	{
        $APPLICATION->IncludeComponent(
            "bitrix:sale.basket.basket",
            "basket",
            Array(
                "ACTION_VARIABLE" => "basketAction",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "AUTO_CALCULATION" => "Y",
                "COLUMNS_LIST" => array(0=>"NAME",1=>"DELETE",2=>"PRICE",3=>"QUANTITY",4=>"SUM",),
                "COMPONENT_TEMPLATE" => "basket",
                "COUNT_DISCOUNT_4_ALL_QUANTITY" => "N",
                "GIFTS_BLOCK_TITLE" => "Выберите один из подарков",
                "GIFTS_CONVERT_CURRENCY" => "N",
                "GIFTS_HIDE_BLOCK_TITLE" => "N",
                "GIFTS_HIDE_NOT_AVAILABLE" => "N",
                "GIFTS_MESS_BTN_BUY" => "Выбрать",
                "GIFTS_MESS_BTN_DETAIL" => "Подробнее",
                "GIFTS_PAGE_ELEMENT_COUNT" => "4",
                "GIFTS_PLACE" => "BOTTOM",
                "GIFTS_PRODUCT_PROPS_VARIABLE" => "prop",
                "GIFTS_PRODUCT_QUANTITY_VARIABLE" => "undefined",
                "GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
                "GIFTS_SHOW_IMAGE" => "Y",
                "GIFTS_SHOW_NAME" => "Y",
                "GIFTS_SHOW_OLD_PRICE" => "N",
                "GIFTS_TEXT_LABEL_GIFT" => "Подарок",
                "HIDE_COUPON" => "Y",
                "OFFERS_PROPS" => array(0=>"SIZES_SHOES",1=>"SIZES_CLOTHES",2=>"COLOR_REF",),
                "PATH_TO_ORDER" => "/personal/order/make/",
                "PRICE_VAT_SHOW_VALUE" => "Y",
                "QUANTITY_FLOAT" => "N",
                "SET_TITLE" => "Y",
                "TEMPLATE_THEME" => "site",
                "USE_GIFTS" => "Y",
                "USE_PREPAYMENT" => "N"
            )
        );
	}
	else
		echo 'false';
}else echo 'false';
?>