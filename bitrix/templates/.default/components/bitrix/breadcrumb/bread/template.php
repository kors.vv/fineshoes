<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = ''; 

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()


$strReturn .= '';

$itemSize = count($arResult);
if($itemSize == 5) {

	$ii = $arResult[$itemSize - 1];
	//unset($arResult[$itemSize - 1]);

	//array_splice($arResult, 2, 0, array($ii));

}

$x = 0;

for($index = 0; $index < $itemSize; $index++)
{
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	if($title == 'Главная страница')
		$title = 'Главная';

	if($title == 'Бренды' && $x)
		continue;

	if($title == 'Бренды')
		$x++;


	$nextRef = ($index < $itemSize-2 && $arResult[$index+1]["LINK"] <> ""? ' itemref="bx_breadcrumb_'.($index+1).'"' : '');
	$child = ($index > 0? ' itemprop="child"' : '');
	$arrow = ($index > 0? '<i class="fa fa-angle-right"></i>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$strReturn .= '<a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '">' . $title . '</a> <span class="brd_r">&nbsp;&bull;&nbsp;</span>';
	}
	else
	{
		$strReturn .= '
			<span>'.$title.'</span>';
	}
}

$strReturn .= '';

return $strReturn;
