<?
/**
 * @global CMain $APPLICATION
 * @param array $arParams
 * @param array $arResult
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
?>

<h1>Личный кабинет</h1>
<div class="cabinet-block clearfix">
	<div class="cabinet-user-menu">
		<a class="active" href="/personal/private/">Личные данные</a>
		<a href="/personal/address/">Адрес доставки</a>
		<a href="/personal/orders/">Мои заказы</a>
	</div>
	<div class="cabinet-user-info">

<?ShowError($arResult["strProfileError"]);?>
<?
if ($arResult['DATA_SAVED'] == 'Y')
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));
?>
<script type="text/javascript">
<!--
var opened_sections = [<?
$arResult["opened"] = $_COOKIE[$arResult["COOKIE_PREFIX"]."_user_profile_open"];
$arResult["opened"] = preg_replace("/[^a-z0-9_,]/i", "", $arResult["opened"]);
if (strlen($arResult["opened"]) > 0)
{
	echo "'".implode("', '", explode(",", $arResult["opened"]))."'";
}
else
{
	$arResult["opened"] = "reg";
	echo "'reg'";
}
?>];
//-->

var cookie_prefix = '<?=$arResult["COOKIE_PREFIX"]?>';
</script>
<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>" enctype="multipart/form-data">
<?=$arResult["BX_SESSION_CHECK"]?>
<input type="hidden" name="lang" value="<?=LANG?>" />
<input type="hidden" name="ID" value=<?=$arResult["ID"]?> />

<input type="hidden" name="LOGIN" maxlength="50" value="<? echo $arResult["arUser"]["LOGIN"]?>" />
	<div class="cabinet-block-input">
						<label>Имя:</label>
						<input type="text"  name="NAME" maxlength="50" value="<?=$arResult["arUser"]["NAME"]?>"/>

						<label>E-mail:</label>
						<input type="text" name="EMAIL" maxlength="50" value="<? echo $arResult["arUser"]["EMAIL"]?>"  autocomplete="off"/>

						<label>Телефон:</label>
						<input type="text" name="PERSONAL_PHONE" value="<? echo $arResult["arUser"]["PERSONAL_PHONE"]?>" />
					</div>

										<div class="cabinet-block-input">
						<label>Новый пароль</label>
						<input type="password" name="NEW_PASSWORD" autocomplete="off"/>

						<label>Повторите пароль</label>
						<input type="password" name="NEW_PASSWORD_CONFIRM" autocomplete="off"/>
					</div>
					
					<div class="cabinet-block-input">
						<label>&nbsp;</label>
						<input class="submitz" type="submit" name="save" value="Сохранить" />
					</div>
					<div class="clear"></div>
	
			
			</div>
</div>