<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
if (empty($arResult["ALL_ITEMS"]))
	return;
?>
<? //var_dump($arResult["MENU_STRUCTURE"])

?>
<nav class="main_mnu_ asasa" >
    <ul class="main_mnu_ul">
                <? $number = 0;?>
		<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>
			<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
		<li data-number="<?=$number++?>">
			<a <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>class="active"<?endif?> href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?></a>
            <?
            $flag = $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["SHOW_MOBILE"];
            if($flag == "Y")
            {?>
                <ul style="display: none;">
                <?  foreach ( $arResult['TYPE_BOOTS'] as $el_id => $empty):?>
                    <li>
                        <a href="<?=$arResult["ALL_ITEMS"][$el_id]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$el_id]["TEXT"]?></a>
                    </li>
                <?endforeach;?>
                </ul>
                <?
            }?>
		</li>
		<?endforeach;?>
                <li style="display:none">
                    <a href='/catalog/brands/'>Бренды</a>
                    <ul>
                        <li>
                            <a href='javascript:;'>Англия</a>
                            <ul>
                                <?  foreach ( $arResult['COUNTRY_BOOTS']['Англия'] as $el_id => $empty):?>
                                    <li><a href="<?=$arResult["ALL_ITEMS"][$el_id]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$el_id]["TEXT"]?></a></li>
                                <?endforeach;?>
                            </ul>
                        </li>
                        <li>
                            <a href='javascript:;'>Испания</a>
                            <ul>
                                <?  foreach ( $arResult['COUNTRY_BOOTS']['Испания'] as $el_id => $empty):?>
                                    <li><a href="<?=$arResult["ALL_ITEMS"][$el_id]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$el_id]["TEXT"]?></a></li>
                                <?endforeach;?>
                            </ul>
                        </li>
                        <li>
                            <a href='javascript:;'>Португалия</a>
                            <ul>
                                <?  foreach ( $arResult['COUNTRY_BOOTS']['Португалия'] as $el_id => $empty):?>
                                    <li><a href="<?=$arResult["ALL_ITEMS"][$el_id]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$el_id]["TEXT"]?></a></li>
                                <?endforeach;?>
                            </ul>
                        </li>
                    </ul>
                </li>
				<?/*<li style="display:none">
                    <a href='/catalog/brands/'>Аксессуары</a>
                    <ul>
                        <li>
                            <a href='javascript:;'>Для обуви</a>
                            <ul>
							<?
								if(CModule::IncludeModule('iblock')){
									$arFilter = array('IBLOCK_ID' => 23, 'SECTION_ID'=>969, 'ACTIVE' => 'Y'); 
									$arSelect = array('ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL');
									$rsSection = CIBlockSection::GetTreeList($arFilter, $arSelect); 
									while($arSection = $rsSection->GetNext()) {
									  echo '<li><a href="'.$arSection['SECTION_PAGE_URL'].'">'.$arSection['NAME'].'</li>';
									}					
								}
							?>                               
                            </ul>
                        </li>
                        <li>
                            <a href='javascript:;'>Для стиля</a>
                            <ul>
                            <?
								if(CModule::IncludeModule('iblock')){
									$arFilter = array('IBLOCK_ID' => 23, 'SECTION_ID'=>968, 'ACTIVE' => 'Y'); 
									$arSelect = array('ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL');
									$rsSection = CIBlockSection::GetTreeList($arFilter, $arSelect); 
									while($arSection = $rsSection->GetNext()) {
									  echo '<li><a href="'.$arSection['SECTION_PAGE_URL'].'">'.$arSection['NAME'].'</li>';
									}					
								}
							?>
                            </ul>
                        </li>
                        <li>
                            <a href='javascript:;'>Косметика</a>
                            <ul>
                            <?
								if(CModule::IncludeModule('iblock')){
									$arFilter = array('IBLOCK_ID' => 23, 'SECTION_ID'=>930, 'ACTIVE' => 'Y'); 
									$arSelect = array('ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL');
									$rsSection = CIBlockSection::GetTreeList($arFilter, $arSelect); 
									while($arSection = $rsSection->GetNext()) {
									  echo '<li><a href="'.$arSection['SECTION_PAGE_URL'].'">'.$arSection['NAME'].'</li>';
									}					
								}
							?>
                            </ul>
                        </li>
                    </ul>
                </li>*/?>
				
	</ul>
</nav>
<div class="submenu_new">
	<div class="center wrapper">
	    <div class="conteiner_submenu_new">
	        <div>
	            <div class="type_colum">
	                <span class="type_header">Типы обуви</span>
	                <?  foreach ( $arResult['TYPE_BOOTS'] as $el_id => $empty):?>
	                    <span class="boot_element">
	                        <a href="<?=$arResult["ALL_ITEMS"][$el_id]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$el_id]["TEXT"]?></a>
	                    </span>
	                <?endforeach;?>
	            </div>
	            <div class="type_colum">
	                <span class="type_header">Бренды</span>
	                <div class="colum">
	                    <span class="country_name"><a href="/catalog/brands/english/">Англия</a></span>
	                    <?  foreach ( $arResult['COUNTRY_BOOTS']['Англия'] as $el_id => $empty):?>
	                        <span class="boot_element">
	                            <a href="<?=$arResult["ALL_ITEMS"][$el_id]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$el_id]["TEXT"]?></a>
	                        </span>
	                    <?endforeach;?>
	                </div>
	                <div class="colum">
	                    <span class="country_name"><a href="/catalog/brands/spanish/">Испания</a></span>
	                    <?  foreach ( $arResult['COUNTRY_BOOTS']['Испания'] as $el_id => $empty):?>
	                        <span class="boot_element">
	                            <a href="<?=$arResult["ALL_ITEMS"][$el_id]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$el_id]["TEXT"]?></a>
	                        </span>
	                    <?endforeach;?>
	                    <div class="item_portu">
	                        <span class="country_name">Португалия</span>
	                        <?  foreach ( $arResult['COUNTRY_BOOTS']['Португалия'] as $el_id => $empty):?>
	                            <span class="boot_element">
	                                <a href="<?=$arResult["ALL_ITEMS"][$el_id]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$el_id]["TEXT"]?></a>
	                            </span>
	                        <?endforeach;?>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <div style="clear: both"></div>
	    </div>
	</div>
    <div style="clear: both"></div>
</div>

<?if($USER->IsAdmin()){?>
<div class="submenu_new2">
	<div class="center wrapper">
	    <div class="conteiner_submenu_new">
			<div class="type_colum type-col-n">
				<span class="type_header">Для обуви</span>
				<?
				if(CModule::IncludeModule('iblock')){
					$arFilter = array('IBLOCK_ID' => 23, 'SECTION_ID'=>969, 'ACTIVE' => 'Y'); 
					$arSelect = array('ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL');
					$rsSection = CIBlockSection::GetTreeList($arFilter, $arSelect); 
					while($arSection = $rsSection->GetNext()) {
					  echo '<span class="boot_element"><a href="'.$arSection['SECTION_PAGE_URL'].'">'.$arSection['NAME'].'</a></span>';
					}					
				}
				?>
			</div>
			<div class="type_colum type-col-n">
				<span class="type_header">Для стиля</span>
				<?
				if(CModule::IncludeModule('iblock')){
					$arFilter = array('IBLOCK_ID' => 23, 'SECTION_ID'=>968, 'ACTIVE' => 'Y'); 
					$arSelect = array('ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL');
					$rsSection = CIBlockSection::GetTreeList($arFilter, $arSelect); 
					while($arSection = $rsSection->GetNext()) {
					  echo '<span class="boot_element"><a href="'.$arSection['SECTION_PAGE_URL'].'">'.$arSection['NAME'].'</a></span>';
					}					
				}
				?>
			</div>
			<div class="type_colum type-col-n">
				<span class="type_header">Косметика</span>
				<?
				if(CModule::IncludeModule('iblock')){
					$arFilter = array('IBLOCK_ID' => 23, 'SECTION_ID'=>930, 'ACTIVE' => 'Y'); 
					$arSelect = array('ID', 'NAME', 'SECTION_PAGE_URL', 'DEPTH_LEVEL');
					$rsSection = CIBlockSection::GetTreeList($arFilter, $arSelect); 
					while($arSection = $rsSection->GetNext()) {
					  echo '<span class="boot_element"><a href="'.$arSection['SECTION_PAGE_URL'].'">'.$arSection['NAME'].'</a></span>';
					}					
				}
				?>
			</div>
	    </div>
	</div>
    <div style="clear: both"></div>
</div>
<?}?>


<?if (false):?>
<nav class="main_mnu_">
	<ul>
		<?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns):?>
			<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
		<li>
			<a <?if($arResult["ALL_ITEMS"][$itemID]["SELECTED"]):?>class="active"<?endif?> href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemID]["TEXT"]?></a>
			<?if (is_array($arColumns) && count($arColumns) > 0):?><div class="submenu">
                            <div class="column"><ul>

                                <? if ($arResult["ALL_ITEMS"][$itemID]["TEXT"] == 'Обувь') echo '<li><a href="/catalog/"><strong>Типы обуви</strong></a></li>'?>

				<?foreach($arColumns as $key=>$arRow):?>
                                    <?foreach($arRow as $itemIdLevel_2=>$arLevel_3):
							if(!empty($arResult["ALL_ITEMS"][$itemIdLevel_2]['PARAMS']['UF_HIDE'])) continue;?>  <?/*!-- second level--*/?>
							<li><a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a></li>
                                    <?endforeach;?>
                                <?endforeach;?>
                                </ul>
                            </div>

						<div class="column last"><ul>
						<? if ($arResult["ALL_ITEMS"][$itemID]["TEXT"] == 'Обувь') echo '<li><a href="/catalog/brands/"><strong>Бренды</strong></a></li>'?>
					<?foreach($arColumns as $key=>$arRow):?><?foreach($arRow as $itemIdLevel_2=>$arLevel_3):
							if(empty($arResult["ALL_ITEMS"][$itemIdLevel_2]['PARAMS']['UF_HIDE'])) continue;
						?>
						<li><a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a></li>
						<?endforeach;?>	<?endforeach;?>		</ul></div>
				</div>			<?endif?>
		</li>
		<?endforeach;?>
	</ul>
</nav>

<? endif; ?>
