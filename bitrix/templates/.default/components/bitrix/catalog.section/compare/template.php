<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!empty($arResult['ITEMS']))
{
	?>
	<div class="compare_page overflow">
	<div class="catalog vitrina clearfix">
		<div class="recom_catalog trans_bg">
			<div class="compare_left_spec">
				<div class="compare-control">
					<div class="compare-btns">
						<a class="add-to-compare" href="/catalog/">Добавить товар</a>
						<br/>
						<a class="remove-compare" href="/catalog/compare/?action=clearcompare">Очистить список</a>
					</div>
					<div class="compare-params-btns">
						<a class="all-params active" href="#">Все характеристики</a>
						<br/>
						<a class="differens-params" href="#">Только различия</a>
					</div>
				</div>
				<table class="compare_table">
					<tbody>
						<tr class="field_brand">
							<td>Бренд: </td>
						</tr>
						<tr class="field_fitting">
							<td>Полнота: </td>
						</tr>
						<tr class="field_last">
							<td>Колодка: </td>
						</tr>
						<tr class="field_sole">
							<td>Подошва: </td>
						</tr>
						<tr class="field_heel">
							<td>Каблук: </td>
						</tr>
						<tr class="field_construction">
							<td>Конструкция: </td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="scrollLeft">
				<div class="leftOffset_compare">
	<? $templateLibrary = array('popup');
	$currencyList = '';
	if (!empty($arResult['CURRENCIES']))
	{
		$templateLibrary[] = 'currency';
		$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
	}
	$templateData = array(
		'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
		'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
		'TEMPLATE_LIBRARY' => $templateLibrary,
		'CURRENCIES' => $currencyList
	);
	unset($currencyList, $templateLibrary);

	$skuTemplate = array();
	if (!empty($arResult['SKU_PROPS']))
	{
		foreach ($arResult['SKU_PROPS'] as $arProp)
		{
			$propId = $arProp['ID'];
			$skuTemplate[$propId] = array(
				'SCROLL' => array(
					'START' => '',
					'FINISH' => '',
				),
				'FULL' => array(
					'START' => '',
					'FINISH' => '',
				),
				'ITEMS' => array()
			);
			$templateRow = '';
			if ('TEXT' == $arProp['SHOW_MODE'])
			{
				$skuTemplate[$propId]['SCROLL']['START'] = '<div class="bx_item_detail_size full" id="#ITEM#_prop_'.$propId.'_cont">'.
					'<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
					'<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';;
				$skuTemplate[$propId]['SCROLL']['FINISH'] = '</ul></div>'.
					'<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style=""></div>'.
					'<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style=""></div>'.
					'</div></div>';

				$skuTemplate[$propId]['FULL']['START'] = '<div class="bx_item_detail_size" id="#ITEM#_prop_'.$propId.'_cont">'.
					'<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
					'<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';;
				$skuTemplate[$propId]['FULL']['FINISH'] = '</ul></div>'.
					'<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style="display: none;"></div>'.
					'<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style="display: none;"></div>'.
					'</div></div>';
				foreach ($arProp['VALUES'] as $value)
				{
					$value['NAME'] = htmlspecialcharsbx($value['NAME']);
					$skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="'.$propId.'_'.$value['ID'].
						'" data-onevalue="'.$value['ID'].'" style="width: #WIDTH#;" title="'.$value['NAME'].'"><i></i><span class="cnt">'.$value['NAME'].'</span></li>';
				}
				unset($value);
			}
			elseif ('PICT' == $arProp['SHOW_MODE'])
			{
				$skuTemplate[$propId]['SCROLL']['START'] = '<div class="bx_item_detail_scu full" id="#ITEM#_prop_'.$propId.'_cont">'.
					'<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
					'<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';
				$skuTemplate[$propId]['SCROLL']['FINISH'] = '</ul></div>'.
					'<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style=""></div>'.
					'<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style=""></div>'.
					'</div></div>';

				$skuTemplate[$propId]['FULL']['START'] = '<div class="bx_item_detail_scu" id="#ITEM#_prop_'.$propId.'_cont">'.
					'<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
					'<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';
				$skuTemplate[$propId]['FULL']['FINISH'] = '</ul></div>'.
					'<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style="display: none;"></div>'.
					'<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style="display: none;"></div>'.
					'</div></div>';
				foreach ($arProp['VALUES'] as $value)
				{
					$value['NAME'] = htmlspecialcharsbx($value['NAME']);
					$skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="'.$propId.'_'.$value['ID'].
						'" data-onevalue="'.$value['ID'].'" style="width: #WIDTH#; padding-top: #WIDTH#;"><i title="'.$value['NAME'].'"></i>'.
						'<span class="cnt"><span class="cnt_item" style="background-image:url(\''.$value['PICT']['SRC'].'\');" title="'.$value['NAME'].'"></span></span></li>';
				}
				unset($value);
			}
		}
		unset($templateRow, $arProp);
	}

	if ($arParams["DISPLAY_TOP_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

	if($arParams['HIDE_SECTION_DESCRIPTION'] !== 'Y')
	{ ?>
<? } ?>

					<div class="fixed_top_compare">
						<div class="fixed_top_compare_in">
	<?
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'STICKER_ID' => $strMainID.'_sticker',
		'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',

		'PRICE' => $strMainID.'_price',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	);

	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);
	$imgTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $arItem['NAME']
	);

	$minPrice = false;
	if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
		$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);

	?>

							<div class="compare_item catalog_item_list item<?=$arItem['ID']?>">
								<div class="catalog_item_mobile_title"></div>
								<div class="catalog_item_mobile_">
									<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="catalog_item_img_link">
										<div class="img">
											<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arItem['NAME']?>">
										</div>
									</a>
									<div class="catalog_item_mobile_info"></div>
								</div>
								<div class="catalog_item_mobile_ catalog_item_mobile_btn"></div>
								<div class="catalog_item_no_mobile">
									<div class="ttl"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></div>
									<div class="display_flex_order">
										<? if(!empty($arItem['DISPLAY_PROPERTIES']['PRICE_BEFORE']['VALUE'])) { ?>
											<div class="price w_old">
												<? echo $minPrice['VALUE']; ?><span class="rur">i</span>
												<br/>
												<span class="oldprice"><?=$arItem['DISPLAY_PROPERTIES']['PRICE_BEFORE']['VALUE']?><span class="rur">i</span></span>
											</div>
										<? } else { ?>
											<div class="price">
												<? echo $minPrice['VALUE']; ?><ruble><span class="text">руб.</span></ruble>
											</div>
										<? } ?>
										<a class="basket icon_basket icon_basket_text basketajax js-add-basket" href="" data-id="<?=$arItem['ID']?>" title="Добавить в корзину"></a>
									</div>
								</div>
								<a class="remove" href="" data-id="<?=$arItem['ID']?>">Убрать</a>
								<div class="loader loader_active"></div>
							</div>
	<?
}
?>

						</div>
					</div>
					<div class="clear"></div>
					<?
foreach ($arResult['ITEMS'] as $key => $arItem)
{

	?>
					<div class="compare_item_teh item<?=$arItem['ID']?>">
						<table class="compare_table preEmpty">
							<tbody>
								<tr class="field_brand">
									<td><?if(!empty($arItem['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE']))  echo $arItem['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE']; else echo '&nbsp;';?></td>
								</tr>
								<tr class="field_fitting">
									<td><?if(!empty($arItem['DISPLAY_PROPERTIES']['FITTING']['VALUE']))  echo $arItem['DISPLAY_PROPERTIES']['FITTING']['VALUE']; else echo '&nbsp;';?></td>
								</tr>
								<tr class="field_last">
									<td><?if(!empty($arItem['DISPLAY_PROPERTIES']['LAST']['VALUE']))  echo $arItem['DISPLAY_PROPERTIES']['LAST']['VALUE']; else echo '&nbsp;';?></td>
								</tr>
								<tr class="field_sole">
									<td><?if(!empty($arItem['DISPLAY_PROPERTIES']['SOLE']['DISPLAY_VALUE']))  echo $arItem['DISPLAY_PROPERTIES']['SOLE']['DISPLAY_VALUE']; else echo '&nbsp;';?></td>
								</tr>
								<tr class="field_heel">
									<td><?if(!empty($arItem['DISPLAY_PROPERTIES']['HEEL']['DISPLAY_VALUE']))  echo $arItem['DISPLAY_PROPERTIES']['HEEL']['DISPLAY_VALUE']; else echo '&nbsp;';?></td>
								</tr>
								<tr class="field_construction">
									<td><?if(!empty($arItem['DISPLAY_PROPERTIES']['CONSTRUCTION']['DISPLAY_VALUE']))  echo $arItem['DISPLAY_PROPERTIES']['CONSTRUCTION']['DISPLAY_VALUE']; else echo '&nbsp;';?></td>
								</tr>
							</tbody>
						</table>
					</div>
<? } ?>
<script type="text/javascript">
BX.message({
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
	BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
<?
	if ($arParams["DISPLAY_BOTTOM_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}
}?>
				</div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<br/><br/> </div>

		<div class="oneclickwrap">
			<div id="oneclickformID" class="oneclickform">
		        <form action="" method="post">
		            <input type="hidden" name="action" value="order1click" />
		            <input type="hidden" name="item" value="" />
		            <input type="hidden" name="count" value="1" />
		            <div class="ttl_click"></div>
		            <div class="ajax_errors"></div>
		            <div class="click_input">
		            	<label>Ваше имя</label>
		            	<input type="text" name="name" value="" class="text" />
					</div>
					<div class="click_input">
		            	<label>Телефон</label>
		            	<input type="text" name="phone" value="" class="text" />
		            </div>
		            <div class="align_center">
		            	<button type="submit" class="sub_pop submitz" />Купить</button>
<p>При отправке данных, вы принимаете условия <a href="/politika-konfidencialnosti/">пользовательского соглашения</a></p>
		            </div>
		        </form>
		    </div>
		</div>
