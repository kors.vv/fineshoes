<div class="catalog addproduct">
	<div class="h2">Похожие товары:</div>
	<br/>
<div class="catalog_list">
	<?
	$elementEdit = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_EDIT');
	$elementDelete = CIBlock::GetArrayByID($arParams['IBLOCK_ID'], 'ELEMENT_DELETE');
	$elementDeleteParams = array('CONFIRM' => GetMessage('CATALOG_RECOMMENDED_PRODUCTS_TPL_ELEMENT_DELETE_CONFIRM'));
	foreach ($arResult['ITEMS'] as $key => $arItem)
	{
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $elementEdit);
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $elementDelete, $elementDeleteParams);
		$strMainID = $this->GetEditAreaId($arItem['ID']);

		$arItemIDs = array(
			'ID' => $strMainID,
			'PICT' => $strMainID . '_pict',
			'SECOND_PICT' => $strMainID . '_secondpict',
			'MAIN_PROPS' => $strMainID . '_main_props',

			'QUANTITY' => $strMainID . '_quantity',
			'QUANTITY_DOWN' => $strMainID . '_quant_down',
			'QUANTITY_UP' => $strMainID . '_quant_up',
			'QUANTITY_MEASURE' => $strMainID . '_quant_measure',
			'BUY_LINK' => $strMainID . '_buy_link',
			'SUBSCRIBE_LINK' => $strMainID . '_subscribe',

			'PRICE' => $strMainID . '_price',
			'DSC_PERC' => $strMainID . '_dsc_perc',
			'SECOND_DSC_PERC' => $strMainID . '_second_dsc_perc',

			'PROP_DIV' => $strMainID . '_sku_tree',
			'PROP' => $strMainID . '_prop_',
			'DISPLAY_PROP_DIV' => $strMainID . '_sku_prop',
			'BASKET_PROP_DIV' => $strMainID . '_basket_prop'
		);

		$strObName = 'ob' . preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

		$strTitle = (
		isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]) && '' != isset($arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"])
			? $arItem["IPROPERTY_VALUES"]["ELEMENT_PREVIEW_PICTURE_FILE_TITLE"]
			: $arItem['NAME']
		);
		$showImgClass = $arParams['SHOW_IMAGE'] != "Y" ? "no-imgs" : "";

		?>


		<div class="catalog_item_list">
			<a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>">
				<div class="img" style="background-image:url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>')"></div>
			</a>
			<div class="ttl"><a href="<? echo $arItem['DETAIL_PAGE_URL']; ?>"><?=$arItem['NAME']?></a></div>
			<div class="price"><?=$arItem['MIN_PRICE']['VALUE']?><span class="rur">i</span></div>
			<a class="basket basketajax in_item" data-id="<?=$arItem['ID']?>">В корзину</a>
			<a class="click1click in_list_js fancy" href="#oneclickformID" ttl="<?=$arItem['NAME']?>" rel="<?=$arItem['NAME']?>">Купить в 1 клик</a>
			<div class="dop_abs"><?

$reviews = CIBlockElement::GetList(
	Array("ID"=>"DESC"),
	Array('ACTIVE' => 'Y', 'IBLOCK_ID' => 7, 'PROPERTY_PRODUCT' => $arItem['ID']),
	false,
	false,
	array('NAME', 'IBLOCK_ID', 'PROPERTY_NAME', 'PROPERTY_COMMENT','PROPERTY_DIGNITY','PROPERTY_DISAD')
);

?>
				<p><a href="#open_comments">Отзывы</a>: (<?=$reviews->result->num_rows?>)</p>
				<?if(!empty($arItem['PROPERTIES']['RATING']['VALUE'])) { ?><p class="security sec_<?=$arItem['PROPERTIES']['RATING']['VALUE']?>">Надежность</p><? } ?>
				<?if(!empty($arItem['PROPERTIES']['WEIGHT']['VALUE'])) { ?><p class="">Вес: <?=$arItem['PROPERTIES']['WEIGHT']['VALUE']?> гр.</p><? } ?>
			</div>
		</div>


		<?
	}
	unset($elementDeleteParams, $elementDelete, $elementEdit);
	?>


		<div class="clear"></div>
	</div>
</div>
<div class="clear"></div>

	<script type="text/javascript">
		BX.message({
			MESS_BTN_BUY: '<? echo ('' != $arParams['MESS_BTN_BUY'] ? CUtil::JSEscape($arParams['MESS_BTN_BUY']) : GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_TPL_MESS_BTN_BUY')); ?>',
			MESS_BTN_ADD_TO_BASKET: '<? echo ('' != $arParams['MESS_BTN_ADD_TO_BASKET'] ? CUtil::JSEscape($arParams['MESS_BTN_ADD_TO_BASKET']) : GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_TPL_MESS_BTN_ADD_TO_BASKET')); ?>',

			MESS_BTN_DETAIL: '<? echo ('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_TPL_MESS_BTN_DETAIL')); ?>',

			MESS_NOT_AVAILABLE: '<? echo ('' != $arParams['MESS_BTN_DETAIL'] ? CUtil::JSEscape($arParams['MESS_BTN_DETAIL']) : GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_TPL_MESS_BTN_DETAIL')); ?>',
			BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
			BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
			ADD_TO_BASKET_OK: '<? echo GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_ADD_TO_BASKET_OK'); ?>',
			TITLE_ERROR: '<? echo GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_CATALOG_TITLE_ERROR') ?>',
			TITLE_BASKET_PROPS: '<? echo GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_CATALOG_TITLE_BASKET_PROPS') ?>',
			TITLE_SUCCESSFUL: '<? echo GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_ADD_TO_BASKET_OK'); ?>',
			BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
			BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
			BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CATALOG_RECOMMENDED_PRODUCTS_CATALOG_BTN_MESSAGE_CLOSE') ?>'
		});
	</script>


				<div class="oneclickwrap">
			<div id="oneclickformID" class="oneclickform">
		        <form action="" method="post">
		            <input type="hidden" name="action" value="order1click" />
		            <input type="hidden" name="item" value="1119" />
		            <input type="hidden" name="count" value="1" />
		            <div class="ttl_click"></div>
		            <div class="ajax_errors"></div>
		            <div class="click_input">
		            	<label>Ваше имя</label>
		            	<input type="text" name="name" value="" class="text" />
					</div>
					<div class="click_input">
		            	<label>Телефон</label>
		            	<input type="text" name="phone" value="" class="text" />
		            </div>
		            <div class="align_center">
		            	<button type="submit" class="sub_pop submitz" />Купить</button>
		            </div>
		        </form>
<p>При отправке данных, вы принимаете условия <a href="/politika-konfidencialnosti/">пользовательского соглашения</a></p>
		    </div>
		</div>
