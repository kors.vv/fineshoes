<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="bx-auth">
<h1>Восстановление пароля</h1>
<?
ShowMessage($arParams["~AUTH_RESULT"]);


/*

		<h1>Восстановление пароля</h1>
	<div class="small-text-block">
		<p>Новый пароль был успешно сгенерирован, и информация, необходимая для доступа к нам на сайт, отправлена на Ваш e-mail.</p>
		<p>Если Вы по какой-то причине не получили письмо, пожалуйста, напишите нам на адрес <a href="mailto:mail@bilelock.ru">mail@bilelock.ru</a></p>
		</div>
		*/
?>

<?
	
	$arResult['NEWPASSWORD'] = mb_substr(md5(time()), 0, 6);
	
	
	
	global $USER;
	$r = $USER->ChangePassword($arResult['LAST_LOGIN'], $arResult['USER_CHECKWORD'], $arResult['NEWPASSWORD'], $arResult['NEWPASSWORD'], 's1');
	
	if($r['TYPE'] == 'OK') {
		
	$rsUser = CUser::GetByLogin($arResult['LAST_LOGIN']);
	$arUser = $rsUser->Fetch();
	
	$fields = array(
		'LOGIN' => $arResult['LAST_LOGIN'],
		'PASSWORD' => $arResult['NEWPASSWORD'],
		'EMAIL' => $arUser['EMAIL']
	);
	
	CEvent::SendImmediate(
		'GET_PASSWORD',
		's1',
		$fields,
		'N');
		
		?>
	
	
	
	<div class="small-text-block">
		<p>Новый пароль был успешно сгенерирован, и информация, необходимая для доступа к нам на сайт, отправлена на Ваш e-mail.</p>
		<p>Если Вы по какой-то причине не получили письмо, пожалуйста, напишите нам на адрес <a href="mailto:mail@bikelock.ru">mail@bikelock.ru</a></p>
	</div>
	<?} else {
?>

<form method="post" action="<?=$arResult["AUTH_FORM"]?>" name="bform">
	<?if (strlen($arResult["BACKURL"]) > 0): ?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<? endif ?>
	<input type="hidden" name="AUTH_FORM" value="Y">
	<input type="hidden" name="TYPE" value="CHANGE_PWD">
	<table class="data-table bx-changepass-table">
		<tbody>
			<tr>
				<td><span class="starrequired">*</span><?=GetMessage("AUTH_LOGIN")?></td>
				<td><input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" class="bx-auth-input" /></td>
			</tr>
			<tr>
				<td><span class="starrequired">*</span><?=GetMessage("AUTH_CHECKWORD")?></td>
				<td><input type="text" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" class="bx-auth-input" /></td>
			</tr>
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td><input type="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>" /></td>
			</tr>
		</tfoot>
	</table>


	</form><? } ?>

<script type="text/javascript">
document.bform.USER_LOGIN.focus();
</script>
</div>