<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<div class="subscribe-form"  id="subscribe-form">
    <?
    $frame = $this->createFrame("subscribe-form", false)->begin();
    ?>
    <form action="<?=$arResult["FORM_ACTION"]?>">
        <div style="display: none;">
        <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
            <label for="sf_RUB_ID_<?=$itemValue["ID"]?>">
                <input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>"<?if($itemValue["CHECKED"]) echo " checked"?> /> <?=$itemValue["NAME"]?>
            </label><br />
        <?endforeach;?>
        </div>
        <table border="0" cellspacing="0" cellpadding="2" align="center">
            <tr>
                <td>
                    <input class="subscribe-email" type="text" placeholder="E-mail" name="sf_EMAIL" size="20" value="<?=$arResult["EMAIL"]?>" title="<?=GetMessage("subscr_form_email_title")?>" />
                </td>
                <td align="right">
                    <input class="btn-black" type="submit" name="OK" value="Подписаться" />
                </td>
            </tr>
        </table>
    </form>
    <?
    $frame->beginStub();
    ?>
    <form action="<?=$arResult["FORM_ACTION"]?>">

        <?foreach($arResult["RUBRICS"] as $itemID => $itemValue):?>
            <label for="sf_RUB_ID_<?=$itemValue["ID"]?>">
                <input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_<?=$itemValue["ID"]?>" value="<?=$itemValue["ID"]?>" /> <?=$itemValue["NAME"]?>
            </label><br />
        <?endforeach;?>

        <table border="0" cellspacing="0" cellpadding="2" align="center">
            <tr>
                <td><input type="text" name="sf_EMAIL" size="20" value="" title="<?=GetMessage("subscr_form_email_title")?>" /></td>
            </tr>
            <tr>
                <td align="right"><input class="btn-black" type="submit" name="OK" value="Подписаться" /></td>
            </tr>
        </table>
    </form>
    <?
    $frame->end();
    ?>
    <p class="agreement">Подписываясь на рассылку, я принимаю<br><a href="/politika-konfidencialnosti/" target="_blank">пользовательское соглашение</a></p>
</div>
