<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$templateLibrary = array('popup');
$currencyList = '';
if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
	'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList
);
unset($currencyList, $templateLibrary);

$strMainID = $this->GetEditAreaId($arResult['ID']);

$arItemIDs = array(
	'ID' => $strMainID,
	'PICT' => $strMainID.'_pict',
	'DISCOUNT_PICT_ID' => $strMainID.'_dsc_pict',
	'STICKER_ID' => $strMainID.'_sticker',
	'BIG_SLIDER_ID' => $strMainID.'_big_slider',
	'BIG_IMG_CONT_ID' => $strMainID.'_bigimg_cont',
	'SLIDER_CONT_ID' => $strMainID.'_slider_cont',
	'SLIDER_LIST' => $strMainID.'_slider_list',
	'SLIDER_LEFT' => $strMainID.'_slider_left',
	'SLIDER_RIGHT' => $strMainID.'_slider_right',
	'OLD_PRICE' => $strMainID.'_old_price',
	'PRICE' => $strMainID.'_price',
	'DISCOUNT_PRICE' => $strMainID.'_price_discount',
	'SLIDER_CONT_OF_ID' => $strMainID.'_slider_cont_',
	'SLIDER_LIST_OF_ID' => $strMainID.'_slider_list_',
	'SLIDER_LEFT_OF_ID' => $strMainID.'_slider_left_',
	'SLIDER_RIGHT_OF_ID' => $strMainID.'_slider_right_',
	'QUANTITY' => $strMainID.'_quantity',
	'QUANTITY_DOWN' => $strMainID.'_quant_down',
	'QUANTITY_UP' => $strMainID.'_quant_up',
	'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
	'QUANTITY_LIMIT' => $strMainID.'_quant_limit',
	'BASIS_PRICE' => $strMainID.'_basis_price',
	'BUY_LINK' => $strMainID.'_buy_link',
	'ADD_BASKET_LINK' => $strMainID.'_add_basket_link',
	'BASKET_ACTIONS' => $strMainID.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
	'COMPARE_LINK' => $strMainID.'_compare_link',
	'PROP' => $strMainID.'_prop_',
	'PROP_DIV' => $strMainID.'_skudiv',
	'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
	'OFFER_GROUP' => $strMainID.'_set_group_',
	'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
);
$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);
$templateData['JS_OBJ'] = $strObName;

$strTitle = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_TITLE"]
	: $arResult['NAME']
);
$strAlt = (
	isset($arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]) && $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"] != ''
	? $arResult["IPROPERTY_VALUES"]["ELEMENT_DETAIL_PICTURE_FILE_ALT"]
	: $arResult['NAME']
);
global $USER;
// if ($USER->IsAdmin()){
//     echo '<pre style= "display:none">';
//     var_dump($arResult);
//     echo '</pre>';

// }
?><div class="catalog_product clearfix" id="<? echo $arItemIDs['ID']; ?>" itemscope itemtype="http://schema.org/Product">
<?
reset($arResult['MORE_PHOTO']);
$arFirstPhoto = current($arResult['MORE_PHOTO']);
?>
<h2 class="noborder single_product_title_mobile"><? if(!empty($arResult['DISPLAY_PROPERTIES']['TITLE']['VALUE']))  echo $arResult['DISPLAY_PROPERTIES']['TITLE']['VALUE']; else echo $arResult['NAME'];?></h2>
<div class="catalog_item_left">
	<div class="img_item">
		<? //$img_cropped = CFile::GetFileArray($arResult['PROPERTIES']['PHOTOS_CROPPED']['VALUE']); var_dump($img_cropped); ?>
		<ul class="product-bxslider">

			<? if(!empty($arResult['PROPERTIES']["PHOTOS"]['VALUE'])) {
$i=0;
				foreach($arResult['PROPERTIES']['PHOTOS']['VALUE'] as $img) {

$img_cropped = CFile::GetFileArray($arResult['PROPERTIES']['PHOTOS_CROPPED']['VALUE'][$i]);
					$img2 = CFile::GetFileArray($img);
					?>
				<li><a class="fancy gallery" href="<?=$img2['SRC']?>" rel="catalog-images-item-group"><img itemprop="image" src="<?=$img_cropped['SRC']; ?>" alt="<?=$arResult['NAME']?>" /></a></li>
			<?	$i++; }} ?>
		</ul>
			<? if(!empty($arResult['DISPLAY_PROPERTIES']['HIT']['VALUE'])) { ?> <div class="label hit"></div><? } ?>
			<? if(!empty($arResult['DISPLAY_PROPERTIES']['NEW']['VALUE'])) { ?> <div class="label new"></div><? } ?>
	</div>
</div>
<div class="catalog_item_right">
	<h1 itemprop="name" class="noborder single_product_title"><? if(!empty($arResult['DISPLAY_PROPERTIES']['TITLE']['VALUE']))  echo $arResult['DISPLAY_PROPERTIES']['TITLE']['VALUE']; else echo $arResult['NAME'];?></h1>
        <?//if ($USER->IsAdmin()):?>
        <div class="catalog_item_right_dop_info">
            <div class="catalog_item_right_props">
		<? if(!empty($arResult['PROPERTIES']['FITTING']['VALUE'])) { ?>
			<p>Полнота: <?=$arResult['PROPERTIES']['FITTING']['VALUE'];?></p>
                <? }else{ echo '&nbsp;';} ?>
		<? if(!empty($arResult['PROPERTIES']['SOLE']['VALUE'])) { ?>
			<p>Подошва: <?=$arResult['PROPERTIES']['SOLE']['VALUE'];?></p>
		<? }else{ echo '&nbsp;';} ?>
            </div>
            <? if (isset($arResult["PROPERTIES"]["SIZE"]) and is_array($arResult["PROPERTIES"]["SIZE"]["VALUE_ENUM_ID"]) ):?>
            <span class="catalog_item_right_size_span">Размер:</span>
            <div class="catalog_item_right_size">
                <span>Размер:</span>
                <select class="choose_size" data-idp="<?=$arResult['ID']?>" data-id="select_size_<?=$arResult['ID']?>">
                    <?foreach ( $arResult["PROPERTIES"]["SIZE"]["VALUE_ENUM_ID"] as $k => $v ):?>
                        <option data-id="<?=$v?>" <?=$arResult["PROPERTIES"]["BAZE_SIZE"]["VALUE"] == $arResult["PROPERTIES"]["SIZE"]["VALUE"][$k]? 'selected': ''?>><?=$arResult["PROPERTIES"]["SIZE"]["VALUE"][$k]?></option>
                    <?endforeach;?>
                </select>
            </div>
            <div class="catalog_item_right_table_size">
                <span class="icon_table_size"><img src="<?=SITE_TEMPLATE_PATH?>/images/shoe_icon.png" /></span>
                <a class="fancy a_table_size" href="<?=SITE_TEMPLATE_PATH?>/images/table_size.png">Размерная сетка</a>
            </div>
            <?endif;?>
            <div style="clear: both;"></div>
	</div>
        <?//endif;?>
	<? $minPrice = (isset($arResult['RATIO_PRICE']) ? $arResult['RATIO_PRICE'] : $arResult['MIN_PRICE']); ?>
	<div class="price_block clearfix"  itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<? if(!empty($arResult['DISPLAY_PROPERTIES']['PRICE_BEFORE']['VALUE'])) { ?>
		<div class="price">
			<table><tr><td>
			<span class="oldprice"><?=$arResult['DISPLAY_PROPERTIES']['PRICE_BEFORE']['VALUE']?><ruble><span class="text">руб.</span></ruble></span>
			<span class="pricetxt">Цена: </span> <span class="w_old" itemprop="price"><?=$minPrice['VALUE']?><ruble><span class="text">руб.</span></ruble></span>
			</td></tr></table>
		</div>
		<? } else { ?>
		<div class="price">
			<table><tr><td>
				<span class="pricetxt">Цена: </span> <span itemprop="price"  itemprop="price"><?=$minPrice['VALUE']?><ruble><span class="text">руб.</span></ruble></span>
			</td></tr></table>
		</div>
		<? } ?>
		<meta itemprop="priceCurrency" content="RUB">
		<div class="basket_btn">
			<a class="basket basketajax in_item js-add-basket basket-buy-cart" data-id="<?=$arResult['ID']?>" onclick="yaCounter47085678.reachGoal('vkorziny'); return true;">В корзину</a>

			<a class="click1click in_item_js" href="#" onclick="yaCounter47085678.reachGoal('zakazat-1klik'); return true;">Купить в 1 клик</a>
						<div class="oneclickwrap">
							<div id="open_1click_1615" class="oneclickform">
						    	<a class="close" href="#"></a>
						        <form action="" method="post">
						            <input type="hidden" name="action" value="order1click" />
						            <input type="hidden" name="item" value="<? if(!empty($arResult['DISPLAY_PROPERTIES']['TITLE']['VALUE']))  echo $arResult['DISPLAY_PROPERTIES']['TITLE']['VALUE']; else echo $arResult['NAME'];?>" />
						            <input type="hidden" name="count" value="1" />
						            <div class="ttl_click"><? if(!empty($arResult['DISPLAY_PROPERTIES']['TITLE']['VALUE']))  echo $arResult['DISPLAY_PROPERTIES']['TITLE']['VALUE']; else echo $arResult['NAME'];?></div>
									<div class="ajax_errors"></div>
						            <div class="click_input">
						            	<label>Ваше имя</label>
						            	<input type="text" name="name" value="" class="text" />
									</div>
									<div class="click_input">
						            	<label>Телефон</label>
						            	<input type="text" name="phone" value="" class="text" />
						            </div>

						            <div class="align_center">
<!--<script type="text/javascript">
            jQuery(document).ready(function(){
	    jQuery('.sub_pop submitz').click(function() {
			    yaCounter47085678.reachGoal('i_one_click');
		      });
	    });
            </script>-->
						            	<button onclick="yaCounter47085678.reachGoal('zakazat-1klik-step2'); return true;" type="submit" class="sub_pop submitz" />Купить</button>
						            </div><br>
							<p class="text_conf_1click">При отправке данных, вы принимаете условия <a href="/politika-konfidencialnosti/" target="_blank">пользовательского соглашения</a></p>
						        </form>
						    </div>

						</div>
		</div>
	</div>

	<div class="single_product_center">
		<? if(!empty($arResult['PROPERTIES']['PHOTOS']['VALUE'])) { ?>
		<div class="imgs_item">
        <? $i=0; ?>
			<? foreach($arResult['PROPERTIES']['PHOTOS']['VALUE'] as $k => $img) {
				$img = CFile::GetFileArray($img);
				if(!empty($arResult['PROPERTIES']['PHOTOS_SMALL']['VALUE'][$k] ))
					$small = CFile::GetFileArray($arResult['PROPERTIES']['PHOTOS_SMALL']['VALUE'][$k]);
				else
					$small = $img;
			?>
				<a <?if($i==0){echo 'style="display: none;"';}?> class="fancy gallery" href="<?=$img['SRC']?>" rel="catalog-images-item-group"><img src="<?=$small['SRC']?>" alt="<?=$arResult['NAME']?>" /></a>
			<? $i++; } ?>
		</div>
		<? } ?>
	</div>

	<a class="click_deshevle fancybox_deshevle" href="#">Нашли дешевле?<br>Снизим цену!</a>
	<a class="compare_a compareajax js-add-favorite" data-id="<?=$arResult['ID']?>" title="Сравнить">Сравнить</a>
	<?

$discount = CSaleDiscount::GetByID(2);
$is = false;
$db_old_groups = CIBlockElement::GetElementGroups($arResult['ID']);
while($ar_group = $db_old_groups->Fetch()) {

    if($ar_group["ID"] == 44)
		$is = true;

    if($ar_group["ID"] == 142)
		$is = true;

    if($ar_group["ID"] == 143)
		$is = true;

    if($ar_group["ID"] == 144)
		$is = true;

    if($ar_group["ID"] == 141)
		$is = true;

}
if(!empty($discount) && $is && $arResult['PROPERTIES']['BRAND']['VALUE'] == 'kryptonite' &&
$arResult['ID'] != 17894 && $arResult['ID'] != 17792 && $arResult['ID'] != 17689) {
	?>
	<br/>
	<div class="h2">Вместе выгодно:</div>
	<div class="tog_wrap">
		<span class="tog_plus">&#10010;</span>
		<a class="tog_item item_id<?=$arResult['kripto']['id']?>" href="/catalog/cables/kryptonite-kryptoflex-410-double-loop-cable/">
			<img src="/upload/iblock/531/53110d63e44f655cb03d2760d109702b.jpg" alt="Кабель Kryptonite KryptoFlex 410 Double Loop Cable" />
			<!--<img src="<?=SITE_TEMPLATE_PATH?>/images/kabel_kryptonite_kryptoflex_410_double_loop_cable_57e790_2.jpg" alt="Кабель Kryptonite KryptoFlex 410 Double Loop Cable" />-->
			<p class="tog_title"><?=$arResult['kripto']['name']?> <strong><?=$arResult['kripto']['price']?> <ruble><span class="text" style="margin-left:-3px!important;"> руб.</span></ruble></strong></p>
		</a>
		<div class="tog_ravno">&#61;</div>
		<div class="tog_summ">
			<span class="tog_summ_sale">
				<span class="tog_summ_old"><?=($minPrice['VALUE'] + $arResult['kripto']['price'])?><ruble><span class="text"> руб.</span></ruble></span>
				<div>
					<?=($minPrice['VALUE'] + $arResult['kripto']['price'] - 500)?><ruble><span class="text"> руб.</span></ruble>
				</div>
			</span>
		</div>
		<div class="tog_addtobasket">
			<a class="basket ajx js-basket" href="" data-id="<?=$arResult['ID']?>" data-addid="18291">В корзину</a>
		</div>	</div>	<?
}
   ?>
</div>
<div class="clear"></div>
<?if( isset($arResult["OTHER_COLOR"]) ):?>
<div class="catalog_item_left block_other_color">
    Другие цвета:<br/>
    <?  foreach ($arResult["OTHER_COLOR"] as $other_color ):?>
        <a class="other_color" href="<?=$other_color['URL']?>" title="<?=$other_color['NAME']?>">
            <img src="<?=$other_color['SRC']?>" alt="<?=$other_color['NAME']?>" />
        </a>
    <?  endforeach;?>

</div>
<?endif;?>
<div class="clear"></div>
<div class="cat_item_text">
<?

$reviews = CIBlockElement::GetList(
	Array("ID"=>"DESC"),
	Array('ACTIVE' => 'Y', 'IBLOCK_ID' => 7, 'PROPERTY_PRODUCT' => $arResult['ID']),
	false,
	false,
	array('NAME', 'IBLOCK_ID', 'PROPERTY_NAME', 'PROPERTY_COMMENT','PROPERTY_DIGNITY','PROPERTY_DISAD')
);

?>
	<div class="tabs_wrap clearfix">
		<div class="tabs">
			<div class="tabs_arrow_left"></div>
			<div class="tabs_inner">
				<? if(!empty($arResult['PREVIEW_TEXT']) || count($arResult["TABLE_PROPERTY_CODE"])) { ?>
					<span class="active">Описание</span>
				<? } ?>
				<? if(!empty($arResult['DETAIL_TEXT'])) { ?>
					<span>Технические характеристики</span>
				<? } ?>
				<span>Доставка</span>
				<span>Оплата</span>
				<span>Гарантия и возврат</span>
				<span class="com_tab">Отзывы (<?=$reviews->result->num_rows?>)</span>
			</div>
			<div class="tabs_arrow_right"></div>
		</div>

		<? if(!empty($arResult['PREVIEW_TEXT']) || count($arResult["TABLE_PROPERTY_CODE"])) { ?>
		<div class="tabs_content active clearfix" itemprop="description">
			<?=$arResult['PREVIEW_TEXT']?>

			<? if(count($arResult["TABLE_PROPERTY_CODE"])) { ?>
				<h3>Характеристики:</h3>
				<table cellpadding="5" cellspacing="1" rules="rows" class="table-property">
				<? foreach($arResult["TABLE_PROPERTY_CODE"] as $name => $value) { ?>
					<tr>
						<th>
							<?=$name?>
						</th>
						<td>
							<?=$value?>
						</td>
					</tr>
				<? } ?>
				</table>
			<? } ?>

		</div>
		<? } ?>

		<? if(!empty($arResult['DETAIL_TEXT'])) { ?>
		<div class="tabs_content clearfix" itemprop="description">
			<?=$arResult['DETAIL_TEXT']?>
		</div>
		<? } ?>

		<div class="tabs_content clearfix">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/delivery_info.php"), false);?>
		</div>

		<div class="tabs_content clearfix">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/pay_info.php"), false);?>
		</div>

		<div class="tabs_content clearfix">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/garant.php"), false);?>
		</div>

		<div class="tabs_content clearfix comments_tabs">
			<div class="comments">
				<a name="open_comments"></a>
				<? if(!empty($reviews->result->num_rows)) {
					while($review = $reviews->Fetch()) {
				?>
				<p>
					<? if(!empty($review['PROPERTY_NAME_VALUE'])) echo 'Имя: '.$review['PROPERTY_NAME_VALUE'].'<br>';?>
					<? if(!empty($review['PROPERTY_DIGNITY_VALUE'])) echo 'Достоинства: '.$review['PROPERTY_DIGNITY_VALUE'].'<br>';?>
					<? if(!empty($review['PROPERTY_DISAD_VALUE'])) echo 'Недостатки: '.$review['PROPERTY_DISAD_VALUE'].'<br>';?>
					<? if(!empty($review['PROPERTY_COMMENT_VALUE'])) echo 'Комментарий: '.$review['PROPERTY_COMMENT_VALUE'].'<br>';?>
				</p>
				<? } } else { ?>
					<p class="empty">Нет отзывов.</p>
				<? } ?>
				<div id="comment-form" class="popup-content">
					<div class="cmmform">
						<form action="" method="post">
							<input type="hidden" name="action" value="comment" />
							<input type="hidden" class="text" name="product" value="<?=$arResult['ID']?>" />

							<input type="text" class="text" name="name" placeholder="Ваше имя:" />

							<textarea class="text" rows="3" name="advantage" placeholder="Достоинства:"></textarea>

							<textarea class="text" rows="3" name="disadvantage" placeholder="Недостатки:"></textarea>

							<textarea class="text" rows="3" name="text" placeholder="Комментарий:"></textarea>
							<br/>
							<button class="submitz" type="submit" name="send-comment">Добавить</button>
							<div class="clear"></div>
						</form>
					</div>
				</div>
			</div>
		</div>

	</div>
	<?/*
	<!-- TAB 1 -->
	<? if(!empty($arResult['PREVIEW_TEXT'])) { ?>
	<div class="tabs_wrap clearfix" id="tabs-1">
		<div class="tabs">
			<span data-tab="tabs-1" class="active">Описание</span>
			<? if(!empty($arResult['DETAIL_TEXT'])) { ?>
				<span data-tab="tabs-2">Технические характеристики</span>
			<? } ?>
			<span data-tab="tabs-3">Доставка</span>
			<span data-tab="tabs-4">Оплата</span>
			<span data-tab="tabs-5">Гарантия и возврат</span>
			<span data-tab="tabs-6" class="com_tab">Отзывы (<?=$reviews->result->num_rows?>)</span>
		</div>
		<div class="tabs_content active clearfix" itemprop="description">
			<?=$arResult['PREVIEW_TEXT']?>
		</div>
	</div>
	<? } ?>
	<!-- /TAB 1 -->
	<!-- TAB 2 -->
	<? if(!empty($arResult['DETAIL_TEXT'])) { ?>
	<div class="tabs_wrap clearfix" id="tabs-2">
		<div class="tabs">
			<? if(!empty($arResult['PREVIEW_TEXT'])) { ?>
				<span data-tab="tabs-1">Описание</span>
			<? } ?>
			<span data-tab="tabs-2" class="active">Технические характеристики</span>
			<span data-tab="tabs-3">Доставка</span>
			<span data-tab="tabs-4">Оплата</span>
			<span data-tab="tabs-5">Гарантия и возврат</span>
			<span data-tab="tabs-6" class="com_tab">Отзывы (<?=$reviews->result->num_rows?>)</span>
		</div>

		<div class="tabs_content active clearfix" itemprop="description">
			<?=$arResult['DETAIL_TEXT']?>
		</div>
	</div>
	<? } ?>
	<!-- /TAB 2 -->
	<!-- TAB 3 -->
	<div class="tabs_wrap clearfix" id="tabs-3">
		<div class="tabs">
			<? if(!empty($arResult['PREVIEW_TEXT'])) { ?>
				<span data-tab="tabs-1">Описание</span>
			<? } ?>
			<? if(!empty($arResult['DETAIL_TEXT'])) { ?>
				<span data-tab="tabs-2">Технические характеристики</span>
			<? } ?>
			<span data-tab="tabs-3" class="active">Доставка</span>
			<span data-tab="tabs-4">Оплата</span>
			<span data-tab="tabs-5">Гарантия и возврат</span>
			<span data-tab="tabs-6" class="com_tab">Отзывы (<?=$reviews->result->num_rows?>)</span>
		</div>

		<div class="tabs_content active clearfix">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/delivery_info.php"), false);?>
		</div>
	</div>
	<!-- /TAB 3 -->
	<!-- TAB 4 -->
	<div class="tabs_wrap clearfix" id="tabs-4">
		<div class="tabs">
			<? if(!empty($arResult['PREVIEW_TEXT'])) { ?>
				<span data-tab="tabs-1">Описание</span>
			<? } ?>
			<? if(!empty($arResult['DETAIL_TEXT'])) { ?>
				<span data-tab="tabs-2">Технические характеристики</span>
			<? } ?>
			<span data-tab="tabs-3">Доставка</span>
			<span data-tab="tabs-4" class="active">Оплата</span>
			<span data-tab="tabs-5">Гарантия и возврат</span>
			<span data-tab="tabs-6" class="com_tab">Отзывы (<?=$reviews->result->num_rows?>)</span>
		</div>

		<div class="tabs_content active clearfix">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/pay_info.php"), false);?>
		</div>
	</div>
	<!-- /TAB 4 -->
	<!-- TAB 5 -->
	<div class="tabs_wrap clearfix" id="tabs-5">
		<div class="tabs">
			<? if(!empty($arResult['PREVIEW_TEXT'])) { ?>
				<span data-tab="tabs-1">Описание</span>
			<? } ?>
			<? if(!empty($arResult['DETAIL_TEXT'])) { ?>
				<span data-tab="tabs-2">Технические характеристики</span>
			<? } ?>
			<span data-tab="tabs-3">Доставка</span>
			<span data-tab="tabs-4">Оплата</span>
			<span data-tab="tabs-5" class="active">Гарантия и возврат</span>
			<span data-tab="tabs-6" class="com_tab">Отзывы (<?=$reviews->result->num_rows?>)</span>
		</div>

		<div class="tabs_content active clearfix">
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/garant.php"), false);?>
		</div>
	</div>
	<!-- /TAB 5 -->
	<!-- TAB 6 -->
	<div class="tabs_wrap clearfix" id="tabs-6">
		<div class="tabs">
			<? if(!empty($arResult['PREVIEW_TEXT'])) { ?>
				<span data-tab="tabs-1">Описание</span>
			<? } ?>
			<? if(!empty($arResult['DETAIL_TEXT'])) { ?>
				<span data-tab="tabs-2">Технические характеристики</span>
			<? } ?>
			<span data-tab="tabs-3">Доставка</span>
			<span data-tab="tabs-4">Оплата</span>
			<span data-tab="tabs-5">Гарантия и возврат</span>
			<span data-tab="tabs-6" class="active com_tab">Отзывы (<?=$reviews->result->num_rows?>)</span>
		</div>

		<div class="tabs_content clearfix active comments_tabs">
			<div class="comments">
				<a name="open_comments"></a>
				<? if(!empty($reviews->result->num_rows)) {
					while($review = $reviews->Fetch()) {
				?>
				<p>
					<? if(!empty($review['PROPERTY_NAME_VALUE'])) echo 'Имя: '.$review['PROPERTY_NAME_VALUE'].'<br>';?>
					<? if(!empty($review['PROPERTY_DIGNITY_VALUE'])) echo 'Достоинства: '.$review['PROPERTY_DIGNITY_VALUE'].'<br>';?>
					<? if(!empty($review['PROPERTY_DISAD_VALUE'])) echo 'Недостатки: '.$review['PROPERTY_DISAD_VALUE'].'<br>';?>
					<? if(!empty($review['PROPERTY_COMMENT_VALUE'])) echo 'Комментарий: '.$review['PROPERTY_COMMENT_VALUE'].'<br>';?>
				</p>
				<? } } else { ?>
					<p class="empty">Нет отзывов.</p>
				<? } ?>
				<div id="comment-form" class="popup-content">
					<div class="cmmform">
						<form action="" method="post">
							<input type="hidden" name="action" value="comment" />
							<input type="hidden" class="text" name="product" value="<?=$arResult['ID']?>" />

							<input type="text" class="text" name="name" placeholder="Ваше имя:" />

							<textarea class="text" rows="3" name="advantage" placeholder="Достоинства:"></textarea>

							<textarea class="text" rows="3" name="disadvantage" placeholder="Недостатки:"></textarea>

							<textarea class="text" rows="3" name="text" placeholder="Комментарий:"></textarea>
							<br/>
							<button class="submitz" type="submit" name="send-comment">Добавить</button>
							<div class="clear"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /TAB 6 -->
	*/?>
</div>
<div class="clear"></div>
</div>
<?$component->SetResultCacheKeys(array("DISPLAY_PROPERTIES"));?>
