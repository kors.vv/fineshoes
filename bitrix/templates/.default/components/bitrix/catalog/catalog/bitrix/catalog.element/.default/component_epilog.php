<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;

/*$APPLICATION->AddHeadString('<link href="http://'.$_SERVER['HTTP_HOST'].''.$arResult['DETAIL_PAGE_URL'].'" rel="canonical" />',true);*/


if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}

if (isset($templateData['JS_OBJ']))
{
?><script type="text/javascript">
BX.ready(BX.defer(function(){
	if (!!window.<? echo $templateData['JS_OBJ']; ?>)
	{
		window.<? echo $templateData['JS_OBJ']; ?>.allowViewedCount(true);
	}
}));
</script><?
}
/*
if(!empty($arResult['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE'])) {
	
	if(empty($arResult['SECTION']['SECTION_PAGE_URL'])) 
		$url = $arResult['LIST_PAGE_URL'] . '?arrFilter_51_' . abs(crc32($arResult['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE'])) . 'Y&set_filter=Показать' ;
	else
		$url = $arResult['SECTION']['SECTION_PAGE_URL'] . $arResult['DISPLAY_PROPERTIES']['BRAND']['VALUE'] . '/';
	
	//$APPLICATION->AddChainItem($arResult['DISPLAY_PROPERTIES']['BRAND']['DISPLAY_VALUE'], $url);
	//
	
}

//$APPLICATION->AddChainItem($arResult['NAME']);*/
?>