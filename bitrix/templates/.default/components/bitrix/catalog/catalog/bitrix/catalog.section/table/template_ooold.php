<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$sorting = $GLOBALS['sorting'];
$sortingBy = $GLOBALS['sortingBy'];

$component->SetResultCacheKeys(array('DESCRIPTION','UF_TEXT_BOTTOM'));
?>
	<div class="catalog_list w_filters">
		<div class="sorters_wrap">
			<div class="sorters">
				<span>Сортировать по:</span>
				<form action="<?=$APPLICATION->GetCurPage()?>" method="get" id="selectblock">
				<select name="sort" class="js-shange-popul">
					<option class="up" value="populatiryup"<? if($sorting == 'SORT' && $sortingBy == 'ASC') echo ' selected="selected"'?>>Популярности</option>
					<!--<option value="populatirydown"<? /*if($sorting == 'SORT' && $sortingBy == 'DESC') echo ' selected="selected"'*/?>>Популярности &nabla;</option>-->
					<option class="up" value="priceup"<? if($sorting == 'CATALOG_PRICE_1' && $sortingBy == 'ASC') echo ' selected="selected"'?>>по увеличению цены</option>
					<option value="pricedown"<? if($sorting == 'CATALOG_PRICE_1' && $sortingBy == 'DESC') echo ' selected="selected"'?>>по уменьшению цены</option>
					<option class="up" value="nameup"<? if($sorting == 'NAME' && $sortingBy == 'ASC') echo ' selected="selected"'?>>Названию (A-Z)</option>
					<option value="namedown"<? if($sorting == 'NAME' && $sortingBy == 'DESC') echo ' selected="selected"'?>>Названию (Z-A)</option>
				</select>
				</form>
			</div>

			<div class="filter_typelist">
				<span>Вид каталога:</span>
				<a class="icon i_list active" href="<?=$APPLICATION->GetCurPage()?>?typelist=table" title="Список"></a>
				<a class="icon i_table " href="<?=$APPLICATION->GetCurPage()?>?typelist=list" title="Таблица"></a>
			</div>
		</div>


<?
if (!empty($arResult['ITEMS']))
{
	$templateLibrary = array('popup');
	$currencyList = '';
	if (!empty($arResult['CURRENCIES']))
	{
		$templateLibrary[] = 'currency';
		$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
	}
	$templateData = array(
		'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css',
		'TEMPLATE_CLASS' => 'bx_'.$arParams['TEMPLATE_THEME'],
		'TEMPLATE_LIBRARY' => $templateLibrary,
		'CURRENCIES' => $currencyList
	);
	unset($currencyList, $templateLibrary);

	$skuTemplate = array();
	if (!empty($arResult['SKU_PROPS']))
	{
		foreach ($arResult['SKU_PROPS'] as $arProp)
		{
			$propId = $arProp['ID'];
			$skuTemplate[$propId] = array(
				'SCROLL' => array(
					'START' => '',
					'FINISH' => '',
				),
				'FULL' => array(
					'START' => '',
					'FINISH' => '',
				),
				'ITEMS' => array()
			);
			$templateRow = '';
			if ('TEXT' == $arProp['SHOW_MODE'])
			{
				$skuTemplate[$propId]['SCROLL']['START'] = '<div class="bx_item_detail_size full" id="#ITEM#_prop_'.$propId.'_cont">'.
					'<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
					'<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';;
				$skuTemplate[$propId]['SCROLL']['FINISH'] = '</ul></div>'.
					'<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style=""></div>'.
					'<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style=""></div>'.
					'</div></div>';

				$skuTemplate[$propId]['FULL']['START'] = '<div class="bx_item_detail_size" id="#ITEM#_prop_'.$propId.'_cont">'.
					'<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
					'<div class="bx_size_scroller_container"><div class="bx_size"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';;
				$skuTemplate[$propId]['FULL']['FINISH'] = '</ul></div>'.
					'<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style="display: none;"></div>'.
					'<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style="display: none;"></div>'.
					'</div></div>';
				foreach ($arProp['VALUES'] as $value)
				{
					$value['NAME'] = htmlspecialcharsbx($value['NAME']);
					$skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="'.$propId.'_'.$value['ID'].
						'" data-onevalue="'.$value['ID'].'" style="width: #WIDTH#;" title="'.$value['NAME'].'"><i></i><span class="cnt">'.$value['NAME'].'</span></li>';
				}
				unset($value);
			}
			elseif ('PICT' == $arProp['SHOW_MODE'])
			{
				$skuTemplate[$propId]['SCROLL']['START'] = '<div class="bx_item_detail_scu full" id="#ITEM#_prop_'.$propId.'_cont">'.
					'<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
					'<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';
				$skuTemplate[$propId]['SCROLL']['FINISH'] = '</ul></div>'.
					'<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style=""></div>'.
					'<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style=""></div>'.
					'</div></div>';

				$skuTemplate[$propId]['FULL']['START'] = '<div class="bx_item_detail_scu" id="#ITEM#_prop_'.$propId.'_cont">'.
					'<span class="bx_item_section_name_gray">'.htmlspecialcharsbx($arProp['NAME']).'</span>'.
					'<div class="bx_scu_scroller_container"><div class="bx_scu"><ul id="#ITEM#_prop_'.$propId.'_list" style="width: #WIDTH#;">';
				$skuTemplate[$propId]['FULL']['FINISH'] = '</ul></div>'.
					'<div class="bx_slide_left" id="#ITEM#_prop_'.$propId.'_left" data-treevalue="'.$propId.'" style="display: none;"></div>'.
					'<div class="bx_slide_right" id="#ITEM#_prop_'.$propId.'_right" data-treevalue="'.$propId.'" style="display: none;"></div>'.
					'</div></div>';
				foreach ($arProp['VALUES'] as $value)
				{
					$value['NAME'] = htmlspecialcharsbx($value['NAME']);
					$skuTemplate[$propId]['ITEMS'][$value['ID']] = '<li data-treevalue="'.$propId.'_'.$value['ID'].
						'" data-onevalue="'.$value['ID'].'" style="width: #WIDTH#; padding-top: #WIDTH#;"><i title="'.$value['NAME'].'"></i>'.
						'<span class="cnt"><span class="cnt_item" style="background-image:url(\''.$value['PICT']['SRC'].'\');" title="'.$value['NAME'].'"></span></span></li>';
				}
				unset($value);
			}
		}
		unset($templateRow, $arProp);
	}

	if ($arParams["DISPLAY_TOP_PAGER"])
	{
		?><? echo $arResult["NAV_STRING"]; ?><?
	}

	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));

	if($arParams['HIDE_SECTION_DESCRIPTION'] !== 'Y')
	{ ?>

<? } ?>
	<?
foreach ($arResult['ITEMS'] as $key => $arItem)
{
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
	$strMainID = $this->GetEditAreaId($arItem['ID']);

	$arItemIDs = array(
		'ID' => $strMainID,
		'PICT' => $strMainID.'_pict',
		'SECOND_PICT' => $strMainID.'_secondpict',
		'STICKER_ID' => $strMainID.'_sticker',
		'SECOND_STICKER_ID' => $strMainID.'_secondsticker',
		'QUANTITY' => $strMainID.'_quantity',
		'QUANTITY_DOWN' => $strMainID.'_quant_down',
		'QUANTITY_UP' => $strMainID.'_quant_up',
		'QUANTITY_MEASURE' => $strMainID.'_quant_measure',
		'BUY_LINK' => $strMainID.'_buy_link',
		'BASKET_ACTIONS' => $strMainID.'_basket_actions',
		'NOT_AVAILABLE_MESS' => $strMainID.'_not_avail',
		'SUBSCRIBE_LINK' => $strMainID.'_subscribe',
		'COMPARE_LINK' => $strMainID.'_compare_link',

		'PRICE' => $strMainID.'_price',
		'DSC_PERC' => $strMainID.'_dsc_perc',
		'SECOND_DSC_PERC' => $strMainID.'_second_dsc_perc',
		'PROP_DIV' => $strMainID.'_sku_tree',
		'PROP' => $strMainID.'_prop_',
		'DISPLAY_PROP_DIV' => $strMainID.'_sku_prop',
		'BASKET_PROP_DIV' => $strMainID.'_basket_prop',
	);

	$strObName = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainID);

	$productTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])&& $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
		: $arItem['NAME']
	);
	$imgTitle = (
		isset($arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']) && $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE'] != ''
		? $arItem['IPROPERTY_VALUES']['ELEMENT_PREVIEW_PICTURE_FILE_TITLE']
		: $arItem['NAME']
	);

	$minPrice = false;
	if (isset($arItem['MIN_PRICE']) || isset($arItem['RATIO_PRICE']))
		$minPrice = (isset($arItem['RATIO_PRICE']) ? $arItem['RATIO_PRICE'] : $arItem['MIN_PRICE']);

	?><div class="catalog_item_typelist">
	<div class="typelist_img">
	<? if(!empty($arItem['PREVIEW_PICTURE']['SRC'])) { ?>
	<a href="<?=$arItem['DETAIL_PAGE_URL']?>">
		<div class="img" style="background-image:url('<?=$arItem['PREVIEW_PICTURE']['SRC']?>')">
			<? if(!empty($arItem['DISPLAY_PROPERTIES']['HIT']['VALUE'])) { ?> <div class="label hit"></div><? } ?>
			<? if(!empty($arItem['DISPLAY_PROPERTIES']['NEW']['VALUE'])) { ?> <div class="label new"></div><? } ?>
		</div>
	</a>
	<? } ?>
</div>
<div class="typelist_info">
	<div class="ttl"><a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a></div><?

$reviews = CIBlockElement::GetList(
	Array("ID"=>"DESC"),
	Array('ACTIVE' => 'Y', 'IBLOCK_ID' => 7, 'PROPERTY_PRODUCT' => $arItem['ID']),
	false,
	false,
	array('NAME')
);

?>
	<p class="gray"><a href="<?=$arItem['DETAIL_PAGE_URL']?>#open_comments">Отзывы</a>: (<?=$reviews->result->num_rows?>)</p>
	<? if(!empty($arItem['PROPERTIES']['RATING']['VALUE'])) { ?><p class="security sec_<?=$arItem['PROPERTIES']['RATING']['VALUE']?>">Надежность</p><? } ?>
	<? if(!empty($arItem['PROPERTIES']['WEIGHT']['VALUE'])) { ?><p class="gray">Вес: <?=$arItem['PROPERTIES']['WEIGHT']['VALUE']?> г</p><? } ?>
</div>
<div class="typelist_btns">


				<? if(!empty($arItem['DISPLAY_PROPERTIES']['PRICE_BEFORE']['VALUE'])) { ?>
			<div class="price w_old">
				<? echo $minPrice['VALUE']; ?><span class="rur">i</span>
				<br/>
				<span class="oldprice"><?=$arItem['DISPLAY_PROPERTIES']['PRICE_BEFORE']['VALUE']?><span class="rur">i</span></span>
			</div>
			<? } else { ?>
			<div class="price">
				<? echo $minPrice['VALUE']; ?><span class="rur">i</span>
			</div>
			<? } ?>
	<a class="click1click in_list_js icon_click fancyoneclick" data-id="<?=$arItem['ID']?>" href="#oneclickformID" title="Купить в 1 клик"></a>
	<a class="basket icon_basket basketajax" href="" data-id="<?=$arItem['ID']?>" title="Добавить в корзину"></a>
	<div class="clear"></div>
	<p><a class="compare_a compareajax js-add-favorite" href="" data-id="<?=$arItem['ID']?>" title="Сравнить">Сравнить</a></p>
</div>
		</div><?
}
?>
<script type="text/javascript">
BX.message({
	BTN_MESSAGE_BASKET_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_BASKET_REDIRECT'); ?>',
	BASKET_URL: '<? echo $arParams["BASKET_URL"]; ?>',
	ADD_TO_BASKET_OK: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	TITLE_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_ERROR') ?>',
	TITLE_BASKET_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_TITLE_BASKET_PROPS') ?>',
	TITLE_SUCCESSFUL: '<? echo GetMessageJS('ADD_TO_BASKET_OK'); ?>',
	BASKET_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_BASKET_UNKNOWN_ERROR') ?>',
	BTN_MESSAGE_SEND_PROPS: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_SEND_PROPS'); ?>',
	BTN_MESSAGE_CLOSE: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE') ?>',
	BTN_MESSAGE_CLOSE_POPUP: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_CLOSE_POPUP'); ?>',
	COMPARE_MESSAGE_OK: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_OK') ?>',
	COMPARE_UNKNOWN_ERROR: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_UNKNOWN_ERROR') ?>',
	COMPARE_TITLE: '<? echo GetMessageJS('CT_BCS_CATALOG_MESS_COMPARE_TITLE') ?>',
	BTN_MESSAGE_COMPARE_REDIRECT: '<? echo GetMessageJS('CT_BCS_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT') ?>',
	SITE_ID: '<? echo SITE_ID; ?>'
});
</script>
<?

if(!empty($arResult['NAV_RESULT'])) { ?>
<div class="pager_wrap">
		<?if(($arResult['NAV_RESULT']->NavPageNomer * $arResult['NAV_RESULT']->NavPageSize) >= $arResult['NAV_RESULT']->NavRecordCount) {?>
			показано <?=$arResult['NAV_RESULT']->NavRecordCount?> из <span class="all_in_pager_items"><?=$arResult['NAV_RESULT']->NavRecordCount?></span>
		<? } else {?>
			<a class="next<?if($arResult['NAV_RESULT']->NavPageNomer < $arResult['NAV_RESULT']->NavPageCount) echo ' ajax'?>" href="#" data-page="<?=($arResult['NAV_RESULT']->NavPageNomer + 1)?>" onclick="next_ajax($(this));return false;">
				<span class="gray_button">Показать еще</span><br/>показано <?=($arResult['NAV_RESULT']->NavPageNomer * $arResult['NAV_RESULT']->NavPageSize)?> из <span class="all_in_pager_items"><?=$arResult['NAV_RESULT']->NavRecordCount?></span>
			</a>
		<? } ?>
	</div>
</div>
<? } ?>
<?

}?>


		<div class="clear"></div>
		<div class="display_none"></div>
	</div>


		<div class="oneclickwrap">
			<div id="oneclickformID" class="oneclickform">
		        <form action="" method="post">
		            <input type="hidden" name="action" value="order1click" />
		            <input type="hidden" name="item" value="" />
		            <input type="hidden" name="count" value="1" />
		            <div class="ttl_click"></div>
		            <div class="ajax_errors"></div>
		            <div class="click_input">
		            	<label>Ваше имя</label>
		            	<input type="text" name="name" value="" class="text" />
					</div>
					<div class="click_input">
		            	<label>Телефон</label>
		            	<input type="text" name="phone" value="" class="text" />
		            </div>
		            <div class="align_center">
		            	<button type="submit" class="sub_pop submitz" />Купить</button>
<p>При отправке данных, вы принимаете условия <a href="/politika-konfidencialnosti/">пользовательского соглашения</a></p>
		            </div>
		        </form>
		    </div>
		</div>
