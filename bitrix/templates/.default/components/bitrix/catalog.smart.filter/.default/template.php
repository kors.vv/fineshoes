<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$templateData = array(
	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
	'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
);
$cur = $APPLICATION->GetCurPage();
?>

<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/ext_scripts/catalog_smartfilter.js"></script>
<?/*
<script type="text/javascript">
$(document).ready(function(){
	if (!$('.filters_wrap').parent().hasClass('center')) {
		$('.filters_wrap').css({'top':'40px'});
	}
	var height_pre_text_catalog_list = parseInt($('.margin_left_links_list').outerHeight());
	var height_catalog_list = height_pre_text_catalog_list+parseInt($('.catalog_list.w_filters').outerHeight());
	if (height_catalog_list < $('.filters_wrap').outerHeight()) {
		$('.catalog_list.w_filters').css({'height':( parseInt($('.filters_wrap').outerHeight()) - height_pre_text_catalog_list + 50)+'px'});
	}
	if ($('.pager_wrap').length) {
		var count_in_page = $('.pager_wrap').find('span.all_in_pager_items').text();
		if (count_in_page >= 0) {
			$('.smartfilter').find('.fromto').append('<p class="fitrer_fromto_count_items">найдено товаров: '+count_in_page+'</p>');
		}
	}
    if ($('.filters_wrap').length){
        scroll_filters();
    }
    $(window).resize(function() {
        if ($('.filters_wrap').length){
            scroll_filters();
        }
		var height_pre_text_catalog_list = parseInt($('.margin_left_links_list').outerHeight());
		var height_catalog_list = height_pre_text_catalog_list+parseInt($('.catalog_list.w_filters').outerHeight());
		if (height_catalog_list < $('.filters_wrap').outerHeight()) {
			$('.catalog_list.w_filters').css({'height':( parseInt($('.filters_wrap').outerHeight()) - height_pre_text_catalog_list + 50)+'px'});
		}
    });
});
function scroll_filters(){
    var scroll_div = $('.filters_wrap');
    var filter_top = parseInt($('.margin_left_links_list').offset().top);
    var window_height = parseInt($(window).outerHeight());
    var tempScrollTop = 0;
    var scrollTop = 0;
    var scrollUp = 0;
    var scrollDown = 0;
    var raznica2 = 0;
    var to_bottom = 80;
    $(window).scroll(function(){
        var scrollTop = $(window).scrollTop();
		var text_top = parseInt($('.catalog_list').offset().top)+parseInt($('.catalog_list').height());
        //высоту всегда узнаем, если вдруг фильтр развернут
        var filter_height = parseInt(scroll_div.outerHeight());
        var top_absolute = text_top-filter_height;
    	var raznica = (window_height-filter_height)-40;
        //если экран НЕ мобильного
    	if ($(window).width() > 760) {
            //2 вариант - если высота фильтра больше высоты экрана
            if (filter_height > window_height) {
            	if (scrollTop > top_absolute-to_bottom) {
            		//останавливаем перед текстом после товаров
                    if (scroll_div.hasClass('fixed_filters') && !scroll_div.hasClass('absolute_bottom_filters')) {
                        scroll_div.removeClass('fixed_filters');
                        scroll_div.addClass('absolute_bottom_filters');
                        scroll_div.css({'top':(top_absolute-filter_top-101)+'px'});
                    }
                }else if (scrollTop < filter_top){
                    //возвращаем на место вверху
                    scroll_div.removeClass('fixed_filters');
                    scroll_div.removeClass('absolute_bottom_filters');
					if ($('.filters_wrap').parent().hasClass('center')) {
						scroll_div.css({'top':'5px','bottom':'auto'});
					} else {
						scroll_div.css({'top':'40px','bottom':'auto'});
					}
                } else {
                	//прокручиваем вместе с экраном
                	if (!scroll_div.hasClass('fixed_filters')) {
	                	scroll_div.addClass('fixed_filters');
	                	scroll_div.removeClass('absolute_bottom_filters');
	                }
                	if (tempScrollTop < scrollTop ){
						//прокручиваем фильтр вниз
						//console.log('прокрутка идет вниз');
						scrollDown = scrollTop;
						if (scrollUp==0) {
							scrollUp = filter_top;
						}
						raznica2 = scrollUp-scrollDown;
	                	if ((raznica-raznica2) < 0 || to_bottom<raznica) {
	                		to_bottom = raznica2;
							scroll_div.css({'top':(raznica2)+'px','bottom':'auto'});
	                	} else {
	                		scroll_div.css({'top':(raznica)+'px','bottom':'auto'});
	                	}
					} else if (tempScrollTop > scrollTop ) {
						//прокручиваем фильтр вверх
						//console.log('прокрутка идет вверх');
						scrollUp = scrollTop;
						raznica2 = scrollUp-scrollDown;
						if ((raznica-raznica2) < 0) {
	                		scroll_div.css({'top':(raznica-raznica2)+'px','bottom':'auto'});
	                	} else {
	                		scroll_div.css({'top':'10px','bottom':'auto'});
	                	}
					}
					tempScrollTop = scrollTop;
            	}
            } else {
                //1 вариант
                //крепим снизу экрана
                if (scrollTop > top_absolute-101) {
                    if (scroll_div.hasClass('fixed_filters') && !scroll_div.hasClass('absolute_bottom_filters')) {
                        scroll_div.removeClass('fixed_filters');
                        scroll_div.addClass('absolute_bottom_filters');
                        scroll_div.css({'top':(top_absolute-filter_top-101)+'px','bottom':'auto'});
                    }
                //крепим сверху экрана
                } else if (scrollTop > filter_top) {
                    if (!scroll_div.hasClass('fixed_filters')) {
                        scroll_div.addClass('fixed_filters');
                        scroll_div.removeClass('absolute_bottom_filters');
                        scroll_div.css({'top':'6px','bottom':'auto'});
                    }
                }else{
                    //возвращаем на место вверху
                    if (scroll_div.hasClass('fixed_filters')) {
                        scroll_div.removeClass('fixed_filters');
                        scroll_div.removeClass('absolute_bottom_filters');
						if ($('.filters_wrap').parent().hasClass('center')) {
							scroll_div.css({'top':'5px','bottom':'auto'});
						} else {
							scroll_div.css({'top':'40px','bottom':'auto'});
						}

                    }
                }
        	}
    	}
    });
}
</script>
*/?>
<aside class="filters_wrap">

<div class="bx-filter">
	<div class="bx-filter-section container-fluid">
        <form action="<?echo $arResult["FORM_ACTION"]?>" method="get">
			<? if((!empty($_GET['set_filter']) || strpos($APPLICATION->GetCurPage(), '/apply/')) && empty($_GET['del_filter']) && strpos($APPLICATION->GetCurPage(), '/clear/') === false) { ?>
			<input
				class="clear_filtr"
				type="submit"
				id="del_filter"
				name="del_filter"
				value="Очистить все"
			/><? } ?>
        </form>
		<form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
			<?foreach($arResult["HIDDEN"] as $arItem):?>
			<input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
			<?endforeach;?>
				<?

				$url = explode('/', $APPLICATION->GetCurPage());
				$catandbrand = '';

				$brandlist = array(
					'abus',
					'kryptonite',
					'onguard',
					'hiplok',
					'trelock',
					'xena',
					'magnum',
					'knog',
					'bbb',
					'master-lock',
					'squire'
				);

				if(!empty($url[3]))
					$catandbrand = $url[3];
					//first price filter
					foreach($arResult["ITEMS"] as $key=>$arItem) {
						if($arItem['CODE']=='BASE') {
							foreach($arResult["ITEMS"] as $kk=>$arIPrice)//prices
							{
								$kk = $arIPrice["ENCODED_ID"];
								if(isset($arIPrice["PRICE"])):
									if ($arIPrice["VALUES"]["MAX"]["VALUE"] - $arIPrice["VALUES"]["MIN"]["VALUE"] <= 0)
										continue;

								$precision = 2;
								if (Bitrix\Main\Loader::includeModule("currency"))
								{
									$res = CCurrencyLang::GetFormatDescription($arIPrice["VALUES"]["MIN"]["CURRENCY"]);
									$precision = $res['DECIMALS'];
								}
								?>
								<div class="fblock fromto bx-filter-parameters-box bx-active">
									<span class="bx-filter-container-modef"></span>
									<div class="ttl">Цена (<span class="rur">i</span>)</div>
									<?
										$valueFrom = !empty($arIPrice["VALUES"]["MIN"]["HTML_VALUE"]) ? $arIPrice["VALUES"]["MIN"]["HTML_VALUE"] : floor($arIPrice["VALUES"]["MIN"]["VALUE"]);
										$valueTo = !empty($arIPrice["VALUES"]["MAX"]["HTML_VALUE"]) ? $arIPrice["VALUES"]["MAX"]["HTML_VALUE"] : floor($arIPrice["VALUES"]["MAX"]["VALUE"]);
									?>
									<div class="field">
										<div class="from-input">
											<input type="text" class="small"
											name="<?echo $arIPrice["VALUES"]["MIN"]["CONTROL_NAME"]?>"
											id="<?echo $arIPrice["VALUES"]["MIN"]["CONTROL_ID"]?>"
											value="<?echo $valueFrom?>"
											size="5"
											onkeyup="smartFilter.keyup(this)">
										</div>
										<div class="from-to-delim">&ndash;</div>
										<div class="to-input">
											<input type="text" class="small"
											name="<?echo $arIPrice["VALUES"]["MAX"]["CONTROL_NAME"]?>"
											id="<?echo $arIPrice["VALUES"]["MAX"]["CONTROL_ID"]?>"
											value="<?echo $valueTo?>"
											size="5"
											onkeyup="smartFilter.keyup(this)">
										</div>
										<div class="clear"></div>
										<div id="fft_price-range-slider"></div>
										<div class="clear"></div>
									</div>
									<script type="text/javascript">
										$(document).ready(function(){
											var sl_name = 'fft_price';
											$("#"+sl_name+"-range-slider").slider({
												range: true,
												min: <?echo floor($arIPrice["VALUES"]["MIN"]["VALUE"])?>,
												max: <?echo ceil($arIPrice["VALUES"]["MAX"]["VALUE"])?>,
												step: (<?echo ceil($arIPrice["VALUES"]["MAX"]["VALUE"])?>-<?echo floor($arIPrice["VALUES"]["MIN"]["VALUE"])?>)/10,
												values: [ <?echo $valueFrom?>, <?echo $valueTo?> ],
												slide: function( event, ui ){
													$("input[name=<?echo $arIPrice["VALUES"]["MIN"]["CONTROL_NAME"]?>]").val(ui.values[0]);
													$("input[name=<?echo $arIPrice["VALUES"]["MAX"]["CONTROL_NAME"]?>]").val(ui.values[1]);
												},
												change: function( event, ui ){

													$("#<?echo $arIPrice["VALUES"]["MIN"]["CONTROL_ID"]?>").trigger('keyup');
													$('.smartfilter').submit();

												}
											});
										});
									</script>
								</div>
								<?endif;
							}
						}
					}

				//not prices
				foreach($arResult["ITEMS"] as $key=>$arItem)
				{
					//$arResult["ITEMS"][51]["CODE"] - BRAND
					//$arResult["ITEMS"][50]["CODE"] - CATEGORY
					if(!empty($catandbrand)) {
						if(in_array($catandbrand, $brandlist) !== false) {
							if($key == 51)
								continue;
						}
					} else if(!empty($url[2])) {
							//if ($catandbrand != 'brands') {
								if($key == 50)
									continue;
							//}
						}

					if(empty($arItem["VALUES"]) || isset($arItem["PRICE"]))
						continue;

					if ($arItem["DISPLAY_TYPE"] == "A" && ($arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0))
						continue;


					?>
					<?php if (true && $arItem["NAME"]!='Розничная цена') { ?>
					<div class="fblock <? if($arItem["DISPLAY_TYPE"] == 'F' || $arItem["DISPLAY_TYPE"] == 'G') echo 'multicheckbox'; else if($arItem["DISPLAY_TYPE"] == 'A') echo 'fromto';?> bx-filter-parameters-box bx-active<? if ($arItem["CODE"]=='MATERIAL') echo ' last'; ?>">
						<span class="bx-filter-container-modef"></span>
						<? if($arItem["CODE"]!="product_stock") {?> <div class="ttl"><?=$arItem["NAME"]?></div> <? } ?>


						<div class="bx-filter-block" data-role="bx_filter_block">
							<div>
							<?
							$arCur = current($arItem["VALUES"]);
							switch ($arItem["DISPLAY_TYPE"])
							{
								case "A"://NUMBERS_WITH_SLIDER
									?>

										<?
		$lower_code = strtolower($arItem['CODE']);
		$valueFrom = !empty($arItem["VALUES"]["MIN"]["HTML_VALUE"]) ? $arItem["VALUES"]["MIN"]["HTML_VALUE"] : floor($arItem["VALUES"]["MIN"]["VALUE"]);
		$valueTo = !empty($arItem["VALUES"]["MAX"]["HTML_VALUE"]) ? $arItem["VALUES"]["MAX"]["HTML_VALUE"] : floor($arItem["VALUES"]["MAX"]["VALUE"]);


	?>
	<div class="field">
		<div class="from-input">
			<input type="text" class="small"
												name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
												value="<?echo $valueFrom?>"
												size="5"
												onkeyup="smartFilter.keyup(this)">
		</div>
		<div class="from-to-delim">&ndash;</div>
		<div class="to-input">
			<input type="text" class="small"
												name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
												value="<?echo $valueTo?>"
												size="5"
												onkeyup="smartFilter.keyup(this)">
		</div>
		<div class="clear"></div>
		<div id="fft_<?=$lower_code?>-range-slider"></div>
		<div class="clear"></div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			var sl_name = 'fft_<?=$lower_code?>';
			$("#"+sl_name+"-range-slider").slider({
				range: true,
				min: <?echo floor($arItem["VALUES"]["MIN"]["VALUE"])?>,
				max: <?echo ceil($arItem["VALUES"]["MAX"]["VALUE"])?>,
				step: (<?echo ceil($arItem["VALUES"]["MAX"]["VALUE"])?>-<?echo floor($arItem["VALUES"]["MIN"]["VALUE"])?>)/10,
				values: [ <?echo $valueFrom?>, <?echo $valueTo?> ],
				slide: function( event, ui ){
					$("input[name=<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>]").val(ui.values[0]);
					$("input[name=<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>]").val(ui.values[1]);
				},
				change: function( event, ui ){
					$("#<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>").trigger('keyup');
					$('.smartfilter').submit();
				}
			});
		});
	</script>
									<?
									break;
								case "B"://NUMBERS
									?>
									<div class="col-xs-6 bx-filter-parameters-box-container-block bx-left">
										<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>
										<div class="bx-filter-input-container">
											<input
												class="min-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MIN"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MIN"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MIN"]["HTML_VALUE"]?>"
												size="5"
												onkeyup="smartFilter.keyup(this)"
												/>
										</div>
									</div>
									<div class="col-xs-6 bx-filter-parameters-box-container-block bx-right">
										<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>
										<div class="bx-filter-input-container">
											<input
												class="max-price"
												type="text"
												name="<?echo $arItem["VALUES"]["MAX"]["CONTROL_NAME"]?>"
												id="<?echo $arItem["VALUES"]["MAX"]["CONTROL_ID"]?>"
												value="<?echo $arItem["VALUES"]["MAX"]["HTML_VALUE"]?>"
												size="5"
												onkeyup="smartFilter.keyup(this)"
												/>
										</div>
									</div>
									<?
									break;
								case "H"://CHECKBOXES_WITH_PICTURES_AND_LABELS
									?>
									<div class="bx-filter-param-btn-block">
									<?foreach ($arItem["VALUES"] as $val => $ar):?>
										<input
											style="display: none"
											type="checkbox"
											name="<?=$ar["CONTROL_NAME"]?>"
											id="<?=$ar["CONTROL_ID"]?>"
											value="<?=$ar["HTML_VALUE"]?>"
											<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
										/>
										<?
										$class = "";
										if ($ar["CHECKED"])
											$class.= " bx-active";
										if ($ar["DISABLED"])
											$class.= " disabled";
										?>
										<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.keyup(BX('<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')); BX.toggleClass(this, 'bx-active');">
											<span class="bx-filter-param-btn bx-color-sl">
												<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
													<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
												<?endif?>
											</span>
											<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?>
											<? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):?>(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"];?></span>)<?endif;?>
											</span>
										</label>
									<?endforeach?>
									</div>
									<?
									break;
								case "P"://DROPDOWN
									$checkedItemExist = false;
									?>
									<div class="bx-filter-select-container">
										<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
											<div class="bx-filter-select-text" data-role="currentOption">
												<?
												foreach ($arItem["VALUES"] as $val => $ar)
												{
													if ($ar["CHECKED"])
													{
														echo $ar["VALUE"];
														$checkedItemExist = true;
													}
												}
												if (!$checkedItemExist)
												{
													echo GetMessage("CT_BCSF_FILTER_ALL");
												}
												?>
											</div>
											<div class="bx-filter-select-arrow"></div>
											<input
												style="display: none"
												type="radio"
												name="<?=$arCur["CONTROL_NAME_ALT"]?>"
												id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
												value=""
											/>
											<?foreach ($arItem["VALUES"] as $val => $ar):?>
												<input
													style="display: none"
													type="radio"
													name="<?=$ar["CONTROL_NAME_ALT"]?>"
													id="<?=$ar["CONTROL_ID"]?>"
													value="<? echo $ar["HTML_VALUE_ALT"] ?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												/>
											<?endforeach?>
											<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none;">
												<ul>
													<li>
														<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
															<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
														</label>
													</li>
												<?
												foreach ($arItem["VALUES"] as $val => $ar):
													$class = "";
													if ($ar["CHECKED"])
														$class.= " selected";
													if ($ar["DISABLED"])
														$class.= " disabled";
												?>
													<li>
														<label for="<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" data-role="label_<?=$ar["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')"><?=$ar["VALUE"]?></label>
													</li>
												<?endforeach?>
												</ul>
											</div>
										</div>
									</div>
									<?
									break;
								case "R"://DROPDOWN_WITH_PICTURES_AND_LABELS
									?>
									<div class="bx-filter-select-container">
										<div class="bx-filter-select-block" onclick="smartFilter.showDropDownPopup(this, '<?=CUtil::JSEscape($key)?>')">
											<div class="bx-filter-select-text fix" data-role="currentOption">
												<?
												$checkedItemExist = false;
												foreach ($arItem["VALUES"] as $val => $ar):
													if ($ar["CHECKED"])
													{
													?>
														<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
															<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
														<?endif?>
														<span class="bx-filter-param-text">
															<?=$ar["VALUE"]?>
														</span>
													<?
														$checkedItemExist = true;
													}
												endforeach;
												if (!$checkedItemExist)
												{
													?><span class="bx-filter-btn-color-icon all"></span> <?
													echo GetMessage("CT_BCSF_FILTER_ALL");
												}
												?>
											</div>
											<div class="bx-filter-select-arrow"></div>
											<input
												style="display: none"
												type="radio"
												name="<?=$arCur["CONTROL_NAME_ALT"]?>"
												id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
												value=""
											/>
											<?foreach ($arItem["VALUES"] as $val => $ar):?>
												<input
													style="display: none"
													type="radio"
													name="<?=$ar["CONTROL_NAME_ALT"]?>"
													id="<?=$ar["CONTROL_ID"]?>"
													value="<?=$ar["HTML_VALUE_ALT"]?>"
													<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
												/>
											<?endforeach?>
											<div class="bx-filter-select-popup" data-role="dropdownContent" style="display: none">
												<ul>
													<li style="border-bottom: 1px solid #e5e5e5;padding-bottom: 5px;margin-bottom: 5px;">
														<label for="<?="all_".$arCur["CONTROL_ID"]?>" class="bx-filter-param-label" data-role="label_<?="all_".$arCur["CONTROL_ID"]?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape("all_".$arCur["CONTROL_ID"])?>')">
															<span class="bx-filter-btn-color-icon all"></span>
															<? echo GetMessage("CT_BCSF_FILTER_ALL"); ?>
														</label>
													</li>
												<?
												foreach ($arItem["VALUES"] as $val => $ar):
													$class = "";
													if ($ar["CHECKED"])
														$class.= " selected";
													if ($ar["DISABLED"])
														$class.= " disabled";
												?>
													<li>
														<label for="<?=$ar["CONTROL_ID"]?>" data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label<?=$class?>" onclick="smartFilter.selectDropDownItem(this, '<?=CUtil::JSEscape($ar["CONTROL_ID"])?>')">
															<?if (isset($ar["FILE"]) && !empty($ar["FILE"]["SRC"])):?>
																<span class="bx-filter-btn-color-icon" style="background-image:url('<?=$ar["FILE"]["SRC"]?>');"></span>
															<?endif?>
															<span class="bx-filter-param-text">
																<?=$ar["VALUE"]?>
															</span>
														</label>
													</li>
												<?endforeach?>
												</ul>
											</div>
										</div>
									</div>
									<?
									break;
								case "K"://RADIO_BUTTONS
									?>
									<div class="radio">
										<label class="bx-filter-param-label" for="<? echo "all_".$arCur["CONTROL_ID"] ?>">
											<span class="bx-filter-input-checkbox">
												<input
													type="radio"
													value=""
													name="<? echo $arCur["CONTROL_NAME_ALT"] ?>"
													id="<? echo "all_".$arCur["CONTROL_ID"] ?>"
													onclick="smartFilter.click(this)"
												/>
												<span class="bx-filter-param-text"><? echo GetMessage("CT_BCSF_FILTER_ALL"); ?></span>
											</span>
										</label>
									</div>
									<?foreach($arItem["VALUES"] as $val => $ar):?>
										<div class="radio">
											<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label" for="<? echo $ar["CONTROL_ID"] ?>">
												<span class="bx-filter-input-checkbox <? echo $ar["DISABLED"] ? 'disabled': '' ?>">
													<input
														type="radio"
														value="<? echo $ar["HTML_VALUE_ALT"] ?>"
														name="<? echo $ar["CONTROL_NAME_ALT"] ?>"
														id="<? echo $ar["CONTROL_ID"] ?>"
														<? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
														onclick="smartFilter.click(this)"
													/>
													<span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?>
													<? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):?>(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"];?></span>)<?endif;?>
													</span>
												</span>
											</label>
										</div>
									<?endforeach;?>
									<?
									break;
								case "U"://CALENDAR
									?>
									<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
										<?$APPLICATION->IncludeComponent(
											'bitrix:main.calendar',
											'',
											array(
												'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
												'SHOW_INPUT' => 'Y',
												'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MIN"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
												'INPUT_NAME' => $arItem["VALUES"]["MIN"]["CONTROL_NAME"],
												'INPUT_VALUE' => $arItem["VALUES"]["MIN"]["HTML_VALUE"],
												'SHOW_TIME' => 'N',
												'HIDE_TIMEBAR' => 'Y',
											),
											null,
											array('HIDE_ICONS' => 'Y')
										);?>

									</div></div>
									<div class="bx-filter-parameters-box-container-block"><div class="bx-filter-input-container bx-filter-calendar-container">
										<?$APPLICATION->IncludeComponent(
											'bitrix:main.calendar',
											'',
											array(
												'FORM_NAME' => $arResult["FILTER_NAME"]."_form",
												'SHOW_INPUT' => 'Y',
												'INPUT_ADDITIONAL_ATTR' => 'class="calendar" placeholder="'.FormatDate("SHORT", $arItem["VALUES"]["MAX"]["VALUE"]).'" onkeyup="smartFilter.keyup(this)" onchange="smartFilter.keyup(this)"',
												'INPUT_NAME' => $arItem["VALUES"]["MAX"]["CONTROL_NAME"],
												'INPUT_VALUE' => $arItem["VALUES"]["MAX"]["HTML_VALUE"],
												'SHOW_TIME' => 'N',
												'HIDE_TIMEBAR' => 'Y',
											),
											null,
											array('HIDE_ICONS' => 'Y')
										);?>
									</div></div>
									<?
									break;
								default://CHECKBOXES
									?>
									<?php
										//сортировка фильтра надежность
										if($key == 52) {
											ksort($arItem["VALUES"]);
										}
									?>

									<?php
                                        $clearfilter = [];
										foreach($arItem["VALUES"] as $val => $ar) {
											if (empty($ar['VALUE'])) {
												unset($arItem["VALUES"][$val]);
											}
                                            $clearfilter[] = $ar["CONTROL_NAME"];
										}
									foreach($arItem["VALUES"] as $val => $ar) {
                                            $ischeck = $ar["CHECKED"];
                                            if($ischeck)
                                                break;
                                        }
									if($ischeck) { ?>
                                    <div><a href="?<?=DeleteParam($clearfilter)?>" class="clear_this_filter">очистить</a></div>
										<?php } $count=0; $count_items = count($arItem["VALUES"]); ?>
										<?php foreach($arItem["VALUES"] as $val => $ar):?>
											<?php $count++; ?>

											<?php if ($arItem['CODE']=='BRAND') { ?>
												<?php if ($count==4 && $count_items>3) { ?>
													<div class="more_filters_slide">
												<? } ?>
											<? } else { ?>
												<?php if ($count==6 && $count_items>5) { ?>
													<div class="more_filters_slide">
												<? } ?>
											<? } ?>

											<input
												type="checkbox"
												value="<? echo $ar["HTML_VALUE"] ?>"
												name="<? echo $ar["CONTROL_NAME"] ?>"
												id="<? echo $ar["CONTROL_ID"] ?>"
												<? echo $ar["CHECKED"]? 'checked="checked"': '' ?> class="chbox js-check"
												<? if( $cur != '/catalog/') echo 'onclick="smartFilter.click(this)"';?>
											/>
											<label data-role="label_<?=$ar["CONTROL_ID"]?>" class="chbox " for="<? echo $ar["CONTROL_ID"] ?>">
												<? if($key == 52) { ?>
													<span class="security sec_<?=$ar["VALUE"]?>">&nbsp;</span> <? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):?>(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"];?></span>)<?endif;?>
												<? } else { ?>
													<?php if (!empty($ar["VALUE"])) { ?>
													<span class="bx-filter-input-checkbox">
														<? if($arItem["CODE"]!="product_stock") { ?>
                                                            <span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?>
                                                        <? } else { ?>
                                                            <span class="bx-filter-param-text" title="<?=$arItem["NAME"];?>"><?=$arItem["NAME"];?>
                                                        <? } ?>
															<? if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):?>(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"];?></span>)<?endif;?>
														</span>
													</span>
													<? } ?>
											<? } ?>
											</label>
											<?php if ($arItem['CODE']=='BRAND'){ ?>
												<?php if ($count==$count_items && $count_items>3) { ?>
													</div>
												<? } ?>
											<? } else { ?>
												<?php if ($count==$count_items && $count_items>5) { ?>
													</div>
												<? } ?>
											<? } ?>

										<?endforeach;?>
										<?php if (count($arItem['VALUES'])>5){?>
											<a class="more_filters_slide_button" href="#">+ Развернуть</a>
										<? } ?>
									<?php } ?>
							</div>
							<div style="clear: both"></div>
						</div>
					</div>
					<?php
					//if (count($arItem["VALUES"])>1 && !empty($arItem["DISPLAY_TYPE"])
					}
					?>
				<?
				}
				 if( true)
				echo '<input type="hidden" name="set_filter" value="Показать">';
				?>
				<div class="col-xs-12 bx-filter-button-box" style="display: none">
					<div class="bx-filter-block">
						<div class="bx-filter-parameters-box-container">
							<input
								class="btn btn-themes"
								type="submit"
								id="set_filter"
								name="set_filter"
								value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
							/>
							<input
								class="btn btn-link"
								type="submit"
								id="del_filter"
								name="del_filter"
								value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
							/>

							<div class="bx-filter-popup-result right" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
								<?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
								<span class="arrow"></span>
								<br/>
								<a href="<?echo $arResult["FILTER_URL"]?>" target=""><?echo GetMessage("CT_BCSF_FILTER_SHOW")?></a>
							</div>
						</div>
					</div>
				</div>
			<div class="clb"></div>
		</form>
	</div>
</div>
</aside>
<script>
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>
<? if(true ) {// $cur == '/catalog/' ?>
<script>
$('.smartfilter').find('input[type=checkbox]').change(function(){

	$(this).closest('form').submit();

})

</script>

<? } ?>
