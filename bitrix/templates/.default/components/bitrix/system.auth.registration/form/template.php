<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>

	<h1>Регистрация</h1>

<div class="cabinet-block">
<?
ShowMessage($arParams["~AUTH_RESULT"]);
?>
<?if($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && is_array($arParams["AUTH_RESULT"]) &&  $arParams["AUTH_RESULT"]["TYPE"] === "OK"):?>
<p><?echo GetMessage("AUTH_EMAIL_SENT")?></p>
<?else:?>

<form method="post" action="<?=$arResult["AUTH_URL"]?>" name="bform">
<?
if (strlen($arResult["BACKURL"]) > 0)
{
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
}
?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="REGISTRATION" />
		<div class="cabinet-block-input">
			<label><?=GetMessage("AUTH_LOGIN_MIN")?><span class="starrequired">*</span>:</label>
			<input class="text" type="text" name="USER_LOGIN" value="<?=$arResult["USER_LOGIN"]?>"/>
		
			<label><?=GetMessage("AUTH_EMAIL")?><span class="starrequired">*</span>:</label>
			<input class="text" type="text" name="USER_EMAIL" value="<?=$arResult["USER_EMAIL"]?>"/>
<?
/* CAPTCHA */
if ($arResult["USE_CAPTCHA"] == "Y")
{
	?>
			<label><?=GetMessage("CAPTCHA_REGF_PROMT")?><span class="starrequired">*</span>:</label>
			<input class="captcha" type="text" name="captcha_word" value=""/>
			<img class="captcha" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="100" height="50" alt="CAPTCHA" />
			<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
		
	<?
}
/* !CAPTCHA */
?>
		</div>
		<div class="cabinet-block-input">
			<label><?=GetMessage("AUTH_PASSWORD_REQ")?><span class="starrequired">*</span>:</label>
			<input class="text" type="password" name="USER_PASSWORD" value="<?=$arResult['USER_PASSWORD']?>" type="password"/>
		
			<label><?=GetMessage("AUTH_CONFIRM")?>:<span class="starrequired">*</span>:</label>
			<input class="text" type="password" name="USER_CONFIRM_PASSWORD" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" type="password"/>
			
			<label>&nbsp;</label> 
			<input type="hidden" name="secret_field" id="secret_field" value="">
			<input class="submitz" type="submit" onclick="yaCounter47085678.reachGoal('registration'); return true;" name="register_submit_button" value="Продолжить" />
		</div>
			
		<div class="clear"></div>
		
		<label><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></label>
		<input type="hidden" class="antibot">
	</form><br />
<p>При отправке данных, вы принимаете условия <a href="/politika-konfidencialnosti/">пользовательского соглашения</a></p>
</div>
<?endif?>
<script>
 	$(document).ready(function(){
 		$('form[name="bform"]').submit(function(){
 			if($('.antibot').val() !== 'antibot'){
 				return false;
 			}
		});
 		$('input[name="USER_EMAIL"]').focus(function(){
 			$('.antibot').val('antibot');
 		})
 		$('.submitz').click(function(){
 			$('#secret_field').val('zuuuauausuur93203dJEIfiwefn489wieN');
 		});
 	})
</script>