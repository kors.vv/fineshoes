<?require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog.php");

function cut($text, $limit) {
    if (!$firstSpaceAfterLimit = mb_strpos($text, ' ', $limit, 'UTF-8')) {
        $firstSpaceAfterLimit = $limit;
    }
    return mb_substr($text, 0, $firstSpaceAfterLimit);
}

$SectionInfoRes = CIBlockElement::GetList(
    array(),
    array("IBLOCK_ID" => $arResult['IBLOCK_ID'], "!ID"=>$arResult['ID']),
    false,
    array('nTopCount'=>2),
    array("ID", "IBLOCK_ID",'NAME', "PREVIEW_PICTURE","DETAIL_PAGE_URL","PREVIEW_TEXT")
);
while($SectionInfo = $SectionInfoRes->GetNextElement()){
    $fields = $SectionInfo->GetFields();{
        $arTempItems[$fields['ID']]=$fields;
        $arTempItems[$fields['ID']]['PREVIEW_PICTURE']=CFile::GetFileArray($fields['PREVIEW_PICTURE']);
    }
}
$arResult['MORE_ELEMENTS']=$arTempItems;
?>
