<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$APPLICATION->SetTitle($arResult['NAME']);
?>
<h1><?=$APPLICATION->GetTitle()?></h1>
<div class="article-detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<br>
	<p><a href="<?=($arResult['LIST_PAGE_URL']);?>">Смотреть все акции</a></p>
	<?/*
    <!-- <div class="articles-readalso">
        <div class="articles-title title-1">Смотрите также</div>
        <div class="articles-list js-clearwhitespace clearwhitespace-cleared">
            <?foreach($arResult['MORE_ELEMENTS'] as $arElement):?>
                    <div class="article">
                        <div class="article-inner">
                            <a href="<?=$arElement['DETAIL_PAGE_URL']?>" class="article-image">
                                <img src="<?=$arElement['PREVIEW_PICTURE']['SRC']?>" alt="<?=$arElement['NAME']?>" data-cover-image="not-fill">
                            </a>
                            <div class="article-content">
                                <div class="article-title">
                                    <a href="<?=$arElement['DETAIL_PAGE_URL']?>"><?=$arElement['NAME']?></a>
                                </div>
                                <div class="article-text">
                                    <p><?=$arElement['PREVIEW_TEXT']?></p>
                                </div>
                            </div>
                        </div>
                    </div>
            <?endforeach;?>
        </div>
    </div> -->
    */?>
</div>