<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>

<!--<h1><?//=$arResult['NAME']?></h1>-->
<div class="articles_list articles_list_p clearfix">
<?foreach($arResult["ITEMS"] as $k => $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
		<article class="articles_item<? if($k > 0 && $k % 3 == 0) echo ' last'?> " id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<div class="art_img">
				<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
					<? if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])) {
						$renderItemImage = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width" => 385, "height" => 200), BX_RESIZE_IMAGE_EXACT);
					?>
						<img src="<?=$renderItemImage['src']?>" alt="<?=$arItem["NAME"]?>">
					<? } ?>
				</a>
			</div>
			<h3 class="title"><a href="<?=$arItem["DETAIL_PAGE_URL"]?>"><?=$arItem["NAME"]?></a></h3>
			<div class="announce"><?=strip_tags($arItem["PREVIEW_TEXT"])?></div>
		</article>
<?endforeach;?>
</div>
