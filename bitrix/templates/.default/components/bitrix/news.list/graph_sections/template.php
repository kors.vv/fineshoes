<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="center">
	<div class="main-page-links">
		<ul>
<?foreach($arResult["ITEMS"] as $k => $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<li<? if(!empty($arItem["DETAIL_PICTURE"]["SRC"])) { ?> style="background: url('<?=$arItem["DETAIL_PICTURE"]["SRC"]?>') center center no-repeat;"<? } ?><?if($k % 2 != 0) {?> class="second"<? } ?> id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		
		<? if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])) { ?>
		<a href="<?=$arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']?>">
			<span class="image"><img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arItem['NAME']?>"/></span>
		</a>
		<? } else { ?>
        <a href="<?=$arItem['DISPLAY_PROPERTIES']['LINK']['VALUE']?>" class="text-link">
			<? if(!empty($arItem['DISPLAY_PROPERTIES']['TITLE']['VALUE'])) { ?><span class="title"><?=$arItem['DISPLAY_PROPERTIES']['TITLE']['VALUE']?></span><br/><? } ?>
			<? if(!empty($arItem['DISPLAY_PROPERTIES']['TEXT']['VALUE'])) { ?><span class="announce"><?=$arItem['DISPLAY_PROPERTIES']['TEXT']['~VALUE']?></span><? } ?>
		</a>		
		<? } ?>
	</li>
<?endforeach;?>
          </ul>
	</div>
</div>