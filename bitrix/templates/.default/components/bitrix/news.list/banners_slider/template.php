<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
		<? /*<!-- </div>
	</div>
</div> -->*/ ?>
<div class="main_slider_bikelock">
	<div class="owl-carousel owl-carousel-slider owl-theme main_slider_inner">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"],"ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<? if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])) { ?>
				<div class="main_slider_bikelock__item main_slider_inner_item" style="background-image: url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>');">
					<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=strip_tags($arItem['~NAME'])?>" class="main_slider_inner_item_img" />
					<div class="main_slider_bikelock__text">
						<div class="main_slider_bikelock__title"><?=$arItem['~NAME']?></div>
						<div class="main_slider_bikelock__announce"><?=$arItem['PREVIEW_TEXT']?></div>
						<? if (!empty($arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"])) { ?>
							<a class="main_slider_bikelock__morelink" href="<?=$arItem["DISPLAY_PROPERTIES"]['LINK']['VALUE']?>" onclick="yaCounter47085678.reachGoal('smotret'); return true;">Смотреть</a>
						<? } ?>
						<? if (!empty($arItem["DISPLAY_PROPERTIES"]["LINK_STOCK"]["VALUE"])) { ?>
							<a class="main_slider_bikelock__morelink white" href="<?=$arItem["DISPLAY_PROPERTIES"]['LINK_STOCK']['VALUE']?>">Все акции</a>
						<? } ?>
					</div>
				</div>
			<? } ?>
		<?endforeach;?>
	</div>
	
	<!--
	<div id="bxSlidex_<?=$arParams["IBLOCK_ID"]?>">
	<?foreach($arResult["ITEMS"] as $arItem):?>
		<?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"],"ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
		<? if(!empty($arItem["PREVIEW_PICTURE"]["SRC"])) { ?>
			<div class="main_slider_bikelock__item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=strip_tags($arItem['~NAME'])?>" />
				<div class="main_slider_bikelock__text">
					<div class="main_slider_bikelock__title"><?=$arItem['~NAME']?></div>
					<div class="main_slider_bikelock__announce"><?=$arItem['PREVIEW_TEXT']?></div>
					<? if (!empty($arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"])) { ?>
						<a class="main_slider_bikelock__morelink" href="<?=$arItem["DISPLAY_PROPERTIES"]['LINK']['VALUE']?>">Смотреть</a>
					<? } ?>
					<? if (!empty($arItem["DISPLAY_PROPERTIES"]["LINK_STOCK"]["VALUE"])) { ?>
						<a class="main_slider_bikelock__morelink white" href="<?=$arItem["DISPLAY_PROPERTIES"]['LINK_STOCK']['VALUE']?>">Все акции</a>
					<? } ?>
				</div>
			</div>
		<? } ?>
	<?endforeach;?>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#bxSlidex_<?=$arParams["IBLOCK_ID"]?>').bxSlider({
				pager: true,
				controls: true,
				auto: true,
				pause: 7000,
				autoDelay: 2000,
				autoHover: true
			});
		});
	</script>
	-->
</div>

	<? /*<!-- <div class="center">
		<div class="wrapper">
			<div class="content"> -->*/ ?>