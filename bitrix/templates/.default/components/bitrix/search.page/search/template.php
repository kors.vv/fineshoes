<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<?if(count($arResult["SEARCH"])>0):?>
<div class="search_insite_bikelock">
	<div class="h2">Разделы сайта</div>	
	<?foreach($arResult["SEARCH"] as $arItem):?>
		<div class="result-item">
			<div class="info">
				<a href="<?echo $arItem["URL"]?>" class="title"><?echo $arItem["TITLE_FORMATED"]?></a>
				<p class="search-preview"><?echo $arItem["BODY_FORMATED"]?></p>
			</div>
		</div>
	<?endforeach;?>
</div>
<?endif;?>