<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();
?>

	<h1>.Регистрация</h1>




<?if($USER->IsAuthorized()):?>

<p><?echo GetMessage("MAIN_REGISTER_AUTH")?></p>
<p>При отправке данных, вы принимаете условия <a href="/politika-konfidencialnosti/">пользовательского соглашения</a></p>
<?else:?>

<div class="cabinet-block">

<?
if (count($arResult["ERRORS"]) > 0):
echo '<div class="messages">';
	foreach ($arResult["ERRORS"] as $key => $error) {
		
			echo '<p class="error">' . str_replace("#FIELD_NAME#", "&quot;".GetMessage("REGISTER_FIELD_".$key)."&quot;", $error) . '</p>';
			
	}

echo '</div>';

elseif($arResult["USE_EMAIL_CONFIRMATION"] === "Y" && false):
?>
<p class=""><?echo GetMessage("REGISTER_EMAIL_WILL_BE_SENT")?></p>
<?endif?>

	<form method="post" action="<?=POST_FORM_ACTION_URI?>" name="regform" enctype="multipart/form-data">
<?
if($arResult["BACKURL"] <> ''):
?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
<?
endif;
?>
		<div class="cabinet-block-input">
			<label><?=GetMessage("REGISTER_FIELD_LOGIN")?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"]['LOGIN'] == "Y"):?><span class="starrequired">*</span><?endif?>:</label>
			<input class="text" type="text" name="REGISTER[LOGIN]" value="<?=$arResult["VALUES"]['LOGIN']?>"/>
		
			<label><?=GetMessage("REGISTER_FIELD_EMAIL")?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"]['EMAIL'] == "Y"):?><span class="starrequired">*</span><?endif?>:</label>
			<input class="text" type="text" name="REGISTER[EMAIL]" value="<?=$arResult["VALUES"]['EMAIL']?>"/>
<?
/* CAPTCHA */
if ($arResult["USE_CAPTCHA"] == "Y")
{
	?>
			<label><?=GetMessage("REGISTER_CAPTCHA_PROMT")?><span class="starrequired">*</span>:</label>
			<input class="captcha" type="text" name="captcha_word" value=""/>
			<img class="captcha" src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="100" height="50" alt="CAPTCHA" />
			<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
		
	<?
}
/* !CAPTCHA */
?>
		</div>
		<div class="cabinet-block-input">
			<label><?=GetMessage("REGISTER_FIELD_PASSWORD")?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"]['PASSWORD'] == "Y"):?><span class="starrequired">*</span><?endif?>:</label>
			<input class="text" type="password" name="REGISTER[PASSWORD]" value="<?=$arResult["VALUES"]['PASSWORD']?>" type="password"/>
		
			<label><?=GetMessage("REGISTER_FIELD_CONFIRM_PASSWORD")?>:<?if ($arResult["REQUIRED_FIELDS_FLAGS"]['CONFIRM_PASSWORD'] == "Y"):?><span class="starrequired">*</span><?endif?>:</label>
			<input class="text" type="password" name="REGISTER[CONFIRM_PASSWORD]" value="<?=$arResult["VALUES"]['CONFIRM_PASSWORD']?>" type="password"/>
			
			<label>&nbsp;</label>
			<input class="submitz" type="submit" name="register_submit_button" value="Продолжить" />

		</div>
			
		<div class="clear"></div>
		<label><span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?></label>
		<p>При отправке данных, вы принимаете условия <a href="/politika-konfidencialnosti/">пользовательского соглашения</a></p>
	</form>
</div>
<?endif?>