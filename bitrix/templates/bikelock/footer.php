<?
if(strpos($_SERVER["REQUEST_URI"], 'catalog') === false && strpos($_SERVER["REQUEST_URI"], 'cart') === false) {
	$APPLICATION->IncludeComponent(
		"bitrix:news.list",
		"instagram-carousel",
		array(
			"ACTIVE_DATE_FORMAT" => "j F Y",
			"ADD_SECTIONS_CHAIN" => "Т",
			"AJAX_MODE" => "N",
			"AJAX_OPTION_ADDITIONAL" => "",
			"AJAX_OPTION_HISTORY" => "N",
			"AJAX_OPTION_JUMP" => "N",
			"AJAX_OPTION_STYLE" => "Y",
			"CACHE_FILTER" => "N",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"CHECK_DATES" => "Y",
			"DETAIL_URL" => "#SITE_DIR#/about/instastyle/#ID#/",
			"DISPLAY_BOTTOM_PAGER" => "Y",
			"DISPLAY_DATE" => "Y",
			"DISPLAY_NAME" => "Y",
			"DISPLAY_PICTURE" => "Y",
			"DISPLAY_PREVIEW_TEXT" => "Y",
			"DISPLAY_TOP_PAGER" => "N",
			"FIELD_CODE" => array(
				0 => "",
				1 => "",
			),
			"FILTER_NAME" => "",
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",
			"IBLOCK_ID" => "22",
			"IBLOCK_TYPE" => "content_blocks",
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
			"INCLUDE_SUBSECTIONS" => "Y",
			"MESSAGE_404" => "",
			"NEWS_COUNT" => "20",
			"PAGER_BASE_LINK_ENABLE" => "N",
			"PAGER_DESC_NUMBERING" => "N",
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
			"PAGER_SHOW_ALL" => "N",
			"PAGER_SHOW_ALWAYS" => "N",
			"PAGER_TEMPLATE" => ".default",
			"PAGER_TITLE" => "Новости",
			"PARENT_SECTION" => "",
			"PARENT_SECTION_CODE" => "",
			"PREVIEW_TRUNCATE_LEN" => "",
			"PROPERTY_CODE" => array(
				0 => "LIKES",
				1 => "",
			),
			"SET_BROWSER_TITLE" => "N",
			"SET_LAST_MODIFIED" => "N",
			"SET_META_DESCRIPTION" => "Y",
			"SET_META_KEYWORDS" => "Y",
			"SET_STATUS_404" => "N",
			"SET_TITLE" => "N",
			"SHOW_404" => "N",
			"SORT_BY1" => "ACTIVE_FROM",
			"SORT_BY2" => "SORT",
			"SORT_ORDER1" => "DESC",
			"SORT_ORDER2" => "ASC",
			"COMPONENT_TEMPLATE" => "instagram-list"
		),
		false
	);
}?>						</div>
				</div>
		</div>
	<?/*/div*/?>
	<footer>
		<div class="clear"></div>
		<div class="footerprotector"></div>
        <div class="center">
            <div class="wrapper">
                <div class="footer footer-desctop">
                    <div class="footer-col footer-col-1">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer_addres_msk.php"), false);?>
                    </div>
                    <div class="footer-col footer-col-2">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer_addres_spb.php"), false);?>
                    </div>
                    <?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "inc",
							"EDIT_TEMPLATE" => "",
							"PATH" => "/include/footer_menu_buyers.php"
						)
					);?>
					<?$APPLICATION->IncludeComponent(
						"bitrix:main.include",
						"",
						Array(
							"AREA_FILE_SHOW" => "file",
							"AREA_FILE_SUFFIX" => "inc",
							"EDIT_TEMPLATE" => "",
							"PATH" => "/include/footer_menu_partnership.php"
						)
					);?>
                    <div class="footer-col footer-col-5">
                        <a href="mailto:mail@fineshoes.ru" onclick="yaCounter47085678.reachGoal('email'); return true;">mail@fineshoes.ru</a>
                        <ul class="footer-soc">
                            <li><a class="inst" href="https://www.instagram.com/fineshoes.ru/" target="_blank"></a></li>
                            <li><a class="vk" href="https://vk.com/" target="_blank"></a></li>
                            <li><a class="fb" href="https://www.facebook.com/" target="_blank"></a></li>
                            <li><a class="youtube" href="https://www.youtube.com/channel/UCNABsEH6F_lJmZ1xSMSGssQ" target="_blank"></a></li>
                        </ul>
                        <div class="payment"></div>
                    </div>
                    <div class="copyright">
                        <div class="logo">
                            <img src="/images/logo.svg" alt="" width="auto" height="53">
                        </div>
                        <div class="years">
                            <span><span class="copyright_numb">© 2017–2018</span> <span class="copyright_bl">Fine shoes</span>  — интернет-магазин классической мужской обуви ручной работы известных брендов</span>
                        </div>
                    </div>
                </div>
                <div class="footer footer-mobile">
                    <div class="center">
                        <?$APPLICATION->IncludeComponent("bitrix:menu", "bottom", Array(
                            "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                            "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                            "COMPONENT_TEMPLATE" => "top",
                            "DELAY" => "N",	// Откладывать выполнение шаблона меню
                            "MAX_LEVEL" => "1",	// Уровень вложенности меню
                            "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                            "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                            "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                            "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                            "ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
                            "USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                            "MENU_THEME" => "site"
                        ),
                            false
                        );?>
                        <div class="footer_social">
                            <div class="inner">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/social_links.php"), false);?>
                            </div>
                        </div>
                        <div class="footer_email">
                            <div class="inner">
                                <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/email.php"), false);?>
                            </div>
                        </div>
                        <div class="footer_email konf">
                            <div class="inner">
                                <a href="/politika-konfidencialnosti/">Политика конфиденциальности</a>
                            </div>
                        </div>
                        <div class="footer_addr">
                            <div class="inner">
                                <div class="addr_msk" itemscope itemtype="https://schema.org/LocalBusiness">
                                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer_addres_msk.php"), false);?>
                                </div><!-- 0b2e067d322897ba -->
                                <div class="addr_spb" itemscope itemtype="https://schema.org/LocalBusiness">
                                    <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/footer_addres_spb.php"), false);?>
                                </div>
                                <div class="clear"></div><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerprotector"></div>
        <div class="center">
            <div class="wrapper">
                <div class="copyright footer-mobile">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/include/logo.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                        false
                    );?>
                    <?/*div class="footer-logo"></div*/?>
                    <div class="years">
                        <span><span class="copyright_numb">&copy; 2017–<?=date('Y')?></span> <span class="copyright_bl">Fine shoes</span>  — интернет-магазин классической кожаной мужской обуви ручной работы</span>
                        <div class="visa"></div><div class="mc"></div>
                    </div>
                    <div class="dev">Создание сайта: «TopForm.Ru»</div></div>
                <div class="clear"></div>
            </div>
        </div>

		<div class="counters">
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69191139-7', 'auto');
	  ga('send', 'pageview');

	</script>
<div style="display:none;">
<br><span data-href="https://bikelock.ru/personal/novyy-razdel2/articles/kak_bystro_prodat_lichnyy_avtomobil/">1</span>
<br><span data-href="https://bikelock.ru/personal/novyy-razdel2/articles/sinteticheskoe_motornoe_maslo_osobennosti_i_dostoinstva/">2</span>
<br><span data-href="https://bikelock.ru/personal/novyy-razdel2/articles/kak_pravilno_zakazat_vizitki/">3</span>
<br><span data-href="https://bikelock.ru/personal/novyy-razdel2/articles/kakaya_tkan_dlya_palto_luchshe/">4</span>
<br><span data-href="https://bikelock.ru/personal/novyy-razdel2/articles/kompaniya_fontini_istoriya_i_dostoinstva/">5</span>
<br><span data-href="https://bikelock.ru/personal/novyy-razdel2/articles/mobilnye_peregorodki_dlya_ofisa/">6</span>
<br><span data-href="https://bikelock.ru/personal/novyy-razdel2/articles/oborudovanie_dlya_pishchevykh_predpriyatiy_farshemeshalki_i_trimmery/">7</span>
</div>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
try {
w.yaCounter47085678 = new Ya.Metrika({
id:47085678,
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,
webvisor:true
});
} catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
s = d.createElement("script"),
f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = "https://mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/47085678" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
	<div style="display:none;">
	<!--LiveInternet counter-->
	<script type="text/javascript">
	document.write("<a href='//www.liveinternet.ru/click' "+
	"target=_blank><img src='//counter.yadro.ru/hit?t50.6;r"+
	escape(document.referrer)+((typeof(screen)=="undefined")?"":
	";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
	screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
	";h"+escape(document.title.substring(0,150))+";"+Math.random()+
	"' alt='' title='LiveInternet' "+
	"border='0' width='31' height='31'><\/a>")
	</script>
	<!--/LiveInternet-->
	</div>
	<?/*<div class="bottom-panel fixed <?if(empty($GLOBALS['countProduct']['count']) && getCountFavorite() == 0 || $APPLICATION->GetCurPage() == '/personal/cart/' || $panelview == 'hide') echo 'hide'?>">
		<div class="wrapper">
			<div class="panel-item <?if(empty($GLOBALS['countProduct']['count'])) echo 'hide'?>" id="panel-order-btn">
				<a class="red-btn" href="/personal/cart/"><span>Оформить заказ</span></a>
			</div>
			<div class="panel-item" id="basket-info">
			<div class="title"><span>Корзина:</span></div>
			<div class="count"><span><?=$GLOBALS['countProduct']['count']?></span></div>
			</div>
			<a class="panel-item" id="compare-info"<?if(getCountFavorite() > 0) echo ' href="/catalog/compare/"';?>>
				<div class="title"><span>Сравнение:</span></div>
				<div class="count"><span><?=getCountFavorite()?></span></div>
			</a>
		</div>
		<div class="bottom-panel-close"></div>
	</div>*/?>

	<div class="bx_catalog_compare_wrap<?if(getCountFavorite() == 0 || $APPLICATION->GetCurPage() == '/catalog/compare/' || $panelview == 'hide') echo ' hide'?>">
		<div class="bx_catalog_compare_count"><a href="javascript:void(0);" onclick="show_compare_panel(this);"><span><?=getCountFavorite()?></span></a></div>
		<?php $compare_items = getItemsFavoriteInfo(); ?>
		<div class="bx_catalog_compare_form_panel">
			<div class="compare-items">
			<?php foreach ($compare_items as $comp_item) { ?>
				<div class="compare-item" rel="row_compare_<?=$comp_item['ID']?>">
					<div class="compare-item-col"><a href="<?=$comp_item['DETAIL_PAGE_URL']?>"><?=$comp_item['NAME']?></a></div>
					<div class="compare-item-col"><noindex><a class="remove_comp" href="javascript:void(0);" data-id="<?=$comp_item['ID']?>" rel="nofollow">Убрать</a></noindex>
					</div>
				</div>
			<?php } ?>
		</div>
			<div class="align_center">
				<a href="/catalog/compare/" class="btn">Перейти в сравнение</a>
			</div>
		</div>
	</div></div>
	</footer>
<div class="bottom-panel-small" <?if(getCountFavorite() != 0 && $APPLICATION->GetCurPage() != '/catalog/compare/' && $panelview == 'hide') echo ' style="display: block;"'?>>&uarr;</div>
	<div class="show_maxcount_compare">В сравнение добавлено максимальное кол-во товаров</div>

	<div class="filter_mobile_panel">
		<div class="filter_mobile_panel_close"></div>
		<div class="filter_mobile_panel_title">
			Фильтр
		</div>
		<div id="filter_mobile_panel_body"></div>
	</div>
	<div class="filter_typelist_bg"></div>
<script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/ext_scripts/owl_carousel_slider_footer.js"></script>
<?/*
<script>
$(window).load(function(){
    $('.owl-carousel-slider').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        items:1

    });
});
</script>
*/?>
</main>
<a href="#" class="button-up">Наверх</a>
</body>
</html>
