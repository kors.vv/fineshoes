$(document).ready(function() {

    $('.search-link').on('click', function(e) {
        e.preventDefault();
        $(this).next().fadeToggle();
    });
    
    
        $('.header_menu.main_menu_ .main_mnu_ ul li[data-number=0]').hover(function(){
            $('.header_menu.main_menu_ .submenu_new').css('display', 'block');
        }, function(){
            $('.header_menu.main_menu_ .submenu_new').css('display', 'none');
        })
        
        $('.header_menu.main_menu_ .main_mnu_ ul li[data-number=1]').hover(function(){
            $('.header_menu.main_menu_ .submenu_new2').css('display', 'block');
        }, function(){
            $('.header_menu.main_menu_ .submenu_new2').css('display', 'none');
        })
        
        $('.header_menu.main_menu_ .submenu_new').hover(
        function(){
            $(this).css('display', 'block');

        }, function(){
            $(this).css('display', 'none');
        })
        
        $('.header_menu.main_menu_ .submenu_new2').hover(
        function(){
            $(this).css('display', 'block');

        }, function(){
            $(this).css('display', 'none');
        })
        
        
    $(document).on('scroll', function () {
        var offset = $('.header_menu').offset().top;
        if (offset > 145) {
            $('.sticky-header').show();
        } else {
            $('.sticky-header').hide();
        }
    });
    
    if ($('#top_header_info').length){
        $.get('/top_header_info_status',{},function(data) {
            if ($('#top_header_info').length && data.res == 'NO') {
                $('#top_header_info').slideDown(500);
            };
        }, "json");
        $('#top_header_info .top_header_close').on('click',function(){
            $('#top_header_info').slideUp(500);
            $.get('/top_header_info_hide',{},function(data) {});
        });
    }
        
    $(".sticky-header .main_mnu_ ul li[data-number=0]").hover(function() {
            $('.sticky-header .submenu_new').css('display', 'block');
            $('.sticky-header').css('height', '600px');
    },function(){
            $('.sticky-header .submenu_new').css('display', 'none');
        });
        
    /* $(".sticky-header .main_mnu_ ul li[data-number=1]").hover(function() {
            $('.sticky-header .submenu_new2').css('display', 'block');
            $('.sticky-header').css('height', '600px');
    },function(){
            $('.sticky-header .submenu_new2').css('display', 'none');
        }); */
        
        
        $('.sticky-header').mousemove(function() {
            if( $(".sticky-header .submenu_new").css('display') == 'none'){
                $(".sticky-header").height(53);
            }
        })
        
        /* $('.sticky-header').mousemove(function() {
            if( $(".sticky-header .submenu_new2").css('display') == 'none'){
                $(".sticky-header").height(53);
            }
        }) */
        
        $(".sticky-header .submenu_new").hover(
            function(){
                $(this).css('display', 'block');
                
            }, function(){
                $(this).css('display', 'none');
                $(".sticky-header").height(53);
            })
            
        /* $(".sticky-header .submenu_new2").hover(
            function(){
                $(this).css('display', 'block');
                
            }, function(){
                $(this).css('display', 'none');
                $(".sticky-header").height(53);
            }) */
            
        $(".sticky-header li").each(function(i, el){
            if( i != 0 ){
                $(el).hover(function() {
                    $(".sticky-header").height(53);
                })
            }
        });
    $(".header_mobile_nav_item.search").on("click", function(e) {
        e.preventDefault();
        $(".header_mobile_search").slideToggle();
    });

    $('.main_mnu_ .main_mnu_ul').after("<div id='mobile-menu'>").clone().appendTo("#mobile-menu");
    $("#mobile-menu").find("*").attr("style", "");
    $("#mobile-menu").children("ul").parent().mmenu({
        extensions        : [ 'widescreen', 'theme-white', 'effect-menu-slide', 'pagedim-black' ],
        navbar: {
            title: "Меню",
        }
    });
    
    

    var filters_wrap = $(".filters_wrap").clone();
    $("#filter_mobile_panel_body").append(filters_wrap);

    $(".filter_mobile").on("click", function() {
        var bodyHeight = $("main").height();
        $(".filter_mobile_panel").css('height', bodyHeight).toggleClass("open");
        $(".filter_typelist_bg").toggleClass("open");
    });

    $(".filter_mobile_panel_close, .filter_typelist_bg").on("click", function() {
        $(".filter_mobile_panel").toggleClass("open");
        $(".filter_typelist_bg").toggleClass("open");
    });

    var header_phones = $(".header .header_phones").clone();
    $("#mobile_phones").append(header_phones);

    $(".header_mobile_nav_item.call").on("click", function(e) {
        e.preventDefault();
        $("#mobile_phones").slideToggle();
    });

    var counterStep = 250;
    var counter = counterStep;
    $(".tabs_arrow_right").on("click", function() {
        var _this = $(this),
                container = _this.closest(".tabs"),
                tabs = container.find(".tabs_inner"),
                tabs_arrow_left = container.find('.tabs_arrow_left');
        tabs.animate( { scrollLeft: counter }, 1000);
        counter+= counterStep;
        tabs_arrow_left.addClass("active");
    });
    $(".tabs_arrow_left").on("click", function() {
        var _this = $(this),
                container = _this.closest(".tabs"),
                tabs = container.find(".tabs_inner");
        tabs.animate( { scrollLeft: 0 }, 1000);
        _this.removeClass("active");
        counter = counterStep;
    });

    var asideHeight = $(".filters_wrap").height();
    if ($(window).width() > 767) {
        $(".search_wrap_flex").css("min-height", asideHeight+25);
    }

    $(window).on("resize", function() {
        if ($(window).width() < 481) {

            catalogItemMobile();

        } else {

        }
    });

    if ($(window).width() < 481) {

        catalogItemMobile();

    }



    function catalogItemMobile() {
        $.each($(".catalog_item_list"), function() {
            var _this = $(this),
                    old = _this.find(".catalog_item_no_mobile"),
                    titleClone = _this.find(".ttl a").clone(),
                    titleMobile = _this.find(".catalog_item_mobile_title"),
                    info = _this.find(".catalog_item_mobile_info"),
                    dop_abs = _this.find(".dop_abs").clone(),
                    other_color = _this.find(".block_other_color a").clone(),
                    price = _this.find(".price").clone(),
                    lenghtP = _this.find(".catalog_item_length").clone(),
                    weight = _this.find(".catalog_item_weight").clone(),
                    reviews = _this.find(".review_item_").clone(),
                    compare = _this.find(".compare_item_").clone(),
                    basketBtn = _this.find(".basket").clone(),
                    catalog_item_mobile_btn = _this.find(".catalog_item_mobile_btn"),
                    block_mobile_other_color = _this.find(".block_mobile_other_color"),
                    loader = _this.find(".loader");

            if ( old.length ) {
                titleMobile.empty().append(titleClone);
                info.empty().append('<br />').append(titleMobile).append(price).append(dop_abs);
                catalog_item_mobile_btn.empty().append('<div class="catalog_item_mobile_info_btn"></div>').find(".catalog_item_mobile_info_btn").append(basketBtn);
                                block_mobile_other_color.empty().append(other_color);
                var datachOld = old.detach();
            }

            if ( loader.length ) {
                loader.removeClass("loader_active");
            }

        });
    }

});
$(window).load(function() 
{
    $('.b24-widget-button-inner-container').attr('onclick','yaCounter47085678.reachGoal("online-consultant"); return true;');
}); 

(function($) { /* replace [data-href] -> a[href]*/
  $(function() {
    $('[data-href!=""][data-href]').each(function() {
      var
        attributes = $(this).prop("attributes"),
        inner      = ($(this).html().length > 0) ? $(this).html() : '',
        $link      = $("<a>", {
          href : $(this).data('href'),
          html : inner
        });
      $.each(attributes, function() {
        if(this.name !== 'data-href')
        {
          $link.attr(this.name, this.value);
        }
      });
      $(this).after($link).remove();
    });
    $(document).triggerHandler("px:linksReady");
  });
})(jQuery);
