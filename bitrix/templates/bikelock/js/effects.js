//ajax pager
//var cPage,cPage2;
function next_ajax(button) {
	$(button).on('click',function(){
	   $(button).css("pointer-events", "none");
		var obj=$(button);
        //console.log(cPage);
        /*if(cPage==$(button).attr('data-page')){
            return false;
        }*/
		var nextPage = $(button).attr('data-page');
		$.get(location.pathname,{'PAGEN_1': nextPage, 'clear' : 'Y'},function(data) {
			var elems = $(data).find('.catalog_item_list,.catalog_item_typelist');
			$('.pager_wrap').before(elems);
			if ($(data).find('.pager_wrap').find('a.next').hasClass('ajax')){
				$('.pager_wrap').html($(data).find('.pager_wrap').html());
			} else {
				$('.pager_wrap').html($(data).find('.pager_wrap').find('a.next').html());
			}
            $(button).css("pointer-events", "all");
			$(window).resize();
		});
		return false;
	});
}

$(document).on('click', '.next.ajax', function(){
    $(this).css("pointer-events", "none");
		var obj=$(this);
        /*if(cPage2==$(this).attr('data-page')){
            return false;
        }*/
		var nextPage = $(this).attr('data-page');
		$.get(location.pathname,{'PAGEN_1': nextPage, 'clear' : 'Y'},function(data) {
			var elems = $(data).find('.catalog_item_list,.catalog_item_typelist');
			$('.pager_wrap').before(elems);

			if ($(window).width() < 481) {

				catalogItemMobile();

			}

			if ($(data).find('.pager_wrap').find('a.next').hasClass('ajax')){
			 //console.log('true');
				$('.pager_wrap').html($(data).find('.pager_wrap').html());
			} else {
			 //console.log(data);
				$('.pager_wrap').html($(data).find('.pager_wrap').html());
			}
            $(this).css("pointer-events", "all");
		});
		//catalogItemMobile();
		return false;
	});

$(function(){

	$('input[name="phone"]').mask("+7(999)999-99-99");
	$('.order_wrap input.input_order_phone').mask("+7(999)999-99-99");

	//scroll to top
	$('.button-up').click(function(){
		$('html, body').animate({ scrollTop: 0 }, 500);
		return false;
	});

	//show up-button
	$(document).scroll(function(){
		var y = $(this).scrollTop();
		if (y > 500) {
			$('.button-up').fadeIn();
		} else {
			$('.button-up').fadeOut();
		}
	});

	$('#ORDER_FORM').submit(function(){

		var errors = [];

		var name = $('[name=ORDER_PROP_1]');
		var phone = $('[name=ORDER_PROP_3]');
		var email = $('[name=ORDER_PROP_2]');
		var city = $('[name=ORDER_PROP_5]');
		var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;


		if(!name.val())
			errors.push('Необходимо заполнить поле «Имя»');

		if(!phone.val())
			errors.push('Необходимо заполнить поле «Телефон»');

		if(!email.val())
			errors.push('Необходимо заполнить поле «Email»');
		else if(!pattern.test(email.val()))
			errors.push('Неверный формат адреса электронной почты. Пример: v.pupkin@gmail.com.');


		if($('[name=DELIVERY_ID]:checked').val() === undefined)
			errors.push('Необходимо заполнить поле «Способ доставки»');

		if($('[name=address]:checked').val() === undefined && city.val() == '')
			errors.push('Необходимо заполнить поле «Город»»');

		$('#order_message').remove();

		if(errors.length) {

			$('.order_wrap').prepend('<div id="order_message"></div>');

			for(var i = 0; i < errors.length ; i++) {

				$('#order_message').append('<p class="error">' + errors[i] + '</p>');

			}

			return false;

		}

		if($('[name=DELIVERY_ID]:checked').val() == 2) {

			if(city.val() && city.val() != 'Москва' && city.val() != 'Петербург') {

				$('[name=DELIVERY_ID]').prop('checked', false);
				$('#currach').prop('checked', true);


			}


		}


		if(city.val() == '')  {

			var cc = '';

			if($('[name=address]:checked').val() == 'Москва')
				cc = 'Москва';
			else if($('[name=address]:checked').val() == 'Петербург')
				cc = 'Петербург';

			city.val(cc);

		}


		var data = $(this).serialize();
		/*Неверный «Проверочный код» */

		$.post('/personal/cart/', data, function(res){

			location.assign(res.redirect);


		}, 'json')

		return false;

	})

	if(baskedAdded.length) {

		var c = baskedAdded.length;

		for(var i = 0 ; i < c ; i++) {

			var element = $('.js-add-basket[data-id=' + baskedAdded[i].ID + ']');
                        select = $('select.choose_size option:selected').val();
                        if ( select == baskedAdded[i].SIZE ){
                            if (element.hasClass('in_item')) {
                                    element.attr('href', '/personal/cart/').html('Товар в <span>корзине</span>');
                            } else {
                                    element.attr('href', '/personal/cart/');
                            }

                            element.attr('class', 'in_basket');

                            element.unbind('click');
                        }

		}

	}

	if('favoriteList' in window) {

		var cc = favoriteList.length;

		for(var i = 0 ; i < cc ; i++) {

			$('.js-add-favorite[data-id=' + favoriteList[i] + ']').attr('class', 'compare_a js-add-favorite remajax active').text('В сравнении');

		}

	}
})
function show_compare_panel(pn){
	$('.bx_catalog_compare_form_panel').fadeToggle(400);
	return false;
}
$(document).ready(function() {

	$('select[name=sort]').fancySelect({toSubmit: true});

	//сворачивание фильтров по иконке (-)
	$(document).on('click', '.filters_wrap .fblock .ttl', function(){
		$(this).toggleClass("slide");
		$(this).parents('.fblock').toggleClass("slide");
		$(this).next('div').slideToggle(400);
		return false;
	});
	//сворачивание элементов фильтра по ссылке "+ Развернуть"
	$(document).on('click', '.fblock .more_filters_slide_button', function(){
		if (!$(this).hasClass('open')) {
			$(this).text("- Свернуть");
			$(this).addClass('open');
		} else {
			$(this).text("+ Развернуть");
			$(this).removeClass('open');
		}
		$(this).prev('div.more_filters_slide').slideToggle(400);
		return false;
	});

	$('.js-shange-popul').change(function(){
		$(this).closest('form').submit();
	})
	//cabinet-login
	$(document).on('click', '.login_block_wrap .icon_login', function(){
		$(this).next('.cabinet-block').fadeToggle(400);
	});
	//main-slider
	var wowSlider = $('#main-slider ul').bxSlider({
		pager: false,
	    startSlide: 0,
	    minSlides: 2.5,
	    maxSlides: 2.5,
	    moveSlides: 1,
	    slideMargin: 10,
	    slideWidth: 5000,
	    onSliderLoad: function(currentIndex) {
	      $('#main-slider').addClass('loaded');
	      $('#main-slider').find('.bx-viewport ul li:not(.bx-clone)').eq(currentIndex + 1).addClass('active-slide');
	    },
				onSlideBefore: function($slideElement, oldIndex, newIndex){
	      $('#main-slider').find('.bx-viewport ul li').removeClass('active-slide');
	    },
	    onSlideAfter: function($slideElement, oldIndex, newIndex){
	      $slideElement.next().addClass('active-slide');
	    }
	});
	//search block
	// $('.search_block .icon_search').on('click',function(){
	// 	if ($(this).parent().css('z-index') == '9995') {
	// 		$(this).parent().css({'z-index': '9999'});
	// 	} else {
	// 		$(this).parent().css({'z-index': '9995'});
	// 	}
	// 	$('.login_block_wrap .cabinet-block').fadeOut(400);
	// 	$(this).next('form').fadeToggle(200);
	// 	return false;
	// });
	$('.search_block input.text').focus(function() {
		$(this).parent('form').addClass('active');
	}).blur(function(){
		$(this).parent('form').removeClass('active');
	});
	$('.search_block input.ya-site-form__input-text').focus(function() {
		$(this).parents('#ya-site-form0').addClass('active');
	}).blur(function(){
		$(this).parents('#ya-site-form0').removeClass('active');
	});
	$(document).on('click', '.search_block .select_search', function(){
		$(this).children().slideToggle(300);
	});
	$(document).on('click', '.search_block .switch_search', function(){
		if (!$(this).hasClass('show_ya')) {
			$(this).text('Поиск сайта').addClass('show_ya');
			$(this).parent().parent().find('#form_search_site').hide();
			$(this).parent().parent().find('#form_search_yandex').show();
		} else {
			$(this).text('Поиск от Яндекс').removeClass('show_ya');
			$(this).parent().parent().find('#form_search_site').show();
			$(this).parent().parent().find('#form_search_yandex').hide();
		}
		$(this).slideUp(300);
		return false;
	});

	//order basket
	$('input[name=DELIVERY_ID]').on('change', function(){
		if ($(this).attr('id') == 'kur') {
			$('.hidden_dostavka').slideDown(500);
			yaCounter47085678.reachGoal('POCHTA');
		} else {
			$('.hidden_dostavka').slideUp(500);
			yaCounter47085678.reachGoal('SAM');
		}
	});
	$('select.order_pay').on('change', function(){yaCounter47085678.reachGoal('PAY');});
	$('input[name=address]:checked').on('change', function(){
		if ($(this).attr('id') == 'spb') {
			yaCounter47085678.reachGoal('SPB');
		} else {
			yaCounter47085678.reachGoal('MSC');
		}
		if ($('input[name=DELIVERY_ID]:checked').val() == 'Курьером на адрес') {
			$('tr.item_dostavka').remove();
			$('#basket_items:last-child').append('<tr class="item_dostavka"><td>&nbsp;</td><td>Доставка по городу</td><td>400<span class="rur">i</span></td><td>1</td><td>400<span class="rur">i</span></td><td>&nbsp;</td></tr>');
			$('.paysumm .summ').text(parseInt(allsum)+400).css({'color': '#C30202'});
		}
	});
	$("textarea[name=comment]").blur(function() {
		if ($(this).val() != '') {yaCounter47085678.reachGoal('COMMENT');};
	});
	//$('#address_drugoy').hide();
	//
	//catalog item
    $(document).on('click', '.click1click.in_item_js', function(){
    	$(this).next('div').children('.oneclickform').fadeIn(400);
    	return false;
    });
    $(document).on('click', '.oneclickform .close', function(){
    	$(this).parent('.oneclickform').fadeOut(400);
    	return false;
    });
    $('.product-bxslider').bxSlider({
    	controls:true,
        infiniteLoop: false,
      	pager: false,
        onSliderLoad: function(currentIndex) {
          $('.catalog_item_left .img_item').addClass('loaded');
        },
    });
    //addproduct
    //catalog default

    $(document).on('click', ".oneclickform .sub_pop", function(){
        var form = $(this).parent().parent();
        $(form).find('input[type=submit]').attr('disabled', 'disabled');
        $(form).find('button[type=submit]').attr('disabled', 'disabled');
        $.post(location.pathname,$(form).serialize(), function(data){
        	$(form).find('input[name=phone]').attr('disabled', 'disabled');
        	$(form).find('input[name=name]').attr('disabled', 'disabled');
            if (data.txt) {
                $(form).empty().html(data.txt);
            } else {
                $(form).find('.ajax_errors').empty().html(data.err);
                $(form).find('input[name=name]').removeAttr('disabled');
                $(form).find('input[name=phone]').removeAttr('disabled');
                $(form).find('input[type=submit]').removeAttr('disabled');
                $(form).find('button[type=submit]').removeAttr('disabled');
            }
        }, "json");
        return false;
    });

	var date = new Date();

	$('form').append('<input type="hidden" name="zyear" value="' + + date.getFullYear() + '">');

	$('.cmmform form').submit(function(){

		var form = this;
		$('.js-remove-data').remove();

        $.post(location.pathname,$(form).serialize(), function(data){

			$(form).parent().before('<div class="js-remove-data">' + data.txt + '</div>');

        }, "json");


		return false;

	})



    $(document).on('click', '.click1click.in_list_js', function(){
    	$('#oneclickformID input[name=item]').val($(this).attr('rel'));
    	$('#oneclickformID .ttl_click').html($(this).attr('ttl'));
    });
    //
    $(document).on('click', '.multicheckbox label,.checkbox label', function(){

		if($(this).closest('.bx-filter'))
			return;

		var id = '#'+$(this).prop('for');
		if ($(id).prop('checked')) {
			$(id).prop('checked', false);
		} else {
			$(id).prop('checked',true);
		}
		$(this).closest('form').submit();
	});
	//
	//comments default
	$('.add-comment').click(function(){
		$('#comment-form input[name=parent]').val(parseInt($(this).attr('rel')));
		$('#comment-form').toggle();
		return false;
	});

	// compare default
	//высота левого блока с названиями хар-ик
	var compare_item_height = 0;
	$('.compare_item').each(function(){
		if ($(this).outerHeight() > compare_item_height) {
	 		compare_item_height = $(this).outerHeight();
	 	}
	});
	//$('.compare_left_spec').css({'margin-top': compare_item_height+30+'px'});

	//убираем пустые строчки хар-ик
	var empty = {};
	$('.compare_table.preEmpty tr').each(function(i,e){
		if ($(this).children('td').html() == "&nbsp;") {
			if (empty[$(this).attr('class')] != false) {
				empty[$(this).attr('class')] = true;
			}
		} else {
			empty[$(this).attr('class')] = false;
		}
	});

	$('.compare_table tr').each(function(){
		if (empty[$(this).attr('class')] == true) {
			$(this).remove();
		}
	});

	//показать\скрыть одинаковые\различные хар-ки
	findIdenticalSpec();
	$(document).on('click', '.compare-control .compare-params-btns a', function(){
		if ($(this).hasClass('active')) {
			return false;
		}
		if ($(this).hasClass('differens-params')) {
			$('.compare-control .compare-params-btns a').removeClass('active');
			$(this).addClass('active');
			$('.compare_table tr.identical').hide();
			return false;
		}
		if ($(this).hasClass('all-params')) {
			$('.compare-control .compare-params-btns a').removeClass('active');
			$(this).addClass('active');
			$('.compare_table tr.identical').show();
			return false;
		}
	});

	//установка ширины списка товаров сравнения
	width_list_compare();

	//установка стилизованного горизонтального скролла
	if ($('.scrollLeft').length) {
		var pane = $('.scrollLeft').jScrollPane();
		var api = pane.data('jsp');
	};

	//в сравнение
	$(document).on('click', '.compare_item .remove', function() {
		//удаление товара

		var obj = this;
		//удаление товара
		var id = $(this).attr('data-id');

		var  fields = {
				'delfavorite' : id
			};

		var elem = $(this).parent();
		var cls = elem.attr('class').split(' ');
		elem = $('.' + cls[1]);

		$.get(location.pathname, fields,function(data) {
			if(data.res == "OK") {
				$('h1 span.compare_count_h1').html('('+data.num+')');
				$('#compare-info .count span').html(data.num);
				elem.remove();

				var compare_item_height = 0;
				$('.compare_item a').each(function(){
					if ($(this).outerHeight() > compare_item_height) {
						compare_item_height = $(this).outerHeight();
					}
				});
				//$('.compare_left_spec').css({'margin-top': (compare_item_height+235)+'px'});
				width_list_compare();
				api.reinitialise();
				findIdenticalSpec();
				if (data.num == 1) {
					$('a.differens-params').hide();
				} else if (data.num>1) {
					$('a.differens-params').show();
				}
				if (data.num == 0) {
					window.location.href = '/catalog/';
				};
			};
		}, "json");
		return false;
	});

	//удаление товара в блоке сравнения справа
	$(document).on('click', '.bx_catalog_compare_form_panel .remove_comp', function() {
		var obj = this;
		var id = $(this).attr('data-id');
		var  fields = {
			'delfavorite' : id
		};
		var elem = $(this).parents('tr');
		$.get('/catalog/compare/', fields,function(data) {
			if(data.res == "OK") {
				$('.bx_catalog_compare_count span').html(data.num);
				elem.remove();
				$('.catalog_list').find('a.compare_a[data-id='+id+']').removeClass('remajax').removeClass('active').addClass('compareajax').text('Сравнить');
				if (data.num == 0) {
					$('.bx_catalog_compare_wrap').hide();
				};
			};
		}, "json");
		return false;
	});

	$(document).on('click', '.remove-compare', function() {
		//удаление товаров
		var ajaxurl = '/catalog/compare?action=clearcompare';
		var obj=$(this);
		//var api = pane.data('jsp');
		$.get(ajaxurl,{},function(data) {
			window.location.href = '/catalog/';
		});
		return false;
	});

	//передвигаем фиксированные изображения товаров при сдвиге влево
	$('.scrollLeft').on('scroll',function(){
		$('.fixed_top_compare').css({'left':parseInt($(this).find('.leftOffset_compare').offset().left)+'px'});
	});
	// $(window).scroll(function() {
	// 	if ($(window).scrollTop() >= 590) {
	// 		$('.compare_item_teh').css({'padding-top': parseInt($('.fixed_top_compare_in .compare_item').outerHeight())+'px'});
	// 		$('.fixed_top_compare').css({'position':'fixed','top':'0','left':parseInt($('.scrollLeft').find(".leftOffset_compare").offset().left)+'px'});
	// 	} else if ($(window).scrollTop() < 590) {
	// 		$('.compare_item_teh').css({'padding-top': '0px'});
	// 		$('.fixed_top_compare').css({'position':'relative','left':'0px'});
	// 	}
	// });
	function width_list_compare() {
		var count_items = parseInt($('.leftOffset_compare').find('.compare_item_teh').length);
		var width_items = parseInt($('.compare_item_teh').outerWidth());
		$('.leftOffset_compare,.fixed_top_compare_in').css({'width': (width_items * count_items) + 'px'});
	}

	function findIdenticalSpec() {
		var field_value = new Object();
		var field_value_re = {};
		var key = '';
		$('.compare_table.preEmpty tr td').each(function(i, obj){
			key=$(obj).parent().attr('class');
			if ($.isArray(field_value[key])) {
				if (field_value[key][field_value[key].length-1] == $(obj).html() && field_value_re[key] != false) {
					field_value_re[key] = true;
				} else {
					field_value_re[key] = false;
				}
				field_value[key].push($(obj).html());
			} else {
				field_value[key] = new Array();
				field_value[key].push($(obj).html());
			}
		});
		//console.log(field_value);
		//console.log(field_value_re);
		$('.compare_table tr').each(function(){
			if (field_value_re[$(this).attr('class')] == true) {
				$(this).addClass('identical');
			}
		});
	}
	//карты в шапке сайта
	$(document).on('click', '.ttl_city .click', function(){
		$(this).parents('table').find('.show_map').fadeOut(400);

		if ($(this).hasClass('opened')) {
			$(this).parent().find('.show_map').fadeOut(400);
			$(this).removeClass('opened');
		} else {
			$(this).parent().find('.show_map').fadeIn(400);
			$('.ttl_city .click').removeClass('opened');
			$(this).addClass('opened')
		}
		return false;
	});
	$(document).on('click',function(event){
		//alert($(event.target).attr('class'));
		if (!$(event.target).closest('table').length) {
			$('.ttl_city').find('.show_map').fadeOut(400);
			$('.ttl_city .click').removeClass('opened');
		}
	});
	$(document).on('click', '.ttl_city .close', function(event){
		$('.ttl_city').find('.show_map').fadeOut(400);
		$('.ttl_city .click').removeClass('opened');
	});
	//

	$('.tabs').on('click', 'span:not(.active)', function() {
		$(this).addClass('active').siblings().removeClass('active')
				.parents('div.tabs_wrap').find('div.tabs_content').eq($(this).index()).fadeIn(150).siblings('div.tabs_content').hide();
	});

	// $('.tabs').on('click', 'span', function() {
	// 	var id_tabs = $(this).attr('data-tab');
	// 	var top_tabs = $('#'+id_tabs).offset().top;
	// 	$("html, body").animate({
	// 		scrollTop: top_tabs
	// 	}, 600);
	// });

	// открытие/закрытие нижнего блока сравнение\корзина
	$(document).on('click', '.bottom-panel-close', function() {
		$(this).parent('.bottom-panel').fadeOut(400,function(){
			$('.bottom-panel').addClass('hide');
		});
		$('.bottom-panel-small').slideDown(300);
		$.post('/service/basket?action=closebottompanel',{},function(data) {}, "json");
	});
	$(document).on('click', '.bottom-panel-small', function() {
		$(this).slideUp(300);
		$('.bottom-panel').fadeIn(400,function(){
			$('.bottom-panel').removeClass('hide');
		});
		$.post('/service/basket?action=openbottompanel',{},function(data) {}, "json");
	});

	//добавить в сравнение
	$(document).on('click', '.compareajax', function() {
		//добавление товара
		var obj = this;
		var id = $(this).attr('data-id');
		var item_url = $(this).parents('.catalog_item_list').children('a').first().attr('href');
		var item_ttl = $(this).parents('.catalog_item_list').find('.ttl').children('a').html();
		var  fields = {
			'addfavorite' : id
		}
		var tr_compare = '<div class="compare-item" rel="row_compare_'+id+'"><div class="compare-item-col"><a href="'+item_url+'">'+item_ttl+'</a></div><div class="compare-item-col"><noindex><a class="remove_comp" href="javascript:void(0);" data-id="'+id+'" rel="nofollow">Убрать</a></noindex></div></div>';
		$.get(location.pathname, fields,function(data) {
			if(data.res == "OK") {
				//добавление из блока справа
				if (data.num>=1) {
					$('.bx_catalog_compare_wrap').show();
				}
				$('.bx_catalog_compare_count span').html(data.num);
				$('.bx_catalog_compare_form_panel .compare-items').append(tr_compare);
				//end добавление из блока справа

				$('.bottom-panel-small').slideUp(300);
				if($('.bottom-panel').length){
					if($('.bottom-panel').hasClass('hide')){
						$('.bottom-panel').fadeIn(300).removeClass('hide');
						//$('.bottom-panel').removeClass('hide');
					}
					$('.bottom-panel #compare-info .count span').html(data.num);
				}
				$(obj).addClass('remajax').addClass('active').removeClass('compareajax').html('В сравнении');
				$(obj).next('a.to_compare_list').removeClass('inb').children('span').html(data.num);
				if (data.num>=2) {
					$('.bottom-panel #compare-info').addClass('active').attr('href','/catalog/compare');
					$(obj).next('a.to_compare_list').removeClass('hide_cmpr').attr('href','/catalog/compare').removeAttr('onclick');
				};
			} else if (data.res == "inadd") {
				window.location.href = url.path;
			} else if (data.res == "MAXCOUNT") {
				$('.show_maxcount_compare').fadeIn(300,function(){
					setTimeout(function(){ $('.show_maxcount_compare').fadeOut(300) }, 2500);
				});
			}
		}, "json");
		return false;
	});
	//удалить из сравнения
	$(document).on('click', '.remajax', function() {
		var obj = this;
		//удаление товара
		var id = $(this).attr('data-id');
		var  fields = {
			'delfavorite' : id
		};
		$.get(location.pathname,fields,function(data) {
			if(data.res == "OK") {
				//удаление из блока справа
				if(data.num==0){
					$('.bx_catalog_compare_wrap').hide();
				}
				$('.bx_catalog_compare_form_panel table').find('tr[rel=row_compare_'+id+']').remove();
				$('.bx_catalog_compare_count span').html(data.num);
				//end удаление из блока справа

				if($('.bottom-panel').length){
					if(data.num==0){
						$('.bottom-panel #compare-info .count span').html(0);
						if ($('#panel-order-btn').hasClass('hide')) {
							$('.bottom-panel').fadeOut(300,function(){
								$('.bottom-panel').addClass('hide');
							})
							$('.bottom-panel-small').slideUp(300);
						}
					} else {
						$('.bottom-panel #compare-info .count span').html(data.num);
					}
				}
				$(obj).addClass('compareajax').removeClass('active').removeClass('remajax').html('Сравнить');
				$(obj).next('a.to_compare_list').children('span').html(data.num);
				if (data.num<=1) {
					$('.bottom-panel #compare-info').removeClass('active').removeAttr('href');
					$(obj).next('a.to_compare_list').addClass('hide_cmpr');
				}
			};
		}, "json");
		return false;
	});

	$(document).on("click", 'a.save', function(){
		$(this).parents('form').submit();
		return false;
	});
        $(document).on('change', 'select.choose_size', function() {
            id =  $(this).attr('data-idp');
            size = $(this).find('option:selected').val();

            var fields = {
                    'action' : 'FIND',
                    'id' : id,
                    'size': size

            }

            $.post('/ajax/basket.php', fields, function(data) {
                btn = $('.basket_btn a[data-id="'+id+'"');
                if (data == 'YES'){
                    if ($(btn).hasClass('in_item')) {
                        $(btn).attr('href', '/personal/cart/').html('Товар в <span>корзине</span>');
                        $(btn).attr('class', 'in_basket').unbind('click');
                    }
                }
                if (data == 'NO'){
                    if ($(btn).hasClass('in_basket')) {
                        $(btn).attr('href', 'javascript:;').html('В корзину');
                        $(btn).attr('class', 'basket basketajax in_item js-add-basket basket-buy-cart');
                        btn.click(ADD2BASKET);

                    }
                }
            });
        });
        $(document).on('click', '.basketajax, .js-basket', ADD2BASKET);
	//добавление товаров по Акции Вместе Дешевле
	$(document).on('click', '.tog_addtobasket a.basket.ajx', function() {
		var ajaxurl = $(this).attr('href');
		var obj=$(this);
		//console.log(ajaxurl);
		$.post(ajaxurl,{},function(data) {
			if(data.res == "OK") {
				$('.bottom-panel-small').slideUp(300);
				$('#basket-head-info').html('<a href="/service/basket"><span>'+data.total.count+'</span></a>');
				$('#basket-head-info-1').html('<a href="/service/basket"><span>'+data.total.count+'</span></a>');
				if($('.bottom-panel').length){
					if($('.bottom-panel').hasClass('hide')){
						$('.bottom-panel').fadeIn(300).removeClass('hide');
					}
					if($('#panel-order-btn').hasClass('hide')){
						$('#panel-order-btn').removeClass('hide');
					}
					$('.bottom-panel #basket-info .count span').html(data.total.count);
				}
				//
				obj.after('<a class="basket added" href="/service/basket" title="Перейти в корзину">Товары в <span>корзине</span></a>');
				obj.remove();
			}
		}, "json");
		return false;
	});

  	$(document).on('click', ".styled_radio", function(){
			$(".styled_radio").removeClass('active');
			$(this).addClass('active');
			$(this).parent().find('input').val($(this).attr('value'));
		});
	// $('ul.tabs').on('click', 'li:not(.current)', function() {
	// 	$(this).addClass('current').siblings().removeClass('current').parents('div#tabs').find('div.box').eq($(this).index()).slideDown(300).siblings('div.box').slideUp(300);
	// });

	$("a.fancy").fancybox({
		helpers: {
			overlay: {
				locked: false,
				css: {
					'background': 'rgba(15,20,29,0.70)'
				}
			},
			title: {
				type: 'inside'
			}
		},
		padding: 0
	});
	$("a.fancyoneclick").fancybox({
		helpers: {
			overlay: {
				locked: false,
				css: {
					'background': 'rgba(15,20,29,0.70)'
				}
			},
			title: null
		},
		padding: 0
	});
	$(".fancybox_deshevle").fancybox({
		type 		: 'ajax',
		href		: '/nashli_deshevle_form.php',
		//maxWidth	: 800,
		//maxHeight	: 600,
		//fitToView	: false,
		//width		: '90%',
		//height	: '90%',
		//autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});

	$(document).on('click', '.notAvable,.basket-button-error', function() {
		return false;
	});

	$(document).on('submit', '.basket_page td.count form', function(){
		return false;
	});


	$(document).on('click', 'input[name=DELIVERY_ID]', function(){

		var allsum = $('#allSum_FORMATED').attr('data-price');

		if ($(this).val() == 2){
			$('#basket_items:last-child').append('<tr class="item_dostavka"><td class="margin">&nbsp;</td><td class="itemphoto">&nbsp;</td><td class="item">Доставка по городу</td><td class="price">400<span class="rur">i</span></td><td class="custom">1</td><td class="custom">400<span class="rur">i</span></td><td class="control">&nbsp;</td><td class="margin">&nbsp;</td></tr>');
			$('.paysumm .summ').html(parseInt(allsum)+400 + ' <span class="rur">i</span>').attr('data-delivery', 400).css({'color': '#C30202'});
		} else {
			$('tr.item_dostavka').remove();
			$('.paysumm .summ').html(parseInt(allsum) + '<span class="rur">i</span>').attr('data-delivery', 0).css({'color': '#BB2941'});
		}
	});


	/*
		var allsum = $('#allSum_FORMATED').attr('data-price').replace(' ', '');
	if ($('input[name=DELIVERY_ID]:checked').val() == 2) {
		$('#basket_items:last-child').append('<tr class="item_dostavka"><td>&nbsp;</td><td>Доставка по городу</td><td>400<span class="rur">i</span></td><td>1</td><td>400<span class="rur">i</span></td><td>&nbsp;</td></tr>');
		$('.paysumm .summ').text(parseInt(allsum)+400).css({'color': '#C30202'});
	}
*/
	$('#address_costom').blur(function(){
		var allsum = $('#allSum_FORMATED').attr('data-price');
		if(($(this).val() != '') && ($(this).val() != 'Другой город')){
			$('#address_drugoy').attr('checked','checked');
			$('tr.item_dostavka').html('<td class="margin">&nbsp;</td><td class="itemphoto">&nbsp;</td><td class="item">Доставка до вашего адреса будет рассчитана отдельно</td><td class="price">0<span class="rur">i</span></td><td class="custom">1</td><td class="custom">0<span class="rur">i</span></td><td class="control">&nbsp;</td><td class="margin">&nbsp;</td></tr>');
			$('#allSum_FORMATED').html(parseInt(allsum)+ ' <span class="rur">i</span>').attr('data-delivery', 0).css({'color': '#BB2941'});
		}
	});
	// $('.realCount').on('click',function() {
	// 	$(this).closest('td').find('input.count').val($(this).attr('real'));
	// 	$(this).closest('td').find('input.count').change();
	// });

	$(document).on('click', "#order_send", function(){
		$.post("/ajax/service/basket/",$(this).closest('form').serialize(), function(data){
			$("#popUp").remove();
			$("#content").html(data);
		});
		return false;
	});

	$(document).on('click', ".basket-button", function(){
		$("#popUp").show();
		return false;
	});

	$(document).on('click', "#popUp a.close", function(){
		$("#popUp").hide();
		return false;
	});
	if (window.location.hash == '#open_comments' || $('#messages_comments').children('p').length) {
		$('.tabs_wrap .tabs span').removeClass('active');
		$('.tabs_wrap .tabs_content').removeClass('active');
		$('.tabs_wrap .tabs span.com_tab').addClass('active');
		$('.tabs_wrap .comments_tabs').addClass('active');
		$('#comment-form').show();
		if ($('#messages_comments').children('p').length) {
			window.location.hash='#comment-form';
		}
	} else {
		$('.tabs_wrap .tabs span').removeClass('active');
		$('.tabs_wrap .tabs span').first().addClass('active');
		$('.tabs_wrap .tabs_content').removeClass('active');
		$('.tabs_wrap .tabs_content').first().addClass('active');
	}
});
$(window).load(function (){
	if (window.location.hash == '#open_comments' || $('#messages_comments').children('p').length) {
		$('.tabs_wrap .tabs span').removeClass('active');
		$('.tabs_wrap .tabs_content').removeClass('active');
		$('.tabs_wrap .tabs span.com_tab').addClass('active');
		$('.tabs_wrap .comments_tabs').addClass('active');
		$('#comment-form').show();
		if ($('#messages_comments').children('p').length) {
			window.location.hash='#comment-form';
		}
	} else {
		$('.tabs_wrap .tabs span').removeClass('active');
		$('.tabs_wrap .tabs span').first().addClass('active');
		$('.tabs_wrap .tabs_content').removeClass('active');
		$('.tabs_wrap .tabs_content').first().addClass('active');
	}
});
function parseURL(url) {
	var a = document.createElement('a');
	a.href = url;
	return {
		source: url,
		protocol: a.protocol.replace(':',''),
		host: a.hostname,
		port: a.port,
		query: a.search,
		params: (function(){
			var ret = {},
			seg = a.search.replace(/^\?/,'').split('&'),
			len = seg.length, i = 0, s;
			for (;i<len;i++) {
				if (!seg[i]) { continue; }
				s = seg[i].split('=');
				ret[s[0]] = s[1];
			}
			return ret;
		})(),
		file: (a.pathname.match(/\/([^\/?#]+)$/i) || [,''])[1],
		hash: a.hash.replace('#',''),
		path: a.pathname.replace(/^([^\/])/,'/$1'),
		relative: (a.href.match(/tp:\/\/[^\/]+(.+)/) || [,''])[1],
		segments: a.pathname.replace(/^\//,'').split('/')
	};
}

var checkboxHeight = "15";
var radioHeight = "14";

var Custom = {
	init: function() {
		var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
		for(a = 0; a < inputs.length; a++) {
			if((inputs[a].type == "checkbox" || inputs[a].type == "radio") && inputs[a].className == "styled") {
				span[a] = document.createElement("span");
				span[a].className = inputs[a].type;

				if(inputs[a].checked == true) {
					if(inputs[a].type == "checkbox") {
						position = "0 -" + (checkboxHeight) + "px";
						span[a].style.backgroundPosition = position;
					} else {
						position = "0 -" + (radioHeight) + "px";
						span[a].style.backgroundPosition = position;
					}
				}
				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				inputs[a].onchange = Custom.clear;
				if(!inputs[a].getAttribute("disabled")) {
					span[a].onmousedown = Custom.pushed;
					span[a].onmouseup = Custom.check;
				} else {
					span[a].className = span[a].className += " disabled";
				}
			}
		}
		document.onmouseup = Custom.clear;
	},
	pushed: function() {
		element = this.nextSibling;
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
		} else if(element.checked == true && element.type == "radio") {
			this.style.backgroundPosition = "0 -" + radioHeight + "px";
		} else if(element.checked != true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
		} else {
			this.style.backgroundPosition = "0 -" + radioHeight + "px";
		}
	},
	check: function() {
		element = this.nextSibling;
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 0";
			element.checked = false;
		} else {
			if(element.type == "checkbox") {
				this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
			} else {
				this.style.backgroundPosition = "0 -" + radioHeight + "px";
				group = this.nextSibling.name;
				inputs = document.getElementsByTagName("input");
				for(a = 0; a < inputs.length; a++) {
					if(inputs[a].name == group && inputs[a] != this.nextSibling) {
						inputs[a].previousSibling.style.backgroundPosition = "0 0";
					}
				}
			}
			element.checked = true;
		}
	},
	clear: function() {
		inputs = document.getElementsByTagName("input");
		for(var b = 0; b < inputs.length; b++) {
			if(inputs[b].type == "checkbox" && inputs[b].checked == true && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight + "px";
			} else if(inputs[b].type == "checkbox" && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			} else if(inputs[b].type == "radio" && inputs[b].checked == true && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + radioHeight + "px";
			} else if(inputs[b].type == "radio" && inputs[b].className == "styled") {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			}
		}
	}
}
window.onload = Custom.init;

function catalogItemMobile() {
	$.each($(".catalog_item_list"), function() {
		var _this = $(this),
				old = _this.find(".catalog_item_no_mobile"),
				titleClone = _this.find(".ttl").clone(),
				titleMobile = _this.find(".catalog_item_mobile_title"),
				info = _this.find(".catalog_item_mobile_info"),
				stars = _this.find(".dop_abs .security").clone(),
				price = _this.find(".price").clone(),
				lenghtP = _this.find(".catalog_item_length").clone(),
				weight = _this.find(".catalog_item_weight").clone(),
				reviews = _this.find(".review_item_").clone(),
				compare = _this.find(".compare_item_").clone(),
				other_color = _this.find(".other_color").clone(),
				basketBtn = _this.find(".basket").clone(),
				catalog_item_mobile_btn = _this.find(".catalog_item_mobile_btn"),
				loader = _this.find(".loader");

		if ( old.length ) {
			titleMobile.empty().append(titleClone);
			info.empty().append(stars).append(price).append(lenghtP).append(weight).append(reviews);
			catalog_item_mobile_btn.empty().append(compare).append(other_color).append('<div class="catalog_item_mobile_info_btn"></div>').find(".catalog_item_mobile_info_btn").append(basketBtn);
			var datachOld = old.detach();
		}

		if ( loader.length ) {
			loader.removeClass("loader_active");
		}

	});
}

function ADD2BASKET() {

		var th = this;

		var id = $(this).attr('data-id');

                var size = $('select[data-id="select_size_'+id+'"] option:selected').attr('data-id');

                var fields = {
			'action' : 'BUY',
			'id' : id,
			'ajax_basket' : 'Y',
                        'prop' : { 'none':'none'}
                    }

                if(typeof size !== "undefined"){
                    fields["prop"]['SIZE']=size;
                }

                //console.log(fields);
		if($(this).attr('data-addid') !== undefined) {

			var fields2 = {
				'action' : 'BUY',
				'id' : $(this).attr('data-addid'),
				'ajax_basket' : 'Y'

			}

			$.get(location.pathname, fields2, function(data) {})
		}

		$.get(location.pathname, fields, function(data) {

		  if(data.indexOf("OK") != -1) {

			var fields = {
				'templateName' : 'basket_top',
				'siteId' : BX.message("SITE_ID"),
				'sessid': BX.bitrix_sessid()
			}

			$.post('/bitrix/components/bitrix/sale.basket.basket.line/ajax.php', fields, function(html) {

				var nums = $(html).find('span').text();


				$('.bottom-panel-small').slideUp(300);

				$('#basket-head-info').html('<a href="/personal/cart/"><span>' + nums + '</span></a>');
				$('#basket-head-info-1').html('<a href="/personal/cart/"><span>' + nums + '</span></a>');
				if($('.bottom-panel').length){

					if($('.bottom-panel').hasClass('hide')){

						$('.bottom-panel').fadeIn(300).removeClass('hide');

					}

					if($('#panel-order-btn').hasClass('hide')){

						$('#panel-order-btn').removeClass('hide');
					}

					$('.bottom-panel #basket-info .count span').html(nums);
				}

				if ($(th).hasClass('in_item')) {
					$(th).attr('href', '/personal/cart/').html('Товар в <span>корзине</span>');
				} else {
					$(th).attr('href', '/personal/cart/');
				}


				$(th).attr('class', 'in_basket').unbind('click');
			})
		  }
		  else
			alert('Товар не удалось добавить в корзину, попробуйте позже');
		})
		return false;
	}
