$(document).ready(function(){
	if (!$('.filters_wrap').parent().hasClass('center')) {
		$('.filters_wrap').css({'top':'40px'});
	}
	var height_pre_text_catalog_list = parseInt($('.margin_left_links_list').outerHeight());
	var height_catalog_list = height_pre_text_catalog_list+parseInt($('.catalog_list.w_filters').outerHeight());
	if (height_catalog_list < $('.filters_wrap').outerHeight()) {
		$('.catalog_list.w_filters').css({'height':( parseInt($('.filters_wrap').outerHeight()) - height_pre_text_catalog_list + 50)+'px'});
	}
	if ($('.pager_wrap').length) {
		var count_in_page = $('.pager_wrap').find('span.all_in_pager_items').text();
		if (count_in_page >= 0) {
			$('.smartfilter').find('.fromto').append('<p class="fitrer_fromto_count_items">найдено товаров: '+count_in_page+'</p>');
		}
	}
    if ($('.filters_wrap').length){
        scroll_filters();
    }
    $(window).resize(function() {
        if ($('.filters_wrap').length){
            scroll_filters();
        }
		var height_pre_text_catalog_list = parseInt($('.margin_left_links_list').outerHeight());
		var height_catalog_list = height_pre_text_catalog_list+parseInt($('.catalog_list.w_filters').outerHeight());
		if (height_catalog_list < $('.filters_wrap').outerHeight()) {
			$('.catalog_list.w_filters').css({'height':( parseInt($('.filters_wrap').outerHeight()) - height_pre_text_catalog_list + 50)+'px'});
		}
    });
});
function scroll_filters(){
    var scroll_div = $('.filters_wrap');
    var filter_top = parseInt($('.margin_left_links_list').offset().top);
    var window_height = parseInt($(window).outerHeight());
    var tempScrollTop = 0;
    var scrollTop = 0;
    var scrollUp = 0;
    var scrollDown = 0;
    var raznica2 = 0;
    var to_bottom = 80;
    $(window).scroll(function(){
        var scrollTop = $(window).scrollTop();
		var text_top = parseInt($('.catalog_list').offset().top)+parseInt($('.catalog_list').height());
        //высоту всегда узнаем, если вдруг фильтр развернут
        var filter_height = parseInt(scroll_div.outerHeight());
        var top_absolute = text_top-filter_height;
    	var raznica = (window_height-filter_height)-40;
        //если экран НЕ мобильного
    	if ($(window).width() > 760) {
            //2 вариант - если высота фильтра больше высоты экрана
            if (filter_height > window_height) {
            	if (scrollTop > top_absolute-to_bottom) {
            		//останавливаем перед текстом после товаров
                    if (scroll_div.hasClass('fixed_filters') && !scroll_div.hasClass('absolute_bottom_filters')) {
                        scroll_div.removeClass('fixed_filters');
                        scroll_div.addClass('absolute_bottom_filters');
                        scroll_div.css({'top':(top_absolute-filter_top-101)+'px'});
                    }
                }else if (scrollTop < filter_top){
                    //возвращаем на место вверху
                    scroll_div.removeClass('fixed_filters');
                    scroll_div.removeClass('absolute_bottom_filters');
					if ($('.filters_wrap').parent().hasClass('center')) {
						scroll_div.css({'top':'5px','bottom':'auto'});
					} else {
						scroll_div.css({'top':'40px','bottom':'auto'});
					}
                } else {
                	//прокручиваем вместе с экраном
                	if (!scroll_div.hasClass('fixed_filters')) {
	                	scroll_div.addClass('fixed_filters');
	                	scroll_div.removeClass('absolute_bottom_filters');
	                }
                	if (tempScrollTop < scrollTop ){
						//прокручиваем фильтр вниз
						//console.log('прокрутка идет вниз');
						scrollDown = scrollTop;
						if (scrollUp==0) {
							scrollUp = filter_top;
						}
						raznica2 = scrollUp-scrollDown;
	                	if ((raznica-raznica2) < 0 || to_bottom<raznica) {
	                		to_bottom = raznica2;
							scroll_div.css({'top':(raznica2)+'px','bottom':'auto'});
	                	} else {
	                		scroll_div.css({'top':(raznica)+'px','bottom':'auto'});
	                	}
					} else if (tempScrollTop > scrollTop ) {
						//прокручиваем фильтр вверх
						//console.log('прокрутка идет вверх');
						scrollUp = scrollTop;
						raznica2 = scrollUp-scrollDown;
						if ((raznica-raznica2) < 0) {
	                		scroll_div.css({'top':(raznica-raznica2)+'px','bottom':'auto'});
	                	} else {
	                		scroll_div.css({'top':'10px','bottom':'auto'});
	                	}
					}
					tempScrollTop = scrollTop;
            	}
            } else {
                //1 вариант
                //крепим снизу экрана
                if (scrollTop > top_absolute-101) {
                    if (scroll_div.hasClass('fixed_filters') && !scroll_div.hasClass('absolute_bottom_filters')) {
                        scroll_div.removeClass('fixed_filters');
                        scroll_div.addClass('absolute_bottom_filters');
                        scroll_div.css({'top':(top_absolute-filter_top-101)+'px','bottom':'auto'});
                    }
                //крепим сверху экрана
                } else if (scrollTop > filter_top) {
                    if (!scroll_div.hasClass('fixed_filters')) {
                        scroll_div.addClass('fixed_filters');
                        scroll_div.removeClass('absolute_bottom_filters');
                        scroll_div.css({'top':'6px','bottom':'auto'});
                    }
                }else{
                    //возвращаем на место вверху
                    if (scroll_div.hasClass('fixed_filters')) {
                        scroll_div.removeClass('fixed_filters');
                        scroll_div.removeClass('absolute_bottom_filters');
						if ($('.filters_wrap').parent().hasClass('center')) {
							scroll_div.css({'top':'5px','bottom':'auto'});
						} else {
							scroll_div.css({'top':'40px','bottom':'auto'});
						}

                    }
                }
        	}
    	}
    });
}
