<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
$panelview = 'show';

if(!empty($_GET['action'])) {

	if($_GET['action'] == 'openbottompanel')
		$_SESSION['viewpanel'] = 'show';
	else if ($_GET['action'] == 'closebottompanel')
		$_SESSION['viewpanel'] = 'hide';

}

if(!empty($_SESSION['viewpanel']))
	$panelview = $_SESSION['viewpanel'];

?>
<?/*!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"*/?>
<!DOCTYPE html>
<html lang="ru">
<head>
	<title><?$APPLICATION->ShowTitle()?> <? if (isset($_GET['PAGEN_1']) && $_GET['PAGEN_1'] > 1) : ?> | Страница <? echo $_GET['PAGEN_1']; ?><? endif; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="yandex-verification" content="882473a6ed003f9f" />
	<?/*<link href="//fonts.googleapis.com/css?family=Roboto:300,500,700,300italic,500italic,700italic&subset=latin,cyrillic-ext,cyrillic,latin-ext" rel="stylesheet">
	<link href="//fonts.googleapis.com/css?family=Roboto+Slab:400,300,700&subset=latin,cyrillic-ext,latin-ext,cyrillic" rel="stylesheet">*/?>
	<?/*<link rel="stylesheet" href="/bitrix/templates/bikelock/font.css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">*/?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/font.css");?>
    <?$APPLICATION->AddHeadString('<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">',true)?>

	<?$APPLICATION->ShowHead();?>


    <?  if ($USER->IsAdmin()){
      // var_dump($GLOBALS['countProduct']);
    }?>
	<script>var baskedAdded = <?=$GLOBALS['countProduct']['ids']?>;</script>
	<?
	$fList = getItemsFavorite();

	if(!empty($fList)) { ?>
		<script>var favoriteList = <?=$fList;?>;</script>
	<? } ?>
	<?/*link rel="shortcut icon" type="image/png" href="<?=SITE_TEMPLATE_PATH?>/images/fav.png"/*/?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/topinfo.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/fancybox.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery.jscrollpane.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/css/jquery-ui.min.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/jquery.bxslider.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/mmenu/jquery.mmenu.all.css");?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/owl.carousel.2.2.1/assets/owl.carousel.min.css");?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery/jquery-2.1.4.min.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.bxslider.min.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/fancybox.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery-ui.min.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.jscrollpane.min.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/effects.js?")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/topinfo.js")?>
	<?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/FancySelect/fancySelect.css")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/FancySelect/fancySelect.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/mmenu/jquery.mmenu.min.js")?>
	<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/owl.carousel.2.2.1/owl.carousel.min.js");?>
	<?
	//$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . "/js/fancyBox3/jquery.fancybox.min.css");
    //$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/fancyBox3/jquery.fancybox.min.js");
    $APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH . "/js/jquery.maskedinput.js");
	?>
	<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
<style>.ya-page_js_yes .ya-site-form_inited_no { display: none; }</style>
</head>
<body>
<?$APPLICATION->ShowPanel();?>
	<main>
        <header>
            <div class="header_mobile 22">
                <a href="#mobile-menu" class="header_mnu_burger"></a>
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                "AREA_FILE_SHOW" => "file",
                "PATH" => "/include/logo.php",
                "EDIT_TEMPLATE" => ""
                ),
                false
                );?>
				<?/*div class="header_logo"><a href="/"></a></div*/?>
                <ul class="header_mobile_nav">
                    <li class="header_mobile_nav_item call"><a href="#!"></a></li>
                    <li class="header_mobile_nav_item search"><a href="#!"></a></li>
                    <li class="header_mobile_nav_item cart"><a href="/personal/cart/"></a></li>
                </ul>
                <div id="mobile_phones"></div>
            </div>
            <div class="sticky-header">
                <div class="center wrapper clearfix">
                    <div class="header_menu">
                        <div class="center wrapper clearfix">
							<?/*if ($_SERVER['SCRIPT_NAME'] == "/index.php") {?>
                            	<img class="logo" src="/images/logo-small.png" alt="">
							<?}else{?>
            					<a href="/"><img class="logo" src="/images/logo-small.png" alt=""></a>
							<?} */?>
							<a href="/"><img class="logo" src="/images/logo-small.png" alt=""></a>
                            <?$APPLICATION->IncludeComponent("bitrix:menu", "menu_multi", Array(
                            "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                            "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                            "COMPONENT_TEMPLATE" => "catalog_horizontal",
                            "DELAY" => "N",	// Откладывать выполнение шаблона меню
                            "MAX_LEVEL" => "2",	// Уровень вложенности меню
                            "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                            "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                            "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                            "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                            "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                            "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                            "MENU_THEME" => "site",	// Тема меню
                            ),
                            false
                            );?>
                            <div class="header_location_list">
                                <div class="ttl_city header_location_item">
                                    Мск
                                </div>
                                <div class="header_phone_item"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone_moscow.php"), false);?></div>
                                <span class="divider"></span>
                                <div class="ttl_city header_location_item">
                                    Спб
                                </div>
                                <div class="header_phone_item"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone_spb.php"), false);?></div>
                            </div>
                            <div class="h_right">
                                <a class="instagram-link" target="_blank" href="https://www.instagram.com/fineshoes.ru/"></a>
                                <!--noindex-->
                                <? if(!$USER->IsAuthorized()) {?>
                                    <div class="login_block_wrap">
                                        <a class="icon_login" href="#">Вход</a>
                                        <div class="cabinet-block">
                                            <form action="/auth/?login=yes" method="post">
                                                <input type="hidden" name="AUTH_FORM" value="Y" />
                                                <input type="hidden" name="TYPE" value="AUTH" />
                                                <div class="cabinet-block-input">
                                                    <label>Логин (E-mail):</label>
                                                    <input class="text" type="text" name="USER_LOGIN" value=""  id="login"/>

                                                    <label>Пароль:</label>
                                                    <input class="text" type="password" name="USER_PASSWORD" value="" id="password"/>

                                                    <div class="clear"></div>
                                                    <div class="cabinet-login-submits">
                                                        <div class="cabinet-login-links">
                                                            <a class="small-link recovery" href="/auth/?forgot_password=yes">Забыли пароль?</a>
                                                            <a class="small-link register" href="/auth/?register=yes">Регистрация</a>
                                                        </div>
                                                        <input class="submitz" type="submit" name="save" value="Вход" />
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </form>
                                        </div>
                                    </div>
                                <? } else { ?>

                                <div class="login_block_wrap w_user">
                                    <a class="icon_login" href="#"><?=$USER->GetLogin()?></a>
                                    <div class="cabinet-block">
                                        <form action="/personal/" method="post">
                                            <div class="cabinet-block-menu">
                                                <a href="/personal/private/">Личные данные</a>
                                                <a href="/personal/address/">Адрес доставки</a>
                                                <a href="/personal/orders/">Мои заказы</a>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="cabinet-login-submits">
                                                <a class="submitz" href="/?logout=yes" class="logout">Выйти из профиля</a>
                                            </div>
                                            <div class="clear"></div>
                                        </form>
                                    </div>
                                </div>			<? } ?>
                                <div class="bakset_block" id="basket-head-info-1">
                                    <?$APPLICATION->IncludeComponent(
                                    "bitrix:sale.basket.basket.line",
                                    "basket_top",
                                    array(
                                    "HIDE_ON_BASKET_PAGES" => "Y",
                                    "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
                                    "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
                                    "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                                    "PATH_TO_PROFILE" => SITE_DIR."personal/",
                                    "PATH_TO_REGISTER" => SITE_DIR."login/",
                                    "POSITION_FIXED" => "N",
                                    "SHOW_AUTHOR" => "N",
                                    "SHOW_EMPTY_VALUES" => "Y",
                                    "SHOW_NUM_PRODUCTS" => "Y",
                                    "SHOW_PERSONAL_LINK" => "Y",
                                    "SHOW_PRODUCTS" => "N",
                                    "SHOW_TOTAL_PRICE" => "Y",
                                    "COMPONENT_TEMPLATE" => "basket_top"
                                    ),
                                    false
                                    );?>
                                </div>
                                <!--/noindex-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border"></div>
            </div>
            <div class="header clearfix header_mobile_search">
                <div class="center wrapper clearfix">
                    <div class="header_phones">
                        <div class="search_block">
                            <div class="select_search"><a class="switch_search" href="#">Поиск от Яндекс</a></div>
								<?$APPLICATION->IncludeComponent(
									"bitrix:search.form",
									"header",
									Array(
										"PAGE" => "#SITE_DIR#search/index.php",
										"USE_SUGGEST" => "Y"
									) 
								);?>
                            <div id="form_search_yandex">
                                <div class="ya-site-form ya-site-form_inited_no" onclick="return {'action':'/search-yandex/','arrow':false,'bg':'transparent','fontsize':14,'fg':'#000000','language':'ru','logo':'rb','publicname':'Поиск по bikelock.ru','suggest':true,'target':'_self','tld':'ru','type':3,'usebigdictionary':true,'searchid':2277061,'input_fg':'#636466','input_bg':'#ffffff','input_fontStyle':'normal','input_fontWeight':'bold','input_placeholder':null,'input_placeholderColor':'#000000','input_borderColor':'#ffffff'}"><form action="https://yandex.ru/search/site/" method="get" target="_self" accept-charset="utf-8"><input type="hidden" name="searchid" value="2277061"/><input type="hidden" name="l10n" value="ru"/><input type="hidden" name="reqenc" value="utf-8"/><input type="search" name="text" value=""/><input type="submit" value="Найти"/></form></div>
                                <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/ext_scripts/yandex_search1.js"></script>
                                <?/*<script type="text/javascript">(function(w,d,c){var s=d.createElement('script'),h=d.getElementsByTagName('script')[0],e=d.documentElement;if((' '+e.className+' ').indexOf(' ya-page_js_yes ')===-1){e.className+=' ya-page_js_yes';}s.type='text/javascript';s.async=true;s.charset='utf-8';s.src=(d.location.protocol==='https:'?'https:':'http:')+'//site.yandex.net/v2.0/js/all.js';h.parentNode.insertBefore(s,h);(w[c]||(w[c]=[])).push(function(){Ya.Site.Form.init()})})(window,document,'yandex_site_callbacks');</script>*/?>
                            </div>
                        </div>
                        <div class="header-new">
                            <a class="header-link" href="http://fineshoesing.ru/" target="_blank">
                                <span class="strong">Fineshoesing</span>
                                Уход за обувью
                            </a><a class="header-link last" href="http://fineshoe.ru/" target="_blank">
                                <span class="strong">Fineshoe</span>
                                Ремонт
                            </a>
                            <div class="add-menu">
                                <a class="add-menu-link" href="/delivery-and-payment/">Доставка, оплата</a>
                                <a class="add-menu-link" href="/contacts/">Контакты</a>
                            </div>

                            <div class="header_location">
                                <div class="header_location_title">
                                    <img src="/images/placemark.png" alt="" width="13" />
                                </div>
                                <div class="header_location_list">
                                    <div class="ttl_city header_location_item">
                                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_moscow.php"), false);?>
                                    </div>
                                    <div class="header_phone_item"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone_moscow.php"), false);?></div>
                                    <span class="divider"></span>
                                    <div class="ttl_city header_location_item">
                                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_spb.php"), false);?>
                                    </div>
                                    <div class="header_phone_item"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone_spb.php"), false);?></div>
                                    <span class="divider"></span>
                                    <div class="header_phone_item"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone_russia.php"), false);?></div>
                                </div>
                            </div>
                            <ul class="header-search">
                                <li>
                                    <a class="instagram-link" target="_blank" href="https://www.instagram.com/fineshoes.ru/"></a>
                                </li>
                                <li class="search-wr">
                                    <a class="search-link" href=""></a>
                                    <div class="search_block">
                                        <div class="select_search"><a class="switch_search" href="#">Поиск от Яндекс</a></div>
											<?$APPLICATION->IncludeComponent(
												"bitrix:search.form",
												"header",
												Array(
													"PAGE" => "#SITE_DIR#search/index.php",
													"USE_SUGGEST" => "Y"
												) 
											);?>
                                        <div id="form_search_yandex">
                                            <div class="ya-site-form ya-site-form_inited_no" onclick="return {'action':'/search-yandex/','arrow':false,'bg':'transparent','fontsize':14,'fg':'#000000','language':'ru','logo':'rb','publicname':'Поиск по bikelock.ru','suggest':true,'target':'_self','tld':'ru','type':3,'usebigdictionary':true,'searchid':2277061,'input_fg':'#636466','input_bg':'#ffffff','input_fontStyle':'normal','input_fontWeight':'bold','input_placeholder':null,'input_placeholderColor':'#000000','input_borderColor':'#ffffff'}"><form action="https://yandex.ru/search/site/" method="get" target="_self" accept-charset="utf-8"><input type="hidden" name="searchid" value="2277061"/><input type="hidden" name="l10n" value="ru"/><input type="hidden" name="reqenc" value="utf-8"/><input type="search" name="text" value=""/><input type="submit" value="Найти"/></form></div>
                                            <script type="text/javascript" src="<?=SITE_TEMPLATE_PATH?>/js/ext_scripts/yandex_search2.js"></script>
                                            <?/*<script type="text/javascript">(function(w,d,c){var s=d.createElement('script'),h=d.getElementsByTagName('script')[0],e=d.documentElement;if((' '+e.className+' ').indexOf(' ya-page_js_yes ')===-1){e.className+=' ya-page_js_yes';}s.type='text/javascript';s.async=true;s.charset='utf-8';s.src=(d.location.protocol==='https:'?'https:':'http:')+'//site.yandex.net/v2.0/js/all.js';h.parentNode.insertBefore(s,h);(w[c]||(w[c]=[])).push(function(){Ya.Site.Form.init()})})(window,document,'yandex_site_callbacks');</script>*/?>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
						<div class="header_location-2">
                                <div class="header_location_title">
                                    <img src="/images/placemark.png" alt="" width="13" />
                                </div>
                                <div class="header_location_list">
                                    <div class="ttl_city header_location_item">
                                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_moscow.php"), false);?>
                                    </div>
                                    <div class="header_phone_item"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone_moscow.php"), false);?></div>
                                    <span class="divider"></span>
                                    <div class="ttl_city header_location_item">
                                        <?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/company_spb.php"), false);?>
                                    </div>
                                    <div class="header_phone_item"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone_spb.php"), false);?></div>
                                    <?/*<span class="divider"></span>
                                    <div class="header_phone_item"><?$APPLICATION->IncludeComponent("bitrix:main.include", "", array("AREA_FILE_SHOW" => "file", "PATH" => SITE_DIR."include/phone_russia.php"), false);?></div>
									*/?>
								</div>
                            </div>
						<?/*
						<script type="text/javascript">
							$(document).ready(function(){
								ymaps.ready(initMapContainer);
								function initMapContainer() {
									myMap_9 = new ymaps.Map("map-header-9", {
										center: [55.751867,37.416871],
										zoom: 16,
										controls: ['zoomControl', 'typeSelector', 'fullscreenControl', 'geolocationControl','trafficControl']
									});
									myCollection_9 = new ymaps.GeoObjectCollection();
									myCollection_9.add(new ymaps.Placemark([55.751867,37.416871],{balloonContent: '<p>121609, г. Москва,<br/> ул. Крылатские холмы, д. 15К2<br/>+7 495 22-55-480</p>'}));
									myMap_9.geoObjects.add(myCollection_9);

									myMap_10 = new ymaps.Map("map-header-10", {
										center: [59.920239,30.356795],
										zoom: 16,
										controls: ['zoomControl', 'typeSelector', 'fullscreenControl', 'geolocationControl','trafficControl']
									});
									myCollection_10 = new ymaps.GeoObjectCollection();
									myCollection_10.add(new ymaps.Placemark([59.920239,30.356795],{balloonContent: '<p>191119, г. Санкт-Петербург,<br/> ул. Лиговский проспект, д. 92Д<br/>+7 812 42-612-42</p>'}));
									myMap_10.geoObjects.add(myCollection_10);
								};
							});
						</script>
						*/?>
                    </div>

                    <div class="clear"></div>
                </div>
            </div>
            <div class="header_menu main_menu_">
                <div class="center wrapper clearfix">
                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                    "AREA_FILE_SHOW" => "file",
                    "PATH" => "/include/logo.php",
                    "EDIT_TEMPLATE" => ""
                    ),
                    false
                    );?>
				<?$APPLICATION->IncludeComponent("bitrix:menu", "menu_multi", Array(
                    "ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
                    "CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
                    "COMPONENT_TEMPLATE" => "catalog_horizontal",
                    "DELAY" => "N",	// Откладывать выполнение шаблона меню
                    "MAX_LEVEL" => "2",	// Уровень вложенности меню
                    "MENU_CACHE_GET_VARS" => "",	// Значимые переменные запроса
                    "MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
                    "MENU_CACHE_TYPE" => "N",	// Тип кеширования
                    "MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
                    "ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
                    "USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
                    "MENU_THEME" => "site",	// Тема меню
                    ),
                    false
                    );?>
                    <div class="h_right">
                        <!--noindex-->
                        <? if(!$USER->IsAuthorized()) {?>
                            <div class="login_block_wrap">
                                <a class="icon_login" href="#">Вход</a>
                                <div class="cabinet-block">
                                    <form action="/auth/?login=yes" method="post">
                                        <input type="hidden" name="AUTH_FORM" value="Y" />
                                        <input type="hidden" name="TYPE" value="AUTH" />
                                        <div class="cabinet-block-input">
                                            <label>Логин (E-mail):</label>
                                            <input class="text" type="text" name="USER_LOGIN" value=""  id="login"/>

                                            <label>Пароль:</label>
                                            <input class="text" type="password" name="USER_PASSWORD" value="" id="password"/>

                                            <div class="clear"></div>
                                            <div class="cabinet-login-submits">
                                                <div class="cabinet-login-links">
                                                    <a class="small-link recovery" href="/auth/?forgot_password=yes">Забыли пароль?</a>
                                                    <a class="small-link register" href="/auth/?register=yes">Регистрация</a>
                                                </div>
                                                <input class="submitz" type="submit" name="save" value="Вход" />
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </form>
                                </div>
                            </div>
                        <? } else { ?>

                        <div class="login_block_wrap w_user">
                            <a class="icon_login" href="#"><?=$USER->GetLogin()?></a>
                            <div class="cabinet-block">
                                <form action="/personal/" method="post">
                                    <div class="cabinet-block-menu">
                                        <a href="/personal/private/">Личные данные</a>
                                        <a href="/personal/address/">Адрес доставки</a>
                                        <a href="/personal/orders/">Мои заказы</a>
                                    </div>
                                    <div class="clear"></div>
                                    <div class="cabinet-login-submits">
                                        <a class="submitz" href="/?logout=yes" class="logout">Выйти из профиля</a>
                                    </div>
                                    <div class="clear"></div>
                                </form>
                            </div>
                        </div>			<? } ?>
                        <div class="bakset_block" id="basket-head-info">
                            <?$APPLICATION->IncludeComponent(
                            "bitrix:sale.basket.basket.line",
                            "basket_top",
                            array(
                            "HIDE_ON_BASKET_PAGES" => "Y",
                            "PATH_TO_BASKET" => SITE_DIR."personal/cart/",
                            "PATH_TO_ORDER" => SITE_DIR."personal/order/make/",
                            "PATH_TO_PERSONAL" => SITE_DIR."personal/",
                            "PATH_TO_PROFILE" => SITE_DIR."personal/",
                            "PATH_TO_REGISTER" => SITE_DIR."login/",
                            "POSITION_FIXED" => "N",
                            "SHOW_AUTHOR" => "N",
                            "SHOW_EMPTY_VALUES" => "Y",
                            "SHOW_NUM_PRODUCTS" => "Y",
                            "SHOW_PERSONAL_LINK" => "Y",
                            "SHOW_PRODUCTS" => "N",
                            "SHOW_TOTAL_PRICE" => "Y",
                            "COMPONENT_TEMPLATE" => "basket_top"
                            ),
                            false
                            );?>
                        </div>
                        <!--/noindex-->
                    </div>
                </div>
            </div>
        </header>
		<div class="clear"></div>
		<div class="center">
			<div class="wrapper">
				<div class="content">
					<? if($APPLICATION->GetCurPage() != '/' && strpos($APPLICATION->GetCurPage(), '/auth/') === false) {?>
						<div class="breadcrumbs">
							<?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"bread",
	array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0",
		"COMPONENT_TEMPLATE" => "bread"
	),
	false
);?>
						</div>
					<? } ?>
