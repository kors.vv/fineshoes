<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>
<div class="instagram-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>

    <div id="<?=$this->GetEditAreaId($arItem['ID']);?>" class="instagram-post">
        <a href="<?=$arItem["PROPERTIES"]["ORIGINAL_URL"]["VALUE"]?>" target="_blank">
            <img class="img-responsive" src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>">
        </a>
        <div class="instagram-post-footer">
            <div class="post-likes">
                <i class="fa fa-heart-o"></i><?=$arItem["PROPERTIES"]["LIKES"]["VALUE"]?>
            </div>
            <div class="post-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
        </div>
    </div>

<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
