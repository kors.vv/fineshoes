<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list about-shoes-list">
<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?><br />
<?endif;?>
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="news-item-about-shoes">
		<div class="news-item-img">
		  <?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
			<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
				<?if($arItem["PREVIEW_PICTURE"]["SRC"]){?>
					<img
						class="preview_picture"
						border="0"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						style="float:left"
					/>
				<?}else{
					$arFileTmp = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]['ID'],array("width" => 300, "height" => 1000),BX_RESIZE_IMAGE_PROPORTIONAL,true);?>
					<img
						class="preview_picture"
						border="0"
						src="<?=$arFileTmp["src"]?>"
						height="auto"
						alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"
						title="<?=$arItem["DETAIL_PICTURE"]["TITLE"]?>"
						style="float:left"
				<?}?>
			</a>
		  <?else:?>
			<?if($arItem["PREVIEW_PICTURE"]["SRC"]){?>
					<img
						class="preview_picture"
						border="0"
						src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
						width="<?=$arItem["PREVIEW_PICTURE"]["WIDTH"]?>"
						height="<?=$arItem["PREVIEW_PICTURE"]["HEIGHT"]?>"
						alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
						title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
						style="float:left"
					/>
				<?}else{
					$arFileTmp = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]['ID'],array("width" => 300, "height" => 1000),BX_RESIZE_IMAGE_PROPORTIONAL,true);?>
					<img
						class="preview_picture"
						border="0"
						src="<?=$arFileTmp["src"]?>"
						height="auto"
						alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"
						title="<?=$arItem["DETAIL_PICTURE"]["TITLE"]?>"
						style="float:left"
				<?}?>
		  <?endif;?>
		</div>
		<div class="news-item-desc">
		  <?if($arParams["DISPLAY_NAME"]!="N" && $arItem["NAME"]):?>
				<?if(!$arParams["HIDE_LINK_WHEN_NO_DETAIL"] || ($arItem["DETAIL_TEXT"] && $arResult["USER_HAVE_ACCESS"])):?>
					<div class="news-item-desc-title-div"><a href="<?echo $arItem["DETAIL_PAGE_URL"]?>" class="news-item-desc-title"><?echo $arItem["NAME"]?></a></div>
				<?else:?>
					<b><?echo $arItem["NAME"]?></b><br />
				<?endif;?>
		  <?endif;?>
		  <?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arItem["PREVIEW_TEXT"]):?>
				<p><?echo $arItem["PREVIEW_TEXT"];?></p>
		  <?endif;?>
			  
			<?foreach($arItem["DISPLAY_PROPERTIES"]["HASHTAG"]["LINK_ELEMENT_VALUE"] as $hashtag):?>
		      <a href="/about-shoes/<?=$hashtag["CODE"]?>/" class="news-item-desc-a">#<?=str_replace(' ', '', $hashtag["NAME"]);?></a>
		    <?endforeach;?>
			
		</div>
	</div>
<?endforeach;?>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<br /><?=$arResult["NAV_STRING"]?>
<?endif;?>
</div>
