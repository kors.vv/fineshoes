<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="list-module-links">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		//print_r($arItem);
	?>
	<?php if (!empty($arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"]) && !empty($arItem["NAME"])) { ?>
		<div class="module-links-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
			<?php if (!empty($arItem["DISPLAY_PROPERTIES"]["COLOR_BG"]["VALUE"]) || !empty($arItem["DISPLAY_PROPERTIES"]["COLOR_TEXT"]["VALUE"])) {
				$style = "style='";
			} else {
				$style = "";
			}?>	
			<?php if (!empty($arItem["DISPLAY_PROPERTIES"]["COLOR_BG"]["VALUE"])) {
				$COLOR_BG = "background:".$arItem["DISPLAY_PROPERTIES"]["COLOR_BG"]["VALUE"].";";
				$style .= $COLOR_BG;
			} ?>
			<?php if (!empty($arItem["DISPLAY_PROPERTIES"]["COLOR_TEXT"]["VALUE"])) {
				$COLOR_TEXT = "color:".$arItem["DISPLAY_PROPERTIES"]["COLOR_TEXT"]["VALUE"].";";
				$style .= $COLOR_TEXT;
				$style .= "' ";
			} ?>
			<?php if (empty($COLOR_TEXT) && !empty($COLOR_BG)) {
				$style .= "' ";
			} ?>
			<a <?php if ($style) { echo $style; } ?>href="<?echo $arItem["DISPLAY_PROPERTIES"]["LINK"]["VALUE"]?>"><?echo $arItem["NAME"]?></a>
		</div>
	<?php } ?>
<?endforeach;?>
</div>