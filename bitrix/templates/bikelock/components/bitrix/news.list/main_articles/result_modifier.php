<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
$hashNames = array();
$itemsList = array();
$i = 0;
foreach ($arResult['ITEMS'] as $key => $arItem) {
	foreach ($arItem['PROPERTIES']['HASHTAG']['VALUE'] as $number => $arHash) {
		$res = CIBlockElement::GetByID($arHash);
		if($ar_res = $res->GetNext())
  			$arResult['ITEMS'][$key]['PROPERTIES']['HASHTAG']['VALUE'][$number] = array("NAME" => str_replace(' ', '', $ar_res['NAME']), "LINK" => $ar_res['DETAIL_PAGE_URL']);
	}
}
foreach ($arResult['ITEMS'] as $key => $arItem) {	
		if($arItem['PROPERTIES']['SHOW_ON_MAIN']['VALUE'] && $i < 3){
			$itemsList[] = $arItem;
			$i++;
		}else{
			continue;
		}
}
$arResult['ITEMS'] = $itemsList;
?>