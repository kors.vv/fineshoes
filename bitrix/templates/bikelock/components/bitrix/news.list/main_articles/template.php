<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="main_articles__area">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
	<div class="main_articles__element" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
		<div class="main_articles__image_area">
			<a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="main_articles__image_link"><img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" title="<?=$arItem['PREVIEW_PICTURE']['TITLE']?>" alt="<?=$arItem['PREVIEW_PICTURE']['ALT']?>" class="main_articles__image"></a>
			<div class="main_articles__title"><a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="main_articles__title_link"><?=$arItem['NAME']?></a></div>
		</div>
		<div class="main_articles__text_area">
			<div class="main_articles__hash_area">
				<?php foreach ($arItem['PROPERTIES']['HASHTAG']['VALUE'] as $key => $hash): ?>
					<a href="<?=$hash['LINK']?>" class="main_articles__hashtag_link"><div class="main_articles__hashtag">#<?=$hash['NAME']?></div></a>
				<?php endforeach ?>
			</div>
		</div>
	</div>
<?endforeach;?>
</div>
