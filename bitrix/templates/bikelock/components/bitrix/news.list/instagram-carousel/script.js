$(function() {
    $('#instagram').bxSlider({
        minSlides: 1,
        maxSlides: 5,
        moveSlides: 1,
        slideWidth: 220,
        slideMargin: 30,
        pager: false,
        controls: true,
        auto: true,
        pause: 7000,
        autoDelay: 2000,
        autoHover: true
    });
});
