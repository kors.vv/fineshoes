<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true); ?>
<a class="instagram-caption" href="https://www.instagram.com/fineshoes.ru/" target="_blank"></a>
<div id="instagram">
    <?foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <?$render_img = CFile::ResizeImageGet(
            $arItem['PREVIEW_PICTURE']['ID'],
            Array("width" => 220,"height" => 220),
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );?>
        <div id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <a target="_blank" href="<?=$arItem["PROPERTIES"]["ORIGINAL_URL"]["VALUE"]?>" style="display: inline-block;width: 220px;height: 220px;background-image: url(<?=$render_img['src']?><?//=$arItem["PREVIEW_PICTURE"]["SRC"]?>);background-size: cover">
            </a>
            <div class="instagram-post-footer">
                <div class="post-likes"><i class="fa fa-heart-o"></i><?=$arItem["PROPERTIES"]["LIKES"]["VALUE"]?></div>
                <div class="post-date"><?=$arItem["DISPLAY_ACTIVE_FROM"]?></div>
            </div>
        </div>
    <?endforeach ?>
</div>
