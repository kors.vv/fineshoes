<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="about-shoes">
  <?if($APPLICATION->GetCurPage() == '/about-shoes/'){?>
	<h1>Все о классической обуви</h1>
  <?}else{?>
    <h1><?=$arResult["SECTION"]["PATH"][0]["NAME"]?></h1>
  <?}?>
</div>

<?if($APPLICATION->GetCurPage() == '/about-shoes/'){?>

  <div class="ab-sh-hashtag-main">
	<div class="ab-sh-hashtag">
	  <?foreach($arResult["ITEMS"] as $arItem => $kol):?>
	    <?
		$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
		?>
	    <?if($arItem>8){?> 
		  <a href="/about-shoes<?=$kol["DETAIL_PAGE_URL"];?>">#<?=str_replace(' ', '', $kol["NAME"]);?></a>
		<?}else{?> 
		  <a href="/about-shoes<?=$kol["DETAIL_PAGE_URL"];?>">#<?=str_replace(' ', '', $kol["NAME"]);?></a>
		  <?if($arItem==8){?>
		   <?/*<span class="ab-sh-mnog">...</span>*/?>
		  <?}?>
		<?}?> 
	  <?endforeach;?>
	  
	</div>
	<a class="ab-sh-hashtag-all">Все темы</a>
  </div>
  
  <div class="article-sidebar-statii">
	<div class="sidebar-block subscribe-block">
		<div class="sidebar-block__main_title">Наши новости на вашей почте</div> 
		<div class="subscribe-form" id="subscribe-form">
			<form action="/personal/subscribe/subscr_edit.php">
				<div style="display: none;">
	   <label for="sf_RUB_ID_1"> <input type="checkbox" name="sf_RUB_ID[]" id="sf_RUB_ID_1" value="1" checked=""> Новости магазина </label><br>
					</div>
					<table cellspacing="0" cellpadding="2" align="center">
					<tbody>
					<tr>
						<td>
	   <input class="subscribe-email" type="text" placeholder="E-mail" name="sf_EMAIL" size="20" value="" title="">
						</td>
						<td align="right">
	   <input class="btn-black" type="submit" name="OK" value="Подписаться">
						</td>
					</tr>
					</tbody>
					</table>
	   <input type="hidden" name="zyear" value="2018">
				</form>
				<p class="agreement">
					 Подписываясь на рассылку, я принимаю<br>
	   <a href="/politika-konfidencialnosti/" target="_blank">пользовательское соглашение</a>
				</p>
			</div>
		</div>
  </div>

  <script>
	$(document).ready(function(){
		var a = $(".ab-sh-hashtag-main"),
			e = $(".ab-sh-hashtag"),
			h = $(".ab-sh-hashtag-all"),
			f = ["Все темы", "Скрыть"];
		e.css("height", "60px");
		a.each(function(a, g) {
			var c = $(".ab-sh-hashtag-all", g);
			c.click(function(b) {
				b.preventDefault();
				b = $(".ab-sh-hashtag", g);
				e.not(b).animate({
					height: "60px"
				}, 800, function() {
					h.not(c).text(f[0])
				});
				var d = b[0],
					a = d.clientHeight < d.scrollHeight ? d.scrollHeight + "px" : "60px";
				b.animate({
						height: a
					},
					500,
					function() {
						c.text(f[+("60px" != a)])
					})
				$('.ab-sh-mnog').toggle();
			})
		})
	});
  </script>
  
<?}?>