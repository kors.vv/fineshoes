<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$res = CIBlockSection::GetByID($arResult["IBLOCK_SECTION_ID"]);
if ($ar_res = $res->GetNext())
    $arResult['SECTION_NAME'] = $ar_res['NAME'];

if (is_array($arResult['PROPERTIES']['LINKED_ARTICLES']['VALUE'])) {
    $arFilter = array('ID' => $arResult['PROPERTIES']['LINKED_ARTICLES']['VALUE']);
    $arSelect = array('ID', 'IBLOCK_ID', 'NAME', 'PREVIEW_TEXT','PREVIEW_PICTURE', 'DETAIL_PAGE_URL');
    $res = CIblockElement::GetList(array(), $arFilter, false, false, $arSelect);

    while($ob = $res->GetNextElement()) {
        $arFields = $ob->GetFields();
        $arResult['LINKED_ARTICLES'][] = $arFields;
    }
}