<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
$APPLICATION->AddHeadString('<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">');
?>

<div class="container">
<div class="article-wrapper" itemscope itemtype="http://schema.org/Article">
    <meta itemprop="name" content="Bike-lock">
    <?if ($arResult['DETAIL_PICTURE']['SRC']):?>
        <div class="header-image" style="background: url(<?=$arResult['DETAIL_PICTURE']['SRC']?>) center center no-repeat;">
            <div class="header-image-text">
                <h1 itemprop="headline"><?=$arResult['NAME']?></h1>
                <a href="<?=$arResult['LIST_PAGE_URL']?>" class="section-name">#<?=$arResult['SECTION_NAME']?></a>
            </div>
            <img class="header-image-logo" src="/images/logo_white.png" alt="">
            <div class="header-image-overlay"></div>
        </div>
    <?else:?>
        <h1 itemprop="headline" ><?=$arResult['NAME']?></h1>
    <?endif;?>
    <div class="articles_list clearfix">
        <?php if (!empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT'])) { ?>
        <div class="articles_item_in">
            <div class="text-block" itemprop="description">
                <?php if (!empty($arResult['PREVIEW_TEXT'])) { ?>
                    <p><?=$arResult['PREVIEW_TEXT']?></p>
                <?php } ?>

                <?if(strpos($arResult['DETAIL_TEXT'], '#SLIDER#') && is_array($arResult['PROPERTIES']['SLIDER_PICTURES']['VALUE'])):
                    $text = explode('#SLIDER#', $arResult['DETAIL_TEXT']);?>
                    <?=$text[0]?>
                    <div class="bxslider-article">
                        <?foreach($arResult['PROPERTIES']['SLIDER_PICTURES']['VALUE'] as $file):
                            $arFileTmp = CFile::ResizeImageGet($file, array("width" => 500, "height" => 500), BX_RESIZE_IMAGE_PROPORTIONAL);
                            ?>
                            <div class="slide"><img src="<?=$arFileTmp['src']?>"></div>
                        <?endforeach;?>
                    </div>
                    <script>
                        $(function(){
                            $('.bxslider-article').bxSlider({
                                mode: 'horizontal',
                                responsive: true,
                                adaptiveHeight: true,
                                slideWidth: 500
                            });
                        });
                    </script>
                    <?=$text[1]?>
                <?else:?>
                    <?=$arResult['DETAIL_TEXT']?>
                <?endif;?>

            </div>
        </div>
    </div>
</div>
<div class="article-sidebar">
    <?if(is_array($arResult['LINKED_ARTICLES'])):?>
        <div class="sidebar-block read-also">
            <h2>Читайте также</h2>
            <?foreach($arResult['LINKED_ARTICLES'] as $article):
                $arFileTmp = CFile::ResizeImageGet($article['PREVIEW_PICTURE'], array('width' => 100, 'height' => 100), BX_RESIZE_IMAGE_EXACT);?>
                <div class="linked-article">
                    <div class="linked-article-img">
                        <a href="<?=$article['DETAIL_PAGE_URL']?>"><img src="<?=$arFileTmp['src']?>" alt=""></a>
                    </div>
                    <div class="linked-article-content">
                        <a href="<?=$article['DETAIL_PAGE_URL']?>" class="linked-article-heading"><?=$article['NAME']?></a>
                        <div class="linked-article-text"><?=strip_tags($article['PREVIEW_TEXT'])?></div>
                    </div>
                </div>
            <?endforeach;?>
            <div class="text-center">
                <a href="/about-shoes/" class="btn-black">Еще статьи</a>
            </div>
        </div>
    <?endif;?>
    <div class="sidebar-block subscribe-block">
        <h2>Наши новости на вашей почте</h2>
        <?$APPLICATION->IncludeComponent(
            "bitrix:subscribe.form",
            "subscribe",
            Array(
                "CACHE_TIME" => "3600",
                "CACHE_TYPE" => "A",
                "PAGE" => "#SITE_DIR#personal/subscribe/subscr_edit.php",
                "SHOW_HIDDEN" => "N",
                "USE_PERSONALIZATION" => "Y"
            ),
            $component
        );?>
    </div>
</div>
	<?php } ?>
  <?/*
  <div class="cont_padd">
    <?$APPLICATION->IncludeComponent(
  	"bitrix:news.list",
  	"other_news",
  	Array(
  		"ACTIVE_DATE_FORMAT" => "d.m.Y",
  		"ADD_SECTIONS_CHAIN" => "Y",
  		"AJAX_MODE" => "N",
  		"AJAX_OPTION_ADDITIONAL" => "",
  		"AJAX_OPTION_HISTORY" => "N",
  		"AJAX_OPTION_JUMP" => "N",
  		"AJAX_OPTION_STYLE" => "Y",
  		"CACHE_FILTER" => "N",
  		"CACHE_GROUPS" => "Y",
  		"CACHE_TIME" => "36000000",
  		"CACHE_TYPE" => "A",
  		"CHECK_DATES" => "Y",
  		"COMPONENT_TEMPLATE" => ".default",
  		"DETAIL_URL" => "",
  		"DISPLAY_BOTTOM_PAGER" => "Y",
  		"DISPLAY_DATE" => "Y",
  		"DISPLAY_NAME" => "Y",
  		"DISPLAY_PICTURE" => "Y",
  		"DISPLAY_PREVIEW_TEXT" => "Y",
  		"DISPLAY_TOP_PAGER" => "N",
  		"FIELD_CODE" => array(0=>"",1=>"",),
  		"FILTER_NAME" => "",
  		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
  		"IBLOCK_ID" => "1",
  		"IBLOCK_TYPE" => "news",
  		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
  		"INCLUDE_SUBSECTIONS" => "Y",
  		"MESSAGE_404" => "",
  		"NEWS_COUNT" => "3",
  		"PAGER_BASE_LINK_ENABLE" => "N",
  		"PAGER_DESC_NUMBERING" => "N",
  		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
  		"PAGER_SHOW_ALL" => "N",
  		"PAGER_SHOW_ALWAYS" => "N",
  		"PAGER_TEMPLATE" => ".default",
  		"PAGER_TITLE" => "Новости",
  		"PARENT_SECTION" => "",
  		"PARENT_SECTION_CODE" => "",
  		"PREVIEW_TRUNCATE_LEN" => "",
  		"PROPERTY_CODE" => array(0=>"",1=>"",),
  		"SET_BROWSER_TITLE" => "Y",
  		"SET_LAST_MODIFIED" => "N",
  		"SET_META_DESCRIPTION" => "Y",
  		"SET_META_KEYWORDS" => "Y",
  		"SET_STATUS_404" => "N",
  		"SET_TITLE" => "Y",
  		"SHOW_404" => "N",
  		"SORT_BY1" => "RAND",
  		"SORT_BY2" => "SORT",
  		"SORT_ORDER1" => "DESC",
  		"SORT_ORDER2" => "ASC"
  	)
  );?>
  </div>
*/?>
</div>
