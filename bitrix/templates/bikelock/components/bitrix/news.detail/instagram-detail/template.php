<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<h1>Инстаграм - <?=$arResult["DISPLAY_ACTIVE_FROM"]?></h1>

<div class="instagram-post-detail">
    <div class="instagram-post-detail__photo">
        <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="">
        <div class="post-likes"><i class="fa fa-heart-o"></i><?=$arResult["PROPERTIES"]["LIKES"]["VALUE"]?> отметок &laquo;Нравится&raquo;</div>
    </div>
    <div class="instagram-post-detail__text">
        <div class="instagram-post-detail__subscribe">
            <strong>fineshoes.ru</strong>
            <a href="https://www.instagram.com/fineshoes.ru/">Подписаться</a>
        </div>

        <div class="instagram-post-detail__text">
            <strong>fineshoes.ru</strong> <?=$arResult["DETAIL_TEXT"]?>
            <?$tags = explode(" ", $arResult["PROPERTIES"]["TAGS"]["VALUE"]);
            foreach ($tags as $tag): ?>
                <a target="_blank" href="https://www.instagram.com/explore/tags/<?=$tag?>/">#<?=$tag?></a>
            <?endforeach;?>
            <?$comments = unserialize($arResult["PROPERTIES"]["COMMENTS"]["~VALUE"]);
            if($comments):
                foreach($comments as $comment): ?>
                <div class="instagram-post-detail__comment">
                    <strong><?=$comment->node->owner->username?></strong>
                    <?=$comment->node->text?>
                </div>
            <?endforeach;
            endif;?>

        </div>
    </div>
</div>