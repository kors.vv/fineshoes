<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $templateData */
/** @var @global CMain $APPLICATION */
use Bitrix\Main\Loader;
global $APPLICATION;
if (isset($templateData['TEMPLATE_THEME']))
{
	$APPLICATION->SetAdditionalCSS($templateData['TEMPLATE_THEME']);
}
if (isset($templateData['TEMPLATE_LIBRARY']) && !empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;
	if (!empty($templateData['CURRENCIES']))
		$loadCurrency = Loader::includeModule('currency');
	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
	?>
	<script type="text/javascript">
		BX.Currency.setCurrencies(<? echo $templateData['CURRENCIES']; ?>);
	</script>
<?
	}
}

$desc = explode('<!--BOTTOM-->', $arResult['DESCRIPTION']);

$APPLICATION->SetPageProperty("CATEGORY_TOP", $desc[0]);
if(!empty($desc[1])) {
	$APPLICATION->SetPageProperty("CATEGORY_BOTTOM", $desc[1]);
}
$arSection = end($arResult['PATH']);
if ($imgId = $arSection["PICTURE"]) {
    $APPLICATION->SetPageProperty("CATEGORY_TOP_IMG", '<img src="'.CFile::GetPath($imgId).'" alt="'.$arSection["NAME"].'" />');
}
?>