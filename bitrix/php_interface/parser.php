<?
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity; 
CModule::IncludeModule("highloadblock"); 

if(!empty($_GET['setdescsect'])) {

	CModule::IncludeModule("iblock");
	$section = CIBlockSection::GetList(
		array(),
		array('=IBLOCK_ID' => 6) ,
		false,
		array('*'),
		array('nTopCount' => 999)
	);
		
	while($item = $section->Fetch()) {
		
		if(empty($item['DESCRIPTION']))
			continue;
		
		if(preg_match_all('/<a href="(.*?)">.*?<\/a>/is', $item['DESCRIPTION'], $maths)) {
			
			
			foreach($maths[1] as $k => $u) {
				
				$link = $maths[0][$k];
				$linkN = $maths[0][$k];
				
				if(substr($u, -1) !== '/')
					$linkN = str_replace($u, $u . '/', $link);
				
				
					
				$item['DESCRIPTION'] = str_replace($link, $linkN, $item['DESCRIPTION']);
				
				
			}
			
			
			$bs = new CIBlockSection;
			$bs->Update($item['ID'], array('DESCRIPTION' => $item['DESCRIPTION']));
			//print_r($item);
			
		}
		
	}
	
}

if(!empty($_GET['setw']) && false) {

	CModule::IncludeModule("iblock");
	
	$elems = CIBlockElement::GetList(
		array(),
		array('=IBLOCK_ID' => 6) ,
		false,
		false,
		array('*')
	);
	
		
	while($item = $elems->GetNextElement()) {
		
		$f = $item->GetFields();
		
		if(empty($f['DETAIL_TEXT']))
			continue;
		
		$w = '';
		
		if(preg_match('/Вес:(.*).?гр/iu', $f['DETAIL_TEXT'], $m)) 
			$w = strip_tags(trim(str_replace('&nbsp;', '', $m[1])));
			
		if(preg_match('/Вес:(.*).?кг/iu', $f['DETAIL_TEXT'], $m))
			$w = str_replace(',', '.', trim($m[1])) * 1000;
	
	
		if($w) {
			
			print($w . ';http://bikelock.ru' . $f['DETAIL_PAGE_URL'] . "\n");
			
			/*$add = new CIBlockElement;
			$add->Update($item['ID'], array(
				'PROPERTY_WEIGHT_VALUE' => $w,
			));
			echo $item['ID'] . '=' . $w;
			echo $add->LAST_ERROR;
			exit;
			*/
		}
		
	}
	exit;
	
}

if(!empty($_GET['setmainsect']) && false) {
	

	CModule::IncludeModule("iblock");
	
	$arr = CIBlockElement::GetList(
		Array("ID"=>"ASC"),
		array('IBLOCK_ID' => 6),
		false,
		array('nTopCount' => 990),
		array()
	);

	$i = 0;
	while($item = $arr->GetNextElement()) {
		
		
		$elem = $item->GetFields();
		
		$db_old_groups = CIBlockElement::GetElementGroups($elem['ID'], true);
		$section = array();
		$sections = array();
		
		while($ar_group = $db_old_groups->Fetch()) {
			
			if(!empty($ar_group['IBLOCK_SECTION_ID']))
				$section = $ar_group;
			
			$sections[] = $ar_group["ID"];
			
		}
		
		if(!empty($section)) {
			
			
		$add = new CIBlockElement;
			$add->Update($elem['ID'], array(
				'IBLOCK_SECTION_ID' => $section['ID'],
				'IBLOCK_SECTION' => $sections
			));
		
		}
		
	}
	
	print(22344);
	exit;
	
	
}


if(!empty($_GET['setbrandsection']) && false) {
	
	CModule::IncludeModule("iblock");
	
	$arr = CIBlockElement::GetList(
		Array("ID"=>"ASC"),
		array('IBLOCK_ID' => 6),
		false,
		array('nTopCount' => 990),
		array('IBLOCK_SECTION_ID ','IBLOCK_ID','ID','NAME')
	);

	while($item = $arr->GetNextElement()) {
		
		$elem = $item->GetFields();
		$props = $item->GetProperties();
		
		if(empty($props['BRAND']['VALUE']))
			continue;
		
		$db_old_groups = CIBlockElement::GetElementGroups($elem['ID'], true);
		$sections = array();
		
		while($ar_group = $db_old_groups->Fetch()) {
			
			$sections[] = $ar_group["ID"];

		}
		
		$arFilter = array('=NAME' => $name, '=IBLOCK_ID' => 6);
		
		$brand = $props['BRAND']['VALUE'];
		
		$section = CIBlockSection::GetList(
			array(),
			array('NAME' => $brand, '=IBLOCK_ID' => 6, 'SECTION_ID' => false) ,
			false,
			array('nTopCount' => 999),
			array('ID')
		);
		
		if($item = $section->Fetch()) 
			$brandID = $item['ID'];
		else {
			
				
	
				$addSection = array(
				'ACTIVE' => 'Y',
					'IBLOCK_ID' => 6,
					'NAME' => $brand,
					'CODE' => $brand ,
					'UF_HIDE' => 'да'
				);
				
				$bs = new CIBlockSection;
				$brandID = $bs->Add($addSection);
		
			
		}
		$sections[] = $brandID;
		
		$add = new CIBlockElement;
		$add->Update($elem['ID'], array(
			'IBLOCK_SECTION' => $sections
		));
		
		
	}

	echo 11;
	exit;
}

//if(!empty($_GET['parsers']))
//	zenStartParser();

function zenStartParser()	{
	mail('info@zenagency.ru', 'start bike', 'start');
	
	ob_start();
	
	$links = array();
	
	$urlBase = 'http://bikelock-old.inteldev.ru/';
	
	$url = 'http://bikelock-old.inteldev.ru/ajax/catalog?page=';
	
	for($i = 1 ; $i < 200; $i++) {
		
		$cc = file_get_contents($url . $i);

		if(!preg_match_all('/<div class="img" style="background-image:url\(\'(.*?)\'\)".*?<a href="(.*?)">(.*?)<\/a>/is', $cc, $matchs))
			break;
		
		foreach($matchs[2] as $k => $y) 
			$links[$y] = array('img' => $urlBase . $matchs[1][$k], 'name' => $matchs[3][$k]);
			
	}
	
	$data = array();
	
	$colors = array(
		1 => 'white',
		2 => 'black',
		3 => 'orange',
		4 => 'yellow',
		5 => 'green',
		6 => 'red ',
		7 => 'biruse',
		8 => 'grey',
		9 => 'purple',
		10 => 'pink',
		11 => 'blue'
	);
	
	$brands = array(
		1 => 'Kryptonite',
		2 => 'Abus',
		3 => 'OnGuard',
		4 => 'Trelock',
		5 => 'Knog',
		6 => 'Xena',
		7 => 'Magnum',
		8 => 'Oxford',
		9 => 'Litelok',
		10 => 'Hiplok',
		11 => 'BBB',
		12 => 'Master lock',
		13 => 'Squire',
	);
	
	$counter = 0;
	
	foreach($links as $u => $d) {
		
		$counter++;
		
		$product = file_get_contents($urlBase . $u);
		$c = explode('/', $u);
		$item = $d;
		$item['CODE'] = $c[count($c) - 1];
		$item['URL_S'] = $urlBase . $u;
		
		$item['gallery'] = array();
		
		if(preg_match('/class="breadcrumbs">(.*?)<\/div/is', $product, $match)) {
			
			if(preg_match_all('/<a href="(.*?)">(.*?)<\/a>/is', $match[1], $match)) {
				
				$item['brand'] = $match[2][3];
				$item['category'] = array($match[2][2]);
				
			}
			
		}
		
		if(preg_match('/a class="fancy gallery" href="(.*?)" rel="catalog-images"/i', $product, $match))
			$item['imgFull'] = $urlBase . $match[1];
		
		if(preg_match('/imgs_item.*?<\/div>/is', $product, $match)) {
			
			if(preg_match_all('/<a class="fancy gallery" href="(.*?)" rel="catalog-images"><img src="(.*?)"/is', $match[0], $match)) {
				
			foreach($match[2] as $mk => $m) {
				
					$item['gallery'][] = array(
						'img' => $urlBase . $m,
						'imgFull' => $urlBase . $match[1][$mk]
					);
				
				}
				
			}
			
		}
		
		
		if(preg_match('/<h1 class="noborder">(.*?)<\/h1>/i', $product, $match))
			$item['nameFull'] = $match[1];
		
		if(preg_match('/<div class="price">(.*?)<\/div>/is', $product, $match)) 
			$item['price'] = intval(strip_tags(trim($match[1])));
		
		if(preg_match('/<span class="oldprice">(.*?)<\/span>/is', $product, $match)) 
			$item['price_before'] = intval(strip_tags(trim($match[1])));
		
		if(preg_match('/label hit/is', $product, $match)) 
			$item['hit'] = 46;
		
		if(preg_match('/label new/is', $product, $match)) 
			$item['new'] = 47;

		if(preg_match('/<div class="security sec_(\d+)">.*?<\/div>/i', $product, $match)) 
			$item['reliability'] = getZenRating(intval($match[1]));
		
		if(preg_match_all('/<div class="tabs_content.*?clearfix">(.*?)<\/div>/is', $product, $match)) {
			
			if(!empty($match[1][0]))
				$item['description'] =  trim($match[1][0]);
			
			if(!empty($match[1][1]) && strpos($product, '<span>Технические характеристики</span>') !== false)
				$item['tex'] =  trim($match[1][1]);
			
			
		}
		
		if(preg_match('/<span class="dd-color" style="display: none">(.*?)<\/span>/i', $product, $match)) {
			
			if(!empty($match[1])) {
				
				$c = explode(',', $match[1]);
				$item['COLOR'] = array();
				
				foreach ($c as $icolor) {
					
					if(!empty($colors[$icolor]))
						$item['COLOR'][] = $colors[$icolor];
				
				}
				
			}
			
		}
		
		if(preg_match('/<span class="dd-brand" style="display: none">(.*?)<\/span>/i', $product, $match)) {
			
			if(!empty($brands[$match[1]]))
				$item['brand'] = $brands[$match[1]];
			
		}
	
		
		if(preg_match('/<span class="dd-weight" style="display: none">(.*?)<\/span>/i', $product, $match)) 
			$item['WEIGHT'] = $match[1];
		
		if(preg_match('/<title>(.*?)<\/title>/i', $product, $match)) 
			$item['title'] = $match[1];
		
		if(preg_match('/<meta name="description" content="(.*?)"/ius', $product, $match)) 
			$item['mdescription'] = trim($match[1]);
		
		if(preg_match('/<meta name="keywords" content="(.*?)"/i', $product, $match)) 
			$item['keywords'] = $match[1];
			
		if(preg_match_all('/catalog_item_list.*?<div class="ttl"><a href=".*?">(.*?)<\/a/is', $product, $match)) {
			
			$item['RECOMENDED'] = array();
			
			foreach($match[1] as $mm) 
				$item['RECOMENDED'][] = $mm;
			
		}

		if(empty($data[$item['CODE']]))
			$data[$item['CODE']] = $item;
		else {
			
			$xx = 1;
			
			while($xx < 20) {
				
				if(empty($data[$item['CODE'] . '-' . $xx]))
					break;
				
				$xx++;
				
				
			}
			
			$item['TAKE'] = 1;
			
			$data[$item['CODE'] . '-' . $xx] = $item;
			
		}
		
		//if($counter % 100 == 0) 
			//mail('info@zenagency.ru', 'bikelock парсинг продуктов', 'Продукт: ' . $counter);
			
	}

	/* Добавляем для каждого товара все категории
	$temp  = $data;
	
	foreach($data as $k => $i) {
		
		$cats = array();
		
		if(!empty($i['TAKE']))
			continue;
		
		foreach($temp as $kk => $ii) {
			
			if($i['CODE'] == $ii['CODE'])
				$cats[] = $ii['category'][0];
			
		}
		
		foreach($temp as $kk => $ii) {
			
			if($i['CODE'] == $ii['CODE'])
				$data[$kk]['category'] = $cats;
			
		}
		
	}
	
	unset($temp);*/
	
	unset($links);
	
	//Добавление
	$data = array_reverse($data, true);
	
	CModule::IncludeModule("catalog");
	CModule::IncludeModule("iblock");
	
	$synxList = array();
	$prodAdd = 0;
	
	mail('info@zenagency.ru', 'bikelock добавлние продуктов', 'Старт');
	
	print_r($data);
	//exit;
	
	foreach($data as $code => $product) {
		
		if(empty($product['price']))
			continue;
			
		$add = new CIBlockElement();
		
		$category = array();
		$categoryType = array();
		if(!empty($product['category'])) {
			
			foreach($product['category'] as $cc) {
				
				$category[] = getZenCategory($cc);
				$categoryType[] = getZenCatType($cc);
			
			}
		}
		
		$brand = 0;
		if(!empty($product['brand'])) {
			
			$brand = getZenBrand($product['brand']);
		
			//Работаем с брендами
			foreach($product['category'] as $cc)
				$category[] = getZenCategory($product['brand'], $cc);
			
			if(empty($product['TAKE']))
				$category[] = getZenCategory($product['brand']);
				
		}
		
		if(!empty($product['COLOR']) && count($product['COLOR']) == 1)
			$product['COLOR'] = $product['COLOR'][0];
		
		$props = array(
			'CATEGORY' => $categoryType,
			'BRAND' => $brand,
			'RATING' => $product['reliability'],
			'WEIGHT' => $product['WEIGHT'],
			'TITLE' => $product['nameFull'],
			'PHOTOS' => array(),
			'PHOTOS_SMALL' => array(),
		);
		
		if(!empty($product['COLOR']))
			$props['COLOR'] = $product['COLOR'];
		
		$arParams = array("replace_space"=>"-","replace_other"=>"-");
		//'CODE' => Cutil::translit($product['NAME'], "ru", $arParams),
		$fields = array(
			'IBLOCK_ID' => 6,
			'NAME' => $product['name'],
			'CODE' => $code,
			'PREVIEW_TEXT' => $product['description'],
			'PREVIEW_TEXT_TYPE' => 'html',
			'DETAIL_TEXT' => $product['tex'],
			'DETAIL_TEXT_TYPE' => 'html',
			"IPROPERTY_TEMPLATES" => array(
				"ELEMENT_META_TITLE" => $product['title'], 
				"ELEMENT_META_KEYWORDS" => $product['keywords'], 
				"ELEMENT_META_DESCRIPTION" => $product['mdescription'] 
			)
		);
		
		$fields['IBLOCK_SECTION_ID'] = $category[0];
		if(count($category) > 1)
			$fields['IBLOCK_SECTION'] = $category;
		
		if(!empty($product['imgFull']))
			$fields['DETAIL_PICTURE'] = getFileArray($product['imgFull']);
			
		if(!empty($product['img']))
			$fields['PREVIEW_PICTURE'] = getFileArray($product['img']);
			
		if(!empty($product['hit']))
			$props['HIT'] = 46;
			
		if(!empty($product['new']))
			$props['NEW'] = 47;
		
		if(!empty($product['price_before']))
			$props['PRICE_BEFORE'] = $product['price_before'];
			
		if(empty($product['TAKE']))
			$props['NOT_MAIN'] = 50;
		else
			$props['NOT_MAIN'] = 51;
		
		if(!empty($product['gallery'])) {
			
			foreach($product['gallery'] as $item) {
				
				$props['PHOTOS'][] = getFileArray($item['imgFull']);
				$props['PHOTOS_SMALL'][] = getFileArray($item['img']);
				
			}
			
		}
		
		$fields['PROPERTY_VALUES'] = $props;
		
		if($id = $add->Add($fields,false, false, true)) {
			
			unset($props['PHOTOS']);
			unset($props['PHOTOS_SMALL']);
			
			$synxList[$id] = array('name' => $product['name'], 'props' => $props);
			//Товар
				$fieldsCat = array(
					'ID' => $id,
					'MEASURE' => 5,
					'CAN_BUY_ZERO' => 'Y',
				);
				
				CCatalogProduct::Add($fieldsCat);
				
				$arFields = Array(
					"PRODUCT_ID" => $id,
					"CATALOG_GROUP_ID" => 1,
					"PRICE" => $product['price'],
					"CURRENCY" => "RUB",
					"QUANTITY_FROM" => false,
					"QUANTITY_TO" => false,
			);

			CPrice::Add($arFields);
			$prodAdd++;
		}
		else
			print_r($add->LAST_ERROR . 'code: ' . $code);
		
	}
	
	echo 'Добавлено продуктов: ' . $prodAdd;
	print_r($synxList);
	
	foreach($data as $product) {
		
		if(empty($product['RECOMENDED']))
			continue;
		
		$dd = getZenProductFromName($synxList, $product['name']);
		
		$id = $dd['id'];
		$n = $dd['props'];
		
		$n['RECOMENDED'] = array();
		
		foreach($product['RECOMENDED'] as $item) {
			
			$dd = getZenProductFromName($synxList, $item);
			if(!empty($dd['id']))
				$n['RECOMENDED'][] = $dd['id'];
			
		}
		print_r('==' . $id . '==');
		print_r($n);
		if(!empty($id) && !empty($n['RECOMENDED'])) {
			
		$add->Update($id, array(
			'PROPERTY_VALUES' => $n
		));
		
			print_r($add->LAST_ERROR );
			
		}
		
	}
	
	$cc = ob_get_contents();
	
	ob_end_clean();
	
	mail('info@zenagency.ru', 'Конец bikelock', $cc);
	
exit;	
	
}