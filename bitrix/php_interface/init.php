<?
use Bitrix\Highloadblock as HL; 
use Bitrix\Main\Entity;

AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
function BeforeIndexHandler($arFields) {
	$arrIblock = array(6,23);
	$arDelFields = array("DETAIL_TEXT", "PREVIEW_TEXT") ;
	if (CModule::IncludeModule('iblock') && $arFields["MODULE_ID"] == 'iblock' && in_array($arFields["PARAM2"], $arrIblock) && intval($arFields["ITEM_ID"]) > 0){
	$dbElement = CIblockElement::GetByID($arFields["ITEM_ID"]) ;
	if ($arElement = $dbElement->Fetch()){
		foreach ($arDelFields as $value){
		if (isset ($arElement[$value]) && strlen($arElement[$value]) > 0){
			$arFields["BODY"] = str_replace (CSearch::KillTags($arElement[$value]) , "", CSearch::KillTags($arFields["BODY"]) );
			}
		}
	}
	return $arFields;
	}
}

// if (!$_SERVER["HTTPS"]) {
// 	$redirect = 'https://fineshoes.ru'.$_SERVER["REQUEST_URI"];
// 	LocalRedirect($redirect, false, "301 Moved Permanently");
// }

CModule::IncludeModule("highloadblock"); 

session_start();

include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/php_interface/parser.php');

function getZenProductFromName($list, $name){
	
	$id = 0;
	
	foreach($list as $k => $n) {
		
		if( mb_strtolower($n['name']) == mb_strtolower($name)) {
			
			$id = array('id' => $k, 'props' => $n['props']);
			break;
			
		}
		
	}
	
	return $id;
	
}

function getFileArray($file) {
		
	$dir = $_SERVER['DOCUMENT_ROOT'] . '/tmp2/';
	if(!is_dir($dir))
		mkdir($dir);
		
	$f = file_get_contents($file);
		
	list($nn, $nbase) = explode('.', basename($file));
	$newname = mt_rand() . time() . '.' . $nbase;
		
	if(file_put_contents($dir . $newname, $f)) {
			
		$mime = getimagesize($dir . $newname);
			//mime_content_type($dir . $newname)
			
		$file = array(
			'name' => $newname,
			'tmp_name' => $dir . $newname,
			'type' => $mime['mime'],
			'size' => filesize($dir . $newname),
			'error' => 0
		);
			
	}
		
	return $file;
		

}


function getZenCategory($name, $parent) {
		
		$category = 0;
		$arFilter = array('=NAME' => $name, '=IBLOCK_ID' => 6, '=SECTION_ID' => 0);
		
		
		CModule::IncludeModule("iblock");
	
		if(!empty($parent)) {

			$ss = CIBlockSection::GetList(
				array(),
				array('=NAME' => $parent, '=IBLOCK_ID' => 6),
				false,
				array('nTopCount' => 999),
				array('ID')
			);
			
			if($sp = $ss->Fetch()) 
				$arFilter['=SECTION_ID'] = $sp['ID'];
			
		}
		
		$section = CIBlockSection::GetList(
			array(),
			$arFilter,
			false,
			array('nTopCount' => 999),
			array('ID')
		);
		
		if($item = $section->Fetch()) 
			$category = $item['ID'];
		else {
			
			if(!empty($arFilter['=SECTION_ID'])) {
				
				$fc = mb_strtoupper(mb_substr($name, 0, 1));
     
	
				$addSection = array(
				'ACTIVE' => 'Y',
					'IBLOCK_ID' => 6,
					'NAME' => $name,
					'CODE' => $name ,
					'IBLOCK_SECTION_ID' => $arFilter['=SECTION_ID']
				);
				
				$bs = new CIBlockSection;
				$category = $bs->Add($addSection);
		
			}
			
			
		}
		return $category;
		
}


function getZenCatType($name) {
		
		$id = 0;
		$name = trim($name);
		
		$synxList = array(
			array('name' => 'U-образные замки', 'id' => 17),
			array('name' => 'Аксессуары', 'id' => 18),
			array('name' => 'Антивандальные велозамки', 'id' => 19),
			array('name' => 'Для мотоцикла', 'id' => 20),
			array('name' => 'Замки на дисковый тормоз', 'id' => 21),
			array('name' => 'Замки на раму', 'id' => 22),
			array('name' => 'Замки с сигнализацией', 'id' => 23),
			array('name' => 'Замки-цепи для колясок', 'id' => 24),
			array('name' => 'Кабели', 'id' => 25),
			array('name' => 'Кодовые замки', 'id' => 26),
			array('name' => 'Крепления для замков', 'id' => 27),
			array('name' => 'Крепления к полу', 'id' => 28),
			array('name' => 'Навесные замки', 'id' => 29),
			array('name' => 'Складные замки', 'id' => 30),
			array('name' => 'Тросовые замки', 'id' => 31),
			array('name' => 'Цепные замки', 'id' => 32),
		);
		
		
		
		foreach($synxList as $item) {
			
			if(mb_strtolower($item['name'], 'UTF-8') == mb_strtolower($name, 'UTF-8'))
				$id = $item['id'];
			
		}
			
		return $id;
		
	}
	
	
function getZenBrand($name) {
		
	$id = '';
		
			
			$hlblock = HL\HighloadBlockTable::getById(2)->fetch(); 
			$entity = HL\HighloadBlockTable::compileEntity($hlblock); 
			$entity_data_class = $entity->getDataClass(); 
			
			$rsData = $entity_data_class::getList(array(
				"select" => array("*"),
				"order" => array("ID" => "ASC"),
				"filter" => array('=UF_NAME' => $name)
			));
					
			if($rsData && $brand = $rsData->Fetch()) 
				$id = $brand['UF_XML_ID'];
			
			
		
		
		return $id;
		
}


function getZenRating($id) {
	
	$i = 0;
	
	$list = array(
		1 => 33,
		2 => 34,
		3 => 35,
		4 => 36,
		5 => 37,
		6 => 38,
		7 => 39,
		8 => 40,
		9 => 41,
		10 => 42,
	);
	
	if(!empty($list[$id]))
		$i = $list[$id];
	
	return $i;
	
}




$sorting = 'sort';
$sortingBy = 'DESC';

if(!empty($_GET['sort'])) {
	
	if($_GET['sort'] == 'populatiryup') {
		
		$_SESSION['sorting'] = 'SORT';
		$_SESSION['sortingBy'] = 'ASC';
		$_SESSION['sorting2'] = 'ID';
		$_SESSION['sortingBy2'] = 'DESC';
		
	}
	else if($_GET['sort'] == 'populatirydown') {
		
		$_SESSION['sorting'] = 'SORT';
		$_SESSION['sortingBy'] = 'DESC';
		$_SESSION['sorting2'] = 'ID';
		$_SESSION['sortingBy2'] = 'ASC';
		
	}
	else if($_GET['sort'] == 'priceup') {
		
		$_SESSION['sorting'] = 'CATALOG_PRICE_1';
		$_SESSION['sortingBy'] = 'ASC';
		
	}
	else if($_GET['sort'] == 'pricedown') {
		
		$_SESSION['sorting'] = 'CATALOG_PRICE_1';
		$_SESSION['sortingBy'] = 'DESC';
		
	}
	else if($_GET['sort'] == 'nameup') {
		
		$_SESSION['sorting'] = 'NAME';
		$_SESSION['sortingBy'] = 'ASC';
		
	}
	else if($_GET['sort'] == 'namedown') {
		
		$_SESSION['sorting'] = 'NAME';
		$_SESSION['sortingBy'] = 'DESC';
		
	}
	
}

$sorting2 = 'shows';
$sortingBy2 = 'asc';

if(!empty($_SESSION['sorting'])) {
$sorting = $_SESSION['sorting'];
$sortingBy = $_SESSION['sortingBy'];
}
if(!empty($_SESSION['sorting2'])) 
{$sorting2 = $_SESSION['sorting2'];
$sortingBy2 = $_SESSION['sortingBy2'];
}
$typeList = 'list';
if(!empty($_GET['typelist']))
	$_SESSION['typelist'] = $_GET['typelist'];

if(!empty($_SESSION['typelist']))
	$typeList = $_SESSION['typelist'];


function hasFavorite($id) {

	$favorites = array();
	if(!empty($_COOKIE['favorites']))
		$favorites = unserialize($_COOKIE['favorites']);
	
	$result = false;
	if(in_array($id, $favorites) !== false)
		$result = true;
	
	return $result;
		
}

function zenSetFavorite($fav) {
	

	setcookie('favorites', serialize($fav), time() + 3600000, '/');
	$_COOKIE['favorites'] = serialize($fav);
	
}

function zenAddFavorite($id) {
	
	$favorites = array();
	if(!empty($_COOKIE['favorites']))
		$favorites = unserialize($_COOKIE['favorites']);
	
	if(in_array($id, $favorites) === false)
		$favorites[] = $id;
	
	zenSetFavorite($favorites);
	
}

function zenDelFavorite($id) {
	
	$favorites = array();
	if(!empty($_COOKIE['favorites']))
		$favorites = unserialize($_COOKIE['favorites']);
	
	if(empty($favorites))
		return;
	
	foreach($favorites as $k => $i) {
		
		if($i == $id) {
			
			unset($favorites[$k]);
			break;
			
		}
		
	}
	
	zenSetFavorite($favorites);
	
}

function getCountFavorite(){
	
	$favorites = array();
	if(!empty($_COOKIE['favorites']))
		$favorites = unserialize($_COOKIE['favorites']);
	
	return count($favorites);
	
}

function getItemsFavoriteInfo(){
	$favorites = array();
	if(!empty($_COOKIE['favorites'])) {
		$favorites = unserialize($_COOKIE['favorites']);
	}
	$arrya_items = array();
	if(!empty($favorites)) {
		$arSelect = Array("ID", "NAME","DETAIL_PAGE_URL");
		$arFilter = Array("ACTIVE"=>"Y", "ID"=>$favorites);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement()) {
		 	$arrya_items[] = $ob->GetFields();
		}
	}
	return $arrya_items;
}

function getItemsFavorite(){
	
	$favorites = array();
	if(!empty($_COOKIE['favorites']))
		$favorites = unserialize($_COOKIE['favorites']);
	
	$out = 0;
	if(!empty($favorites)) 
		$out = '[' . implode(',', $favorites) . ']';
		
	return $out;
	
}

function getItemsFavoriteArray(){
	
	$favorites = array();
	if(!empty($_COOKIE['favorites']))
		$favorites = unserialize($_COOKIE['favorites']);
		
	return $favorites;
	
}

if(!empty($_GET['addfavorite'])) {
	
	$result = array();
	
	if(getCountFavorite() >= 5)
		$result = array('res' => 'MAXCOUNT');
	else {
		
		zenAddFavorite($_GET['addfavorite']);
		$result = array('res' => 'OK', 'num' => getCountFavorite());
	
	}
	
	echo json_encode($result);
	
	exit;
	
}

if(!empty($_GET['delfavorite'])) {
	
	zenDelFavorite($_GET['delfavorite']);

	$result = array('res' => 'OK', 'num' => getCountFavorite());
	
	echo json_encode($result);
	exit;
	
}

if(isset($_GET['action']) && $_GET['action'] =='clearcompare') {
	
	setcookie('favorites', serialize(array()), time() - 3600000, '/');

	
}

$zyear = date('Y');

if(!empty($_POST['action']) && !empty($_POST['zyear']) && $_POST['zyear'] == $zyear) {
	
	
	
	if($_POST['action'] == 'order1click') {
		
		$result = array('err' => "");
		if(empty($_POST['name']))
			$result['err'] = '<p>Необходимо заполнить поле «Ваше имя»</p>';
		
		if(empty($_POST['phone']))
			$result['err'] .= '<p>Необходимо заполнить поле «Телефон»</p>';
		
		if(!empty($_POST['name']) && !empty($_POST['phone']) && !empty($_POST['item'])) {
			
			$data = array(
				'NAME' => $_POST['name'],
				'PHONE' => $_POST['phone'],
				'PRODUCT' => $_POST['item'],
			);
			
			CEvent::SendImmediate(
				'ONEPAYCLICK',
				's1',
				$data
			);
			
			$result = array('txt' => '<p style="font-size:14px;"><strong>Спасибо за Ваш заказ!</strong><br/>В ближайшее время с Вами свяжутся.</p>');
			
		}
		
		print(json_encode($result));
		exit;
		
	}
	if($_POST['action'] == 'feedbackcont') {
		
		$result = array('message' => "", 'status' =>
"error");
		if(empty($_POST['name']))
			$result['message'] = '<p class="error">Обязательное поле «Ваше имя:» Не заполнено</p>';
		
		if(empty($_POST['message']))
			$result['message'] .= '<p class="error">Обязательное поле «Сообщение:» Не заполнено</p>';
		

		if(!empty($_POST['name']) && !empty($_POST['message'])) {
			
			$data = array(
				'NAME' => $_POST['name'],
				'MESSAGE' => $_POST['message'],
			);
			
			if(!empty($_POST['email']))
				$data['EMAIL'] = $_POST['email'];
			
			CEvent::SendImmediate(
				'CALLFORM',
				's1',
				$data
			);
			
			$result = array('status' => 'success', 'message' => '<p class="success"><strong>Ваше сообщение отправлено</p>');
			
		}
		
		print(json_encode($result));
		exit;
		
	}
	
	if($_POST['action'] == 'comment' && CModule::IncludeModule("iblock")) {
		
		$result = array('txt' => "");
		if(empty($_POST['name']))
			$result['txt'] = '<p class="error">Необходимо заполнить поле «Ваше имя»</p>';
		
		if(empty($_POST['text']))
			$result['txt'] .= '<p class="error">Необходимо заполнить поле «Комментарий»</p>';
		
		if(!empty($_POST['name']) && !empty($_POST['text']) && !empty($_POST['product'])) {
			
			$props = array(
				'PRODUCT' => $_POST['product'],
				'NAME' => $_POST['name'],
				'COMMENT' => $_POST['text'],
			);
			
			if(!empty($_POST['advantage']))
				$props['DIGNITY'] = $_POST['advantage'];
			
			if(!empty($_POST['disadvantage']))
				$props['DISAD'] = $_POST['disadvantage'];
			
			$data = array(
				'NAME' => $_POST['name'] . ' | ' . date('Y.m.d'),
				"IBLOCK_ID" => 7, 
				'ACTIVE' => 'N',
				'PROPERTY_VALUES' => $props,
			);
			
			$el = new CIBlockElement; 
			$ID = $el->Add($data); 
			
			if($ID) {
				

			$data = $props;
			$data['LINK'] = '<a href="//' . SITE_SERVER_NAME . '/bitrix/admin/iblock_element_edit.php?IBLOCK_ID=7&type=catalog&ID=' . $ID . '&lang=ru&find_section_section=0&WF=Y">Модерировать</a>';
		
			CEvent::Send(
				'ADDREVIEW',
				's1',
				$data
			);
				
			$result = array('txt' => '<p>Спасибо! Ваш отзыв появится на сайте после проверки модератором</p>');
			
			}
			
		}
		else
			$result['txt'] .= '<br>';
		
		print(json_encode($result));
		exit;
		
	}
	
}

function getCountBasket() {
	
      CModule::IncludeModule("sale");
	  
	  $out = array('ids' => CUtil::PhpToJSObject(array(), false, true, true), 'count' => 0);
	  $ids = array();
	  $items = CSaleBasket::GetList(false, array("FUSER_ID" => CSaleBasket::GetBasketUserID(),"LID" => SITE_ID,"ORDER_ID" => "NULL", 'CAN_BUY' => 'Y'),false,false,array("ID", 'PRODUCT_ID' ));
	  
		if( $items->SelectedRowsCount()) {
                    $key = 0 ;
			while($item = $items->Fetch()) {
				
				$ids[$key]['ID'] = $item['PRODUCT_ID'];
                                $db_res = CSaleBasket::GetPropsList(
                                        array(
                                                "SORT" => "ASC",
                                                "NAME" => "ASC"
                                            ),
                                        array("BASKET_ID" => $item['ID'])
                                    );
                                while ($ar_res = $db_res->Fetch())
                                {
                                    if ( $ar_res["CODE"] == 'SIZE'){
                                       $ids[$key]['SIZE'] =  $ar_res["VALUE"];
                                    }
                                }
				$key++;
			}
			
			$out = array('ids' => CUtil::PhpToJSObject($ids, false, true, true), 'count' => $items->SelectedRowsCount(), $ids_b);
						
		}
      return $out;
}


$countProduct = getCountBasket();



AddEventHandler("main", "OnBeforeUserLogin", "OnBeforeUserLoginHandler");

function OnBeforeUserLoginHandler(&$arFields) {
	

	if (isset($arFields['LOGIN']) && strpos($arFields['LOGIN'], "@")) {

		$filter = Array( "=EMAIL" => $arFields['LOGIN']);
		$fields = array('SELECT' => array('LOGIN'));
		
		$rsUsers = CUser::GetList($by="id", $order="desc", $filter, $fields);
		$res = $rsUsers->Fetch();

		$arFields["LOGIN"] = $res['LOGIN'];

	}
	
}


function getInclinationByNumber($number, $arr = Array()) {
    $number = (string) $number;
    $numberEnd = substr($number, -2);
    $numberEnd2 = 0;
    if(strlen($numberEnd) == 2){
        $numberEnd2 = $numberEnd[0];
        $numberEnd = $numberEnd[1];
    }

    if ($numberEnd2 == 1) return $arr[2];
    else if ($numberEnd == 1) return $arr[0];
    else if ($numberEnd > 1 && $numberEnd < 5)return $arr[1];
    else return $arr[2];
}

AddEventHandler("sale", "OnOrderNewSendEmail", "modifySendingSaleData");


function modifySendingSaleData($orderID, &$eventName, &$arFields) {
	
    $phone = '';
	
    $arOrder = CSaleOrder::GetByID($orderID);
 
    $orderProps = CSaleOrderPropsValue::GetOrderProps($orderID);
 
    while ($arProps = $orderProps->Fetch()) {
		
        if ($arProps['CODE'] == 'PHONE') {
			
            $phone = htmlspecialchars($arProps['VALUE']);
			
			break;
			
        }
		
    }
     
    $arFields['PHONE'] = $phone;
	
}
AddEventHandler("sale", "OnSaleComponentOrderOneStepComplete", "OnBeforeOrderAddHandler");

function OnBeforeOrderAddHandler($ID, $arOrder) {
    CModule::IncludeModule( 'sale' );

    $pay_system=CSalePaySystem::GetByID($arOrder['PAY_SYSTEM_ID']);

    $dbBasketItems = CSaleBasket::GetList(array("ID" => "ASC"), array("ORDER_ID" => intval($ID)), false, false, array("ID", "NAME", "QUANTITY", "PRICE", "CURRENCY"));
    while( $arItems = $dbBasketItems->Fetch() )
    {
        $list .= $arItems['NAME'].'<br>';
    }
    if($arOrder2 = CSaleOrderPropsValue::GetOrderProps($ID) ) {
        while ($arProps = $arOrder2->Fetch()) {
            $result[$arProps['CODE']] 	= $arProps['VALUE'];
        }
    }
    if($arOrder = CSaleOrder::GetByID($ID)) {
        $arDeliv = CSaleDelivery::GetByID($arOrder['DELIVERY_ID']);
        $delivery = $arDeliv['NAME'];
        $price = $arOrder['PRICE'];
        $description = $arOrder['USER_DESCRIPTION'];
    }

    if( $result['CITY']=='Москва') {
        $arEventFields= array(
            "ORDER_ID" => $ID,
            "CITY" => $result['CITY'],
            "USER_NAME" => $result['FIO'],
            "PHONE" => $result['PHONE'],
            "EMAIL" => $result['EMAIL'],
            "ORDER_LIST" => $list,
            "PRICE" => $price,
            "USER_DESCRIPTION" => $description,
            "DELIVERY" => $delivery,
            "PAY_SYSTEM" => $pay_system['NAME']
        );
        CEvent::Send("SEND_ORDER_NEW", SITE_ID, $arEventFields, "N", 63);
    } else {
		
        $arEventFields= array(
            "ORDER_ID" => $ID,
            "CITY" => $result['CITY'],
            "USER_NAME" => $result['FIO'],
            "PHONE" => $result['PHONE'],
            "EMAIL" => $result['EMAIL'],
            "ORDER_LIST" => $list,
            "PRICE" => $price,
            "USER_DESCRIPTION" => $description,
            "DELIVERY" => $delivery,
            "PAY_SYSTEM" => $pay_system['NAME']
        );
        CEvent::Send("SEND_ORDER_NEW_NON_MSK", SITE_ID, $arEventFields, "N", 64);
    }
    /*
            $dbOrderProps = CSaleOrderPropsValue::GetList(
                    array("SORT" => "ASC"),
                    array("ORDER_ID" => $ID, "CODE"=>array("LOCATION", "CITY", "PHONE", "EMAIL", "NAME", "STREET"))
                );
                while ($arOrderProps = $dbOrderProps->GetNext()):

                endwhile;	*/

    /*
    if($arOrder = CSaleOrder::GetByID($ID)) {

    }*/

}