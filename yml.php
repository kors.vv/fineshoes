<?php
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

	define("FILE_NAME_YML", "market.yml");
	define("SHOP_NAME", "Fineshoes");
	define("COMPANY_NAME", htmlspecialchars("\"Шилин А.В.\""));
	define('COMPANY_TYPE', 'ИП ');
	define('COMPANY_URL', 'https://fineshoes.ru/');

	if (!CModule::IncludeModule("highloadblock"))
	{
	   ShowError(GetMessage("Модуль highloadblock не установлен."));
	   return;
	}

	use Bitrix\Highloadblock as HL;
	use Bitrix\Main\Entity;

	//сначала выбрать информацию о ней из базы данных
	$hldata = Bitrix\Highloadblock\HighloadBlockTable::getById(2)->fetch();

	//затем инициализировать класс сущности
	$hlentity = Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hldata);

	$hlDataClass = $hldata['NAME'].'Table';

	CModule::IncludeModule("iblock");

	global $APPLICATION;
	
	class YMLGenerator {

		private $fp;
		private $hlDataClass;
		public $offers = array();

	    function __construct($hlDataClass) {
	    	$this->hlDataClass = $hlDataClass;
	    }


		public function generate() {
			$this->fp = fopen($_SERVER["DOCUMENT_ROOT"]."/".FILE_NAME_YML, "wb");

			self::writeYMLHeaders();
			self::writeRootElement();

			fclose($this->fp);
		}

		public function writeYMLHeaders() {
			fwrite($fp, '<? header("Content-Type: text/xml; charset=utf-8");?>');

			$headers = "<"."?xml version=\"1.0\" encoding=\"utf-8\"?".">" . "\n";
			$headers .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">'."\n";

			fwrite($this->fp, $headers);
		}

		public function writeRootElement() {
			$rootOpeningTag = '<yml_catalog date="'.date("Y-m-d H:i").'">'."\n" . "<shop>\n";
			$rootClosingTag = "\n</shop>\n" . "</yml_catalog>";

			$shopContent = self::generateShopContent(COMPANY_NAME);

			fwrite($this->fp, $rootOpeningTag);

			fwrite($this->fp, $shopContent);

			fwrite($this->fp, $rootClosingTag);
		}

		public function generateShopContent($name) {

			$content = "<name>" . SHOP_NAME . "</name>" . "\n";
			$content .= "<company>" . COMPANY_TYPE . $name . "</company>" . "\n";
			$content .= '<url>' . COMPANY_URL . "</url>\n";

			$currenciesContent = array('RUR' => 1);
			$content .= self::getCurrencies($currenciesContent);
			$content .= self::getCategories();
			$content .= self::getDelieveryOptions();
			$content .= self::getOffers();

			return $content;
		}

		private function getCurrencies($contents) {
			$currencies = "<currencies>\n";

			foreach($contents as $key => $value) {
				$currencies .= "<currency id=\"$key\" rate=\"$value\"/>\n";
			}

			$currencies .= "</currencies>\n";

			return $currencies;
		}

		private function getCategories() {
			$categories = "<categories>\n";

			$arFilter = array('IBLOCK_ID' => 6, 'ACTIVE' => 'Y'); 
			$arSelect = array('ID', 'NAME', 'IBLOCK_SECTION_ID');
			$rsSection = CIBlockSection::GetTreeList($arFilter, $arSelect); 

			while($arSection = $rsSection->Fetch()) {

				$parentId = $arSection['IBLOCK_SECTION_ID'] != null ? 
							$arSection['IBLOCK_SECTION_ID'] :
							"";

				$arInternalFilter = array('IBLOCK_ID' => 6, 'SECTION_ID' => intval($arSection['ID']), 'ACTIVE' => 'Y'); 
				$arInternalSelect = array('ID', 'NAME');
				$rsInternalSection = CIBlockSection::GetTreeList($arInternalFilter, $arInternalSelect); 

				if($rsInternalSection->Fetch() == false) {
					$this->offers[$arSection['NAME']] = $arSection['ID'];
				}

				$categories .= "<category id=\"{$arSection['ID']}\"";
				$categories .= $parentId != "" ? " parentId=\"$parentId\"" : "";
				$categories .= ">" . htmlspecialchars($arSection['NAME']) . "</category>\n";
			}
			

			$categories .= "</categories>\n";

			return $categories;
		}

		private function getDelieveryOptions() {
			$dOptions = "";
			$dOptions .= "<delivery-options>\n";
			$dOptions .= "<option cost=\"300\" days=\"32\"/>\n";
			$dOptions .= "</delivery-options>\n";

			return $dOptions;
		}

		private function getOffers() {
			$offers = "<offers>\n";
			
			foreach ($this->offers as $key=>$value) {

				$arSelect = Array();
				$arFilter = Array("IBLOCK_ID"=>6, "SECTION_ID"=>$value, "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");
				$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

				while($ob = $res->GetNextElement())
				{	

					 	$arFields = $ob->GetFields();
					 	$arProps = $ob->GetProperties();

					 	$hlDataClass = $this->hlDataClass;
					 	$vendorCode = $arProps['BRAND']['VALUE'];
					 	$resultVendorArr = $hlDataClass::getList(array(
							'select' => array("UF_NAME"),
						    'filter' => array("UF_XML_ID"=>$vendorCode),
						))->fetch();
						$resultVendor = $resultVendorArr['UF_NAME'];

					 	$ar_res = CPrice::GetBasePrice($arFields['ID']);
				 		$price = intval($ar_res['PRICE']);

					 	if($price == 0) {
							continue;
						}

					 	// Если производитель не заполнен то задать название магазина в качестве производителя
					 	if($resultVendor == null || $resultVendor == "") {
					 		$resultVendor = "Squire";
					 	}

					 	$offers .= "<offer id=\"{$arFields['ID']}\" available=\"true\">\n";

						$offers .= "<url>" . COMPANY_URL . substr($arFields['DETAIL_PAGE_URL'], 1) . "</url>\n";
						$offers .= "<price>" . $price . "</price>\n";
						$offers .= "<currencyId>" . "RUR" . "</currencyId>\n";
						$offers .= "<categoryId>" . $value . "</categoryId>\n";
						$offers .= "<delivery>" . "true" . "</delivery>\n";
						$offers .= "<name>" . htmlspecialchars($arFields['NAME']) . "</name>\n";
						$offers .= "<vendor>" . htmlspecialchars($resultVendor) . "</vendor>\n";
						$offers .= "<model>" . htmlspecialchars($arFields['NAME']) . "</model>\n";
						$offers .= "</offer>\n";
					
				}
				
			}
			

			$offers .= "</offers>";

			return $offers;
		}
	}

	$newYMLgen = new YMLGenerator($hlDataClass);
	$newYMLgen -> generate();
