<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Адреса наших магазинов, где вы можете купить обувь ручной работы в Москве, Петербурге и в других городах России.");
$APPLICATION->SetPageProperty("title", "Магазин классической кожаной обуви | Где купить мужскую обувь в Москве и Петербурге?");
$APPLICATION->SetTitle("Контакты ");
?><h1>Контакты магазинов</h1>
<div class="text-block">
	<p>
		 Шоурум и розничная продажа обуви осуществляется по адресам:
	</p>
</div>
<div class="contacts_list clearfix">
	<div class="contact_item" itemscope="" itemtype="http://schema.org/LocalBusiness">
		<div class="ttl">
			 Mосква
		</div>
		<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" class="address">
			 119034, г. <span itemprop="addressLocality">Москва</span>, <br>
 <span itemprop="streetAddress">ул. Льва Толстого, дом 23/7c3, п. 3, 1 эт.&nbsp;</span>
		</div>
		<div class="time">
			<p>
 <strong>Режим работы: <br>
				 пн-пт: </strong><time datetime="Mo-Fr 11:00-21:00" itemprop="openingHours">11:00—21:00</time> <br>
 <strong>сб-вс и праздники: </strong><time datetime="Sa-Su 11:00-19:00" itemprop="openingHours">11:00—19:00</time>
			</p>
		</div>
		<div class="phone" itemprop="telephone">
			<a href="tel:+74956629876" onclick="yaCounter47085678.reachGoal('call'); return true;">+7 495 66-2-9876</a>
		</div>
		<div class="email">
			<a href="mailto:mail@fineshoes.ru" onclick="yaCounter47085678.reachGoal('email'); return true;">mail@fineshoes.ru</a>
		</div>
		<div class="map_item" id="map-central-cnt-9">
		</div>
	</div>
	<div class="contact_item" itemscope="" itemtype="http://schema.org/LocalBusiness">
		<div class="ttl">
			 Санкт-Петербург
		</div>
		<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" class="address">
			 191119, г. <span itemprop="addressLocality">Санкт-Петербург</span>, <br>
 <span itemprop="streetAddress">ул. Лиговский проспект, д. 92Д</span>
		</div>
		<div class="time">
			<p>
 <strong>Режим работы: <br>
				 пн-пт: </strong><time datetime="Mo-Fr 11:00-21:00" itemprop="openingHours">11:00—21:00</time> <br>
 <strong><strong>сб-вс</strong>&nbsp;и праздники:</strong> <time datetime="Sa-Su 11:00-19:00" itemprop="openingHours">11:00—19:00</time>
			</p>
		</div>
		<div class="phone" itemprop="telephone">
			<a href="tel:+74956629876" onclick="yaCounter47085678.reachGoal('call'); return true;">+7 812 40-727-60</a>
		</div>
		<div class="email">
				<a href="mailto:mail@fineshoes.ru" onclick="yaCounter47085678.reachGoal('email'); return true;">mail@fineshoes.ru</a>
		</div>
		<div class="map_item" id="map-central-cnt-10">
		</div>
	</div>
	<div class="clear">
	</div>
</div>
<div class="text-block">
	<p>
		 По вопросам представительства, покупок оптом и сотрудничества пишите на нашу общую электронную почту <a href="mailto:mail@fineshoes.ru" onclick="yaCounter47085678.reachGoal('email'); return true;">mail@fineshoes.ru</a> с пометкой «для Владимира». Мы вышлем вам коммерческое предложение и все условия работы.
	</p>
</div>
<div class="clear">
</div>
 <?$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "file",
		"PATH" => SITE_DIR."include/feedback.php"
	)
);?>
<div class="small-text-block">
	<div class="rekviziti">
 <strong>Реквизиты компании:</strong> <br>
  		 
                 ООО "Полезные вещи" <br>
		 ИНН 7839443694 <br>
                 КПП 783901001 <br>
                 ОГРН 1117847185950 <br>

	</div>
	<div class="ur_adres">
 <strong>Юридический адрес:</strong> <br>
		 190121, Москва, Английский пр д.2-56
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('.form_509_76').off('submit');
		$('.form_509_76').on('submit',function(){
			$.post('/',$(this).serializeArray(),function(data){
				if (data.status == 'error'){
					$('.message-container-509-76').html(data.message);
				} else {
					$('.form_509_76').replaceWith(data.message);
				}
			},'json');
			return false;
		});
	});
</script>
<script type="text/javascript">
	$(window).load(function(){
		ymaps.ready(initMapContainer);
		function initMapContainer() {
			myMap_cnt9 = new ymaps.Map("map-central-cnt-9", {
				center: [55.735403, 37.584281],
				zoom: 16,
				controls: ['zoomControl', 'typeSelector', 'fullscreenControl', 'geolocationControl','trafficControl']
			});
			myCollection_cnt9 = new ymaps.GeoObjectCollection();
			myCollection_cnt9.add(new ymaps.Placemark([55.735403, 37.584281],{balloonContent: '<p>119034, г. Москва,<br/> ул. Льва Толстого, дом 23/7c3<br/>+7 495 66-2-9876</p>'}));
			myMap_cnt9.geoObjects.add(myCollection_cnt9);

			myMap_cnt10 = new ymaps.Map("map-central-cnt-10", {
				center: [59.920239,30.356795],
				zoom: 16,
				controls: ['zoomControl', 'typeSelector', 'fullscreenControl', 'geolocationControl','trafficControl']
			});
			myCollection_cnt10 = new ymaps.GeoObjectCollection();
			myCollection_cnt10.add(new ymaps.Placemark([59.920239,30.356795],{balloonContent: '<p>191119, г. Санкт-Петербург,<br/> ул. Лиговский проспект, д. 92Д<br/>+7 812 40-727-60</p>'}));
			myMap_cnt10.geoObjects.add(myCollection_cnt10);
		};
	});
</script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>