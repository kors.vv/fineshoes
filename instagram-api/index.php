<?php
$_SERVER["DOCUMENT_ROOT"] = "/var/www/topform/data/www/fineshoes.ru";
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('CHK_EVENT', true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

// Supply a user id and an access token
$userid = "fineshoes.ru";
$accessToken = "6623579343.bf35f9e.bdc9dca497cb4599b80e6a4ccba299be";

// Gets our data
function fetchData($url){
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, 20);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

// Pulls and parses data.
$result = fetchData("https://api.instagram.com/v1/users/self/media/recent/?access_token={$accessToken}");
$result = json_decode($result);

if (CModule::IncludeModule("iblock")) {
    foreach ($result->data as $post) {
        $detail = fetchData("https://api.instagram.com/v1/media/{$post->id}/?access_token={$accessToken}");
        $detail = json_decode($detail);

        $preview_text = preg_replace('/\W(\#[a-zA-ZА-Яa-я0-9]+\b)(?!;)/iu', '', $post->caption->text);
        $detail_text = preg_replace('/\W(\#[a-zA-ZА-Яa-я0-9]+\b)(?!;)/iu', '', $detail->data->caption->text);
        $tags = implode(' ', $detail->data->tags);

        $comments = json_decode(fetchData("{$post->link}?__a=1"));
        $comments = serialize($comments->graphql->shortcode_media->edge_media_to_comment->edges);

        $el = new CIBlockElement;

        $PROP = array();
        $PROP[97] = $post->likes->count; // лайки
        $PROP[98] = $tags; // хештеги
        $PROP[99] = $comments; // комменты
        $PROP[100] = $post->link; // ссылка

        $arLoadProductArray = Array(
            "MODIFIED_BY" => 1,
            "IBLOCK_ID" => 22,
            "IBLOCK_SECTION_ID" => false,
            "PROPERTY_VALUES" => $PROP,
            "NAME" => $post->id,
            "CODE" => $post->id,
            "ACTIVE" => "Y",
            "ACTIVE_FROM" => date('d.m.Y', $post->caption->created_time),
            "PREVIEW_TEXT" => $preview_text,
            "DETAIL_TEXT" => $detail_text,
            "DETAIL_PICTURE" => CFile::MakeFileArray($post->images->standard_resolution->url)
        );
		
		// Проверка элемента на существование и добавление в инфоблок
		$rsItems = CIBlockElement::GetList(array(),array('IBLOCK_ID' => 22, '=CODE' => $post->id), false, false, array('ID'));
		if ($arItem = $rsItems->GetNext()) {
			$arLoadProductArrayUpdate = Array(
				"MODIFIED_BY" => $USER->GetID(), 
				"PREVIEW_TEXT" => $preview_text,
				"DETAIL_TEXT" => $detail_text,
				"PROPERTY_VALUES"=> $PROP,
			);
			if($el->Update($arItem['ID'], $arLoadProductArrayUpdate))
				echo "Element exists and has been updated. ID: ".$arItem['ID']." <br />";
		} else {
			if ($PRODUCT_ID = $el->Add($arLoadProductArray, false, true, true))
				echo "New ID: {$PRODUCT_ID} <br>";
		}
    }
}
include($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");