<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Раздел с замками, которые идут по скидке. Заходите и приобретайте у нас велозамок.");
$APPLICATION->SetPageProperty("keywords", "Акции на велозамки, велосипедные замки, купить велозаомк, купить замок в спб, купить велозамок в Москве");
$APPLICATION->SetPageProperty("title", "Акции и скидки на замки для велосипедов и мотоциклов | Распродажа велозамков в Москве и Санкт-Петербурге");
$APPLICATION->SetTitle("Акции");
$GLOBALS['arrFilter']['=PROPERTY_WHERE_ACTION_VALUE'] = 'Да';
?>
<div class="text-block">
	<h1>Акции на велозамки</h1>
	<p>
		 В этом разделе вы найдете модели от разных производителей замков со скидкой до 50%. Количество ограничено.
	</p>
	<div id="record-TASK_16304-19144-cover" class="feed-com-block-cover">
		<div id="record-TASK_16304-19144" class="feed-com-block-outer">
			<div class="feed-com-block blog-comment-user-1 sonet-log-comment-createdby-1 feed-com-block-approved feed-com-block-read">
				<div class="feed-com-text">
					<div class="feed-com-text-inner">
						<div id="record-TASK_16304-19144-text" class="feed-com-text-inner-inner">
							 1) У Вас День Рождения — значит, вам к нам!<br>
 <br>
							 День Рождения — замечательный праздник, который с замиранием сердца ждет каждый из нас. Магазин bikelock дарит каждому покупателю в этот день скидку 10% на любой товар. Акция действует в течение 7 дней до и 7 дней после вашего Дня Рождения.<br>
 <br>
							 Для получения скидки по акции, Вам необходимо:<br>
							<ol>
								<li>Прийти в магазин bikelock, выбрать любой товар. Или совершить заказ на сайте.</li>
								<li>При оплате на кассе проинформировать кассира о желании получить скидку по акции «Именинники». Или указать это в комментарии к заказу на сайте.</li>
								<li>Предъявить документ, подтверждающий дату вашего рождения. При заказе на сайте в отдельном письме указать номер заказа и приложить фото или скан документа.</li>
								<li>Получить скидку 10% по акции «Именинник».</li>
							</ol>
							 Предложение действует в магазинах bikelock в Москве и Санкт-Петербурге или при заказе на сайте. Скидка не суммируется с другими предложениями и не распространяется на товары, участвующие в распродаже.<br>
 <br>
							 2)&nbsp;При покупке одновременно двух замков — скидка 5-10% в зависимости от суммы заказа.
						</div>
						<div class="feed-com-text-inner-inner" style="margin-left: 30px;">
						</div>
						<div class="feed-com-text-inner-inner">
							 3)&nbsp;Если вы покупали у нас замок ранее — возьмите с собой (или приложите фото/скан по электронной почте) чек или номер прошлого оплаченного заказа при новой покупке. Мы обязательно сделаем скидку или подарок постоянным клиентам.
						</div>
						<div class="feed-com-text-inner-inner">
						</div>
						<div class="feed-com-text-inner-inner">
							 4) По всей России у нас действует бесплатная доставка до пункта выдачи транспортной компании Сдэк.&nbsp;
						</div>
						<div class="feed-com-text-inner-inner">
						</div>
						<div class="feed-com-text-inner-inner">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"list_easy", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"FILTER_NAME" => "arrFilter",
		"USE_FILTER" => "Y",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => "6",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => "-",
		"LINE_ELEMENT_COUNT" => "3",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_LIMIT" => "1",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "30",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "NEW",
			1 => "HIT",
			2 => "PRICE_BEFORE",
			3 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => "",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"TEMPLATE_THEME" => "blue",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "list_easy"
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>