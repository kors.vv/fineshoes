<meta itemprop="name" content="Fine Shoes - Москва"></meta>								 
<div class="addr_title">Магазин в Москве</div>
 								 
<div class="addr_phone"><a href="tel:+74952255480" onclick="yaCounter47085678.reachGoal('call'); return true;"><span itemprop="telephone">+7 495 22-55-480</span></a></div>
 								 
<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" class="addr_text"> 
  <div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" class="addr_text">119021, г. Москва, ул. Льва Толстого, дом 23/7c3, п. 3, 1 эт.</div>
 </div>
 <br>
<div class="addr_worktime"><span>Режим работы:</span> 
  <br />
 <meta content="Mo-Fr 11:00-21:00" itemprop="openingHours"></meta>пн-пт: 11:00&mdash;21:00</div>
 								 
<div class="addr_holidays"><meta content="Sa-Su 11:00-19:00" itemprop="openingHours"></meta>сб-вс и праздники: 11:00—19:00</div>
 								<meta itemprop="email" content="mail@fineshoes.ru"></meta>