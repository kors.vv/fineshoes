<div class="addr_title">
	 Магазин в Санкт-Петербурге
</div>
<div class="addr_phone">

<a href="tel:+78124072760" onclick="yaCounter47085678.reachGoal('call'); return true;"><span itemprop="telephone">+7 812&nbsp;40-727-60</span></a>
</div>
<div itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress" class="addr_text">
 <span itemprop="postalCode">191119</span>, <span itemprop="addressLocality">г. Санкт-Петербург</span>, <span itemprop="streetAddress">ул. Лиговский пр., д. 92Д</span>
</div>
<br>
<div class="addr_worktime">
	 Режим работы:<br>
	 пн-пт: 11:00—21:00
</div>
<div class="addr_holidays">
	 сб-вс и праздники: 11:00—19:00
</div>
<br>