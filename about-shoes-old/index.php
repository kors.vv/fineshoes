<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Информация о обуви");
$APPLICATION->SetTitle("Об обуви");

?>
<h1><?$APPLICATION->ShowTitle()?></h1>
<?
	$res = CIBlockSection::GetList(
	    Array(),
	    Array(
	        /*'TYPE'=>'news',
	        'SITE_ID'=>SITE_ID,
	        'ACTIVE'=>'Y',*/
        	"IBLOCK_ID" => array(1),
	    ), false
	);
	$ArrayCategories = array();
	while($ar_res = $res->GetNext()) {
		$ArrayCategories[] = $ar_res;
	}

 if (!empty($ArrayCategories)) {?>
<div class="articles_list articles_list_p clearfix">
	<?php foreach ($ArrayCategories as $k => $cat) { ?>
		<?php if (!empty($cat["PICTURE"])) {$renderImage = CFile::ResizeImageGet($cat["PICTURE"], Array("width" => 385, "height" => 200), BX_RESIZE_IMAGE_EXACT);} ?>
		<article class="articles_item<? if($k > 0 && $k % 3 == 0) echo ' last'?> ">
			<div class="art_img">
				<a href="<?=str_replace("#SITE_DIR#", "", $cat['CODE']);?>">
					<?php if (!empty($renderImage)) { echo CFile::ShowImage($renderImage['src'], 385, 200, "", "", true); } ?>
				</a>
			</div>
			<h3 class="title"><a href="<?=str_replace("#SITE_DIR#", "", $cat['CODE']);?>"><?=$cat['NAME'];?></a></h3>
		</article>
	<?php } ?>
</div>
<?php } ?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
