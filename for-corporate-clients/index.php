<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Корпоративным клиентам | Интернет-магазин Fineshoes");
$APPLICATION->SetPageProperty("keywords", "предложение для корпоративных клиентов");
$APPLICATION->SetPageProperty("description", "Предложение корпоративным клиентам в интернет-магазине Fineshoes.");
$APPLICATION->SetTitle("Корпоративным клиентам");
?>
<h1>Корпоративным клиентам</h1>
<p>Всем компаниям мы предлагаем скидку 30-50% на покупку мужской классической обуви в колличестве от 10 пар.
	Дополнительно мы можем сделать брендированные подарки. Если сотрудники вашей компании предпочитают классический стиль в обуви, то это предложение для вас.</p>
<p>Для уточнения деталей отправьте письмо на почту <a href="mailto:mail@fineshoes.ru" target="_blank" onclick="yaCounter47085678.reachGoal('email'); return true;">mail@fineshoes.ru</a> с пометкой "Корпоративный заказ".</p>
<p><img alt="корпоративным клиентам" src="/upload/medialibrary/2c6/2c6f0a61f115ad6f856530a735eaf705.jpg" style="float: left; margin-top: 20px;" title="Оптовым покупателям"></p>
<br clear="all">
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>


