<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus("404 Not Found");
@define("ERROR_404","Y");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена");?>
<div class="page_404">
    <div>
        <h1><span>404</span>Извините, страница не найдена</h1>
        <p>Простите, но у нас возникли проблемы с поиском страницы,<br/> которую Вы запрашиваете</p>
        <p><a href="/" class="btn_tr top_arrow">Перейти на главную</a></p>
    </div>
</div>

<?php require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
