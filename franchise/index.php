<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Франшиза по продаже обуви | Интернет-магазин Fineshoes");
$APPLICATION->SetPageProperty("description", "Fineshoes.ru — франшиза по продаже обуви по всей России. Приглашаем франчайзи к сотрудничеству для продажи обуви.");
$APPLICATION->SetTitle("Франшиза");
?><h1>Франшиза</h1>
<br>
<p>
 <span style="font-size: 14pt;">FineShoes.ru — интернет-магазин, предлагающий бренды по разумной цене, предоставляет услуги франчайзинга на территории России и стран ближнего зарубежья. </span>
</p>
 <span style="font-size: 14pt;"> </span>
<p>
 <span style="font-size: 14pt;">
	Интернет-магазин Fineshoes.ru поставляет различные виды классической обуви. В ассортименте: оксфорды, дерби, монки, а также лоферы и топсайдеры. Ассортимент постоянно увеличивается. </span>
</p>
 <span style="font-size: 14pt;"> </span>
<p>
 <span style="font-size: 14pt;">
	В Москве Санкт-Петербурге мы являемся одними из самых крупных магазинов по продаже классической мужской обуви ручной работы, об этом говорит наш широкий ассортимент. Когда мы решили основать магазин по продаже обуви, первой мыслью стало снабдить людей проживающих в регионах надежным и качественным товаром. При этом наши покупатели хотят ходить в качественной обуви по приемлемым ценам. В следствии этого мы сделали наш ассортимент таким, чтобы каждый мог подобрать подходящий для себя товар по соотношению цена-качество. Как вы уже поняли именно по этому высокую долю наших продаж составляют заказы из регионов и всевозможных периферий нашей необъятной родины. </span>
</p>
 <span style="font-size: 14pt;"> </span>
<p>
 <span style="font-size: 14pt;">
	На сегодняшний день магазин fineshoes.ru представляет собой отлаженную систему по продаже обуви в Москве и Петербурге, а также онлайн по всей России и странам бывшего содружества. </span>
</p>
 <span style="font-size: 14pt;"> </span>
<p>
 <span style="font-size: 14pt;">
	Мы предлагаем готовое решение в виде оптимального набора брендов, свободного склада и контактной информации на сайте для вашего магазина. Вам не надо изобретать велосипед, он уже изобретен, не надо набивать шишки и наступать на грабли, это все уже пройденный нами ранее этап. Хотите быть частью федеральной сети fineshoes.ru и следовать по успешному шаблону? Тогда это ваш выбор.</span>
</p>
<h2>
Уникальные особенности франшизы: </h2>
<ul>
	<li><span style="font-size: 14pt;">Большее количество брендов для франчайзинга;</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Условия по каждому бренду обуви являются аналогичными как при работе с дистрибьютором марки;</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Высокий процент наценки на изделия;</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Добавление вашей контактная информации на сайте;</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Быстрая сборка и доставка заказа до ТК.</span><br>
 </li>
</ul>
<h2>
Описание франшизы:</h2>
<h2>
Формат 1. Шоурум </h2>
<ul>
	<li><span style="font-size: 14pt;">Для различных городов.</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Объем инвестиций — от 2 млн. руб.</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Товарный запас от 1.5 млн. руб.</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Роялти — отсутствует</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Срок окупаемости от 8 до 18 месяцев</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Торговая наценка до 200%</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Необходимая площадь от 25 до 40 кв.м.</span><br>
 </li>
</ul>
<h2>
Формат 2. Fine Store </h2>
<ul>
	<li><span style="font-size: 14pt;">Объем инвестиций — от 5 млн. руб.</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Торговое оборудование и ремонт от 1 млн. руб.</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Товарный запас от 4 млн. руб. (в зависимости от размера торговой площади)</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Роялти — отсутствует</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Срок окупаемости от 12 до 18 месяцев</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Торговая наценка до 200%</span><br>
 <span style="font-size: 14pt;"> </span></li>
	<li><span style="font-size: 14pt;">Необходимая площадь: от 30 до 60 кв.м.</span><br>
 <span style="font-size: 14pt;"> </span></li>
</ul>
 <span style="font-size: 14pt;"> </span>
<p>
 <span style="font-size: 14pt;">
	Более детальную информацию запрашивайте по электронной почте.</span><br>
</p>
<ul>
</ul><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>