<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("description", "Шоу-рум обуви ручной работы в Москве и Петербурге. О компании Fineshoes.");
$APPLICATION->SetPageProperty("title", "О магазине Fineshoes в Москве и Санкт-Петербурге | Интернет-магазин классической обуви");
$APPLICATION->SetTitle("О магазине");
?><h2>О нас</h2>
<p style="text-align: justify;">
	 Fineshoes.ru является новым, успешно стартовавшим магазином обуви, который может предложить своим покупателям широкий ассортимент выбора по максимально доступным ценам. В наших мультибрендовых магазинах представлен товар от производителей из ведущих стран, а это значит, что вся качественная обувь ручной работы собрана в одном месте, и нет необходимости искать ее на разных сайтах, достаточно просто зайти к нам и выбрать понравившеюся модель. Мы ведем сотрудничество только с проверенными и утвердившими свой статус на мировом рынке поставщиками. Абсолютно на весь товар, представленный на сайте, распространяется гарантия и возможность обмена-возврата.
</p>
<h3 style="text-align: justify;">Наиболее важными критериями при выборе обуви для нас являются:</h3>
<ul>
	<li>Ручная работа<br>
 </li>
	<li>Высокое качество<br>
 </li>
	<li>Производство в Европе<br>
 </li>
	<li>Высокая степень комфорта<br>
 </li>
	<li>Современный дизайн на тему вечная классика<br>
 </li>
	<li>Оптимальное сочетание стоимости и качества обуви в каждой ценовой категории</li>
</ul>
<p>
</p>
<p style="text-align: justify;">
	 Практически весь модельный ряд представляемой на Fineshoes.ru обуви пригоден для российского климата и подходит для носки на каждый день. Естественно, чтобы проверить обувь на прочность и качество исполнения в полном масштабе, нужно протестировать ее на себе и убедится в этом лично. За свое недолгое существование наш магазин уже успел обзавестись постоянными покупателями из многих городов России, преимущественно это люди из Москвы и Санкт-Петербурга. Отдавая свое предпочтение магазину Fineshoes.ru, вы автоматически становитесь членом клуба по интересам, который объединяет любовь к высококачественной обуви.
</p>
<p style="text-align: justify;">
	 Приятных вам покупок!
</p><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>