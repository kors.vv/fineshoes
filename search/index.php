<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Поиск");
?>
<h1>Поиск</h1>
<?//if(!empty($_GET['q'])):?>
    <div class="cabinet-block search_site_bikelock">

<?$APPLICATION->IncludeComponent(
	"fineshoes:catalog.search",
	"search",
	Array(
		"ACTION_VARIABLE" => "action",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"BASKET_URL" => "/personal/basket/",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "N",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"GENERAL_SITE_SEARCH" => "N",
		"HIDE_NOT_AVAILABLE" => "N",
		"IBLOCK_ID" => array("6","23"),
		"IBLOCK_TYPE" => "catalog",
		"LINE_ELEMENT_COUNT" => "4",
		"NO_WORD_LOGIC" => "N",
		"OFFERS_LIMIT" => "1",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "Y",
		"PAGER_TEMPLATE" => "about-shoes",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "32",
		"PRICE_CODE" => array("BASE"),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PROPERTY_CODE" => array("","WEIGHT","RATING","NEW","HIT","PRICE_BEFORE",""),
		"RESTART" => "N",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SHOW_PRICE_COUNT" => "1",
		"USE_LANGUAGE_GUESS" => "Y",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N"
	)
);?> 
        
		<?
		if (true)
		$APPLICATION->IncludeComponent(
			"bitrix:search.page",
			"search2",
			Array(
				"AJAX_MODE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "N",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"CACHE_TIME" => "3600",
				"CACHE_TYPE" => "A",
				"CHECK_DATES" => "N",
				"DEFAULT_SORT" => "rank",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"FILTER_NAME" => "",
				"NO_WORD_LOGIC" => "N",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "about-shoes",
				"PAGER_TITLE" => "Результаты поиска",
				"PAGE_RESULT_COUNT" => "10",
				"PATH_TO_USER_PROFILE" => "",
				"RATING_TYPE" => "",
				"RESTART" => "Y",
				"SHOW_ITEM_DATE_CHANGE" => "Y",
				"SHOW_ITEM_TAGS" => "Y",
				"SHOW_ORDER_BY" => "Y",
				"SHOW_RATING" => "",
				"SHOW_TAGS_CLOUD" => "N",
				"SHOW_WHEN" => "N",
				"SHOW_WHERE" => "N",
				"TAGS_INHERIT" => "Y",
				"USE_LANGUAGE_GUESS" => "Y",
				"USE_SUGGEST" => "Y",
				"USE_TITLE_RANK" => "Y",
				"arrFILTER" => array("iblock_catalog"),
				"arrFILTER_iblock_catalog" => array("6","23"),
				"arrFILTER_iblock_news" => array("all"),
				"arrFILTER_main" => array(""),
				"arrWHERE" => ""
			)
		);
		?>
    </div>
<?//endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>