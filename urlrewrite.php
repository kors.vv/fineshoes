<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/about-shoes/everything-about-the-british-footwear/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/everything-about-the-british-footwear/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/how-to-choose-and-match-shoes/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/how-to-choose-and-match-shoes/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/care-and-storage-of-shoes/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/care-and-storage-of-shoes/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/technology-manufacturing/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/technology-manufacturing/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/([^/]+)\\.html(\$|\\?.*)#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "",
		"PATH" => "/about-shoes/detail.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/(.*)/(\$|tags\\.|\\?.*)#",
		"RULE" => "ELEMENT_CODE=\$1",
		"ID" => "",
		"PATH" => "/about-shoes/tags.php",
	),
	array(
		"CONDITION" => "#^/about/instastyle/([0-9]+)/\$#",
		"RULE" => "ELEMENT_ID=\$1",
		"ID" => "",
		"PATH" => "/about/instastyle/detail.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/types-of-shoe/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/types-of-shoe/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/shoe-nuances/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/shoe-nuances/index.php",
	),
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/loafers/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/loafers/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/brogues/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/brogues/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/oxfords/#",
		"RULE" => "",
		"ID" => "bitrix:news.list",
		"PATH" => "/about-shoes/oxfords/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/monki/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/monki/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/derby/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-shoes/derby/index.php",
	),
	array(
		"CONDITION" => "#^/personal/orders/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/orders/index.php",
	),
	array(
		"CONDITION" => "#^/personal/order/#",
		"RULE" => "",
		"ID" => "bitrix:sale.personal.order",
		"PATH" => "/personal/order/index.php",
	),
	array(
		"CONDITION" => "#^/about-locks/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/about-locks/index.php",
	),
	array(
		"CONDITION" => "#^/accessories/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/accessories/index.php",
	),
	array(
		"CONDITION" => "#^/about-shoes/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/about-shoes/index.php",
	),
	array(
		"CONDITION" => "#^/catalog/#",
		"RULE" => "",
		"ID" => "bitrix:catalog",
		"PATH" => "/catalog/index.php",
	),
	array(
		"CONDITION" => "#^/store/#",
		"RULE" => "",
		"ID" => "bitrix:catalog.store",
		"PATH" => "/store/index.php",
	),
	array(
		"CONDITION" => "#^/stock/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/stock/index.php",
	),
	array(
		"CONDITION" => "#^/news/#",
		"RULE" => "",
		"ID" => "bitrix:news",
		"PATH" => "/Об обуви/index.php",
	),
);

?>