<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Оптовая продажа обуви");
$APPLICATION->SetPageProperty("description", "Fineshoes.ru — оптовая продажа обуви в Москве и Санкт-Петербурге.");
$APPLICATION->SetTitle("Информация для оптовых покупателей");
?>
<h1>Оптовикам</h1>
<span style="font-size: 13pt;">Мы предлагаем широкий выбор обуви оптом для дилеров и оптовых клиентов. Для вас&nbsp;—&nbsp;это отличная возможность предоставить качественный товар своим клиентам. Работая с нами, вы гарантированно получаете:</span><br>
<span style="font-size: 13pt;"> </span>
<ul>
	<li><span style="font-size: 13pt;">Большой выбор обуви</span></li>
	<li><span style="font-size: 13pt;">Удобный способ доставки</span></li>
	<li><span style="font-size: 13pt;">Выгодные цены</span></li>
</ul>
 <span style="font-size: 13pt;">
Актуальный прайс-лист вы можете запросить по телефону&nbsp;+7 812 40-727-60, либо отправить письмо на нашу почту <a href="mailto:mail@fineshoes.ru" onclick="yaCounter47085678.reachGoal('email'); return true;">mail@fineshoes.ru</a>.</span>
<br>
<p><img alt="Оптовым покупателям" src="/upload/medialibrary/616/6167805829f6d10d2f877317a1150617.jpg" style="float: left; margin-top: 20px;" title="Оптовым покупателям"></p>
<br clear="all">
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>