<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");

if(!$USER->GetID()) {
	
	
	LocalRedirect('/auth/');
	
}

else {
	
$ID = $USER->GetID();

$datadeliv = array(
					'PROPERTY_CITY_VALUE' => '',
					'PROPERTY_STREET_VALUE' => '',
					'PROPERTY_HOME_VALUE' => '',
					'PROPERTY_CORPUS_VALUE' => '',
					'PROPERTY_ROOM_VALUE' => '',
);

$rsUser = CUser::GetByID($USER->GetID()); 
$arUser = $rsUser->Fetch(); 
					
					
CModule::IncludeModule('iblock');
					
$elem = CIBlockElement::GetList(
						array(),
						array('PROPERTY_USER' => $USER->GetID(), 'IBLOCK_ID' => 8),
						false,
						1,
array('ID' ,'IBLOCK_ID', 'PROPERTY_CITY', 'PROPERTY_STREET', 'PROPERTY_HOME', 'PROPERTY_CORPUS', 'PROPERTY_ROOM')
);
					
$ii = $elem->Fetch();
if(!empty($ii))
	$datadeliv = $ii;

$post = false;

if(!empty($_POST)) {
	
	$dataadd = array();
	
	if(!empty($_POST['STREET']))
		$dataadd['STREET'] = $_POST['STREET'];
	
	if(!empty($_POST['CORPUS']))
		$dataadd['CORPUS'] = $_POST['CORPUS'];
	
	if(!empty($_POST['HOME']))
		$dataadd['HOME'] = $_POST['HOME'];
	
	if(!empty($_POST['ROOM']))
		$dataadd['ROOM'] = $_POST['ROOM'];
	
	if(!empty($_POST['city']) && $_POST['city'] == 'Другой'){
		
		$dataadd['CITY'] = $_POST['city_other'];
	
		if(empty($dataadd['CITY']))
			$dataadd['CITY'] = 'Другой';
			
	}
	else if(!empty($_POST['city']))
		$dataadd['CITY'] = $_POST['city'];
	
	$el = new CIBlockElement;
	
	$dataadd['USER'] = $ID;
	
	$dataadd = array(
		'NAME' => $USER->GetParam('EMAIL'),
		'PROPERTY_VALUES'=> $dataadd
	);
	
	if(!empty($ii['ID'])) {
		
		//UPDATE
		
		$el->Update($ii['ID'], $dataadd);
	}
	else {
		
		$dataadd['IBLOCK_ID'] = 8;
		$el->Add($dataadd);
	
	
	}
	
	$post = true;
}
				
?>
	<h1>Личный кабинет</h1>
<div class="cabinet-block clearfix">
	<div class="cabinet-user-menu">
		<a href="/personal/private/">Личные данные</a>
		<a class="active" href="/personal/address/">Адрес доставки</a>
		<a href="/personal/orders/">Мои заказы</a>
	</div>
	<div class="cabinet-user-info">
	<? if(!empty($post)) { ?>
	<p><span class="notetext">Изменения сохранены</span></p>
<? } ?>
					<div class="profile">
				<form class="default" action="/personal/address/" method="post" enctype="multipart/form-data">

					<div class="cabinet-block-input">
						<label>Город</label>
						<select name="city">
							<option value=""<?if($datadeliv['PROPERTY_CITY_VALUE'] == '') echo ' selected="selected"'?>>---</option>
							<option value="Москва"<?if($datadeliv['PROPERTY_CITY_VALUE'] == 'Москва') echo ' selected="selected"'?>>Москва</option>
							<option value="Петербург"<?if($datadeliv['PROPERTY_CITY_VALUE'] == 'Петербург') echo ' selected="selected"'?>>Петербург</option>
							<option value="Другой"<?if($datadeliv['PROPERTY_CITY_VALUE'] != 'Москва' && $datadeliv['PROPERTY_CITY_VALUE'] != 'Петербург' && $datadeliv['PROPERTY_CITY_VALUE'] != '') echo ' selected="selected"'?>>Другой</option>
						</select>
					</div>
					<div id="city_other" class="cabinet-block-input"<?if($datadeliv['PROPERTY_CITY_VALUE'] == 'Москва' || $datadeliv['PROPERTY_CITY_VALUE'] == 'Петербург' || $datadeliv['PROPERTY_CITY_VALUE'] == '') echo ' style="display:none;"'?>>
						<label>&nbsp;</label>
						<input type="text" name="city_other" <?if($datadeliv['PROPERTY_CITY_VALUE'] != 'Москва' && $datadeliv['PROPERTY_CITY_VALUE'] != 'Петербург' && $datadeliv['PROPERTY_CITY_VALUE'] != '') echo ' value="'.$datadeliv['PROPERTY_CITY_VALUE'].'"'?>/>
					</div>
					<hr/>
					<div class="cabinet-block-input">
						<label>Улица</label>
						<input type="text" name="STREET" value="<?=$datadeliv['PROPERTY_STREET_VALUE']?>" />

						<label>Корпус</label>
						<input type="text" name="CORPUS" value="<?=$datadeliv['PROPERTY_CORPUS_VALUE']?>" />
					</div>
					<div class="cabinet-block-input">
						<label>Дом</label>
						<input type="HOME" name="HOME" value="<?=$datadeliv['PROPERTY_HOME_VALUE']?>" />

						<label>Квартира</label>
						<input type="text" name="ROOM" value="<?=$datadeliv['PROPERTY_ROOM_VALUE']?>" />
					</div>
					<hr/>
					<div class="cabinet-block-input">
						<label>&nbsp;</label>
						<input class="submitz" type="submit" name="save" value="Сохранить" />
					</div>
					<div class="clear"></div>
				</form>
			</div>
			
			<script type="text/javascript">
				$(document).ready(function(){
					$('select[name=city]').change(function(){
						if ($(this).val()=='Другой') {
							$('#city_other').show();
						} else {
							$('#city_other').hide();
						}
					});
				});
			</script>
			
			</div>
</div>
									</div>
			</div>
		</div>
<? } require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>